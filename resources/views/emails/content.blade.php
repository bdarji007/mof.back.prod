<tr>
    <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;"
        align="right">
        <table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;"
               border="0" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;">
                    <a style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;"
                       href="http://task.najah.online/dashboard#details/{!! $USER_TASK_REF !!}"
                       target="_blank" rel="noopener">اضغط هنا لعرض
                        المهمة</a></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
