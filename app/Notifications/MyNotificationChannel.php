<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MyNotificationChannel extends Notification
{
    use Queueable;

    protected $details;
    protected $noti_def_id;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details, $noti_def_id)
    {
        $this->details = $details;
        $this->noti_def_id = $noti_def_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //'mail',
        return [CustomDatabaseChannel::class, 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Bhavesh The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }*/

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => $this->details['content'],
            'noti_def_id' => $this->noti_def_id
        ];
    }

   /* public function toDatabase($notifiable)
    {
        return [
            'data' => $this->details['order_id']
        ];
    }*/
}
