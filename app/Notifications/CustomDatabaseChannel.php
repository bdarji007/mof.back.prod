<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;

class CustomDatabaseChannel
{

    public function send($notifiable, Notification $notification)
    {
        $data = $notification->toArray($notifiable);

        return $notifiable->routeNotificationFor('database')->create([
            'id' => $notification->id,

            //customize here
            'noti_def_id' => $data['noti_def_id'], //<-- comes from toDatabase() Method below

            'type' => get_class($notification),
            'data' => $data,
            'read_at' => null,
        ]);
    }

}
