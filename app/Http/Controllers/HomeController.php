<?php

namespace App\Http\Controllers;

use App\Notifications\MyNotificationChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Modules\ClientApp\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function sendNotification()
    {
        $user = User::all();

        $details = [
            'greeting' => 'Hi Artisan',
            'body' => 'This is my first notification from ItSolutionStuff.com',
            'thanks' => 'Thank you for using ItSolutionStuff.com tuto!',
            'actionText' => 'View My Site',
            'actionURL' => url('/'),
            'order_id' => 101
        ];

        Notification::send($user, new MyNotificationChannel($details));

        dd('done');
    }
}
