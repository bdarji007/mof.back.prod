<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\ClientApp\Entities\NotificationDefination;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;

class NotificationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:notification-view|notification-create|notification-edit|notification-delete', ['only' =>
            ['index', 'show']]);
        $this->middleware('permission:notification-create', ['only' => ['edit', 'saveNotification']]);
        $this->middleware('permission:notification-edit', ['only' => ['edit', 'saveNotification']]);
        $this->middleware('permission:notification-view', ['only' => ['viewnotification']]);
        $this->middleware('permission:notification-delete', ['only' => ['notificationdelete']]);
        $this->middleware('permission:notification-active-inactive', ['only' => ['notificationActiveDeactive']]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $notification = NotificationDefination::get();
        $i = 0;
        $notifiChannel = $notifiEvent = $notifiStatus = [];
        foreach ($notification as $notif) {
            $event = \DB::select(\DB::raw("select * from  notif_event where id=$notif->event_id"));
            $channelName = ($notif->channel == 'a' ? 'Alert' : ($notif->channel == 'i' ? 'Inbox' : ($notif->channel == 'e'
                ? 'Email' : 'Broadcast')));
            $notification[$i]->eventName = $event[0]->name;
            $notification[$i]->channelName = $channelName;
            $notifiEvent[$i]['value'] = $notif->event_id;
            $notifiEvent[$i]['text'] = $event[0]->name;
            $notifiChannel[$i]['value'] = $notif->channel;
            $notifiChannel[$i]['text'] = $channelName;
            $notifiStatus[$i]['value'] = ($notif->status_active) ? 1 : 0;
            $notifiStatus[$i]['text'] = ($notif->status_active == 0 ? 'deactive' : 'active');
            $i++;
        }
        $newArray = array();
        $usedFruits = array();
        foreach ($notifiChannel AS $key => $line) {
            if (!in_array($line['value'], $usedFruits)) {
                $usedFruits[] = $line['value'];
                $newArray[$key] = $line;
            }
        }
        $notifiChannel = $newArray;
        $newArray = NULL;
        $usedFruits = NULL;

        $newArray = array();
        $usedFruits = array();
        foreach ($notifiEvent AS $key => $line) {
            if (!in_array($line['value'], $usedFruits)) {
                $usedFruits[] = $line['value'];
                $newArray[$key] = $line;
            }
        }
        $notifiEvent = $newArray;
        $newArray = NULL;
        $usedFruits = NULL;

        $newArray = array();
        $usedFruits = array();
        foreach ($notifiStatus AS $key => $line) {
            if (!in_array($line['value'], $usedFruits)) {
                $usedFruits[] = $line['value'];
                $newArray[$key] = $line;
            }
        }
        $notifiStatus = $newArray;
        $newArray = NULL;
        $usedFruits = NULL;

        return response()->json([
            "code" => 200,
            "notificationlist" => $notification,
            "notifiChannel" => $notifiChannel,
            "notifiEvent" => $notifiEvent,
            "notifiStatus" => $notifiStatus,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('clientapp::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    public function notificationview(Request $request, $id) {
        return $this->edit($request, $id);
    }

    public function edit(Request $request, $id)
    {
        $notification = NotificationDefination::Where('id', $id)->first();

        //var_dump($notification);
        if ($notification) {
            $getUsers = \DB::table('notif_to_user')->where('notif_id', $id)->get();
            $getRoles = \DB::table('notif_to_role')->where('notif_id', $id)->get();
            $roleIds = [];
            foreach ($getRoles as $role) {
                $roleIds[] = $role->role_id;
            }

            $usersIdData = [];
            foreach ($getUsers as $user) {
                $usersIdData[] = $user->user_id;
            }

            $role = Role::whereIn('id', $roleIds)->get();
            $users = User::join('subtenant', 'users.subtenant_id', '=', 'subtenant.id')->select('users.id', 'users.name', 'users.last_name', 'subtenant.name as sub_name', 'users.sector_id', 'users.subtenant_id', 'users.email')->whereHas('roles', static function ($query) use ($roleIds) {
                return $query->whereIn('id', $roleIds);
            })->get();

            $userDataArray = [];
            $j = 0;
            foreach ($users as $userData) {
                if (in_array($userData->id, $usersIdData)) {
                    $userData->is_id = $userData->id;
                    $userData->noti_id = $userData->id;
                    $userData->subtenant_id = (int)$userData->subtenant_id;
                    $userDataArray[$j] = $userData;
                } else {
                    //$userData->isCheck = false;
                    $userData->noti_id = $userData->id;
                    $userDataArray[$j] = $userData;
                }
                $j++;
            }
//->whereIn('users.id', $usersIdData)

            $userIds = [];
            $i = 0;
            foreach ($userDataArray as $use) {
                if ($use->is_id) {
                    $userIds[] = $use->id;
                }
                $getSub = \DB::table('subtenant')->where('id', $use->sector_id)->get();
                $userDataArray[$i]->sector_name = $getSub[0]->name;
                $i++;
            }

            $notIn = array_diff($usersIdData, $userIds);

            if (count($notIn) > 0) {
                foreach ($notIn as $not) {
                    $user = User::join('subtenant', 'users.subtenant_id', '=', 'subtenant.id')->select('users.id', 'users.name', 'users.last_name', 'subtenant.name as sub_name', 'users.sector_id', 'users.subtenant_id', 'users.email')->where('users.id', $not)->get();
                    $k = 0;
                    $notInData = [];
                    foreach ($user as $use) {
                        $notInData[$k] = $use;
                        $userIds[] = $use->id;
                        $notInData[$k]->is_id = $use->id;
                        $notInData[$k]->outOfRole = true;
                        $notInData[$k]->noti_id = $use->id;
                        $notInData[$k]->subtenant_id = (int)$use->subtenant_id;
                        $getSub = \DB::table('subtenant')->where('id', $use->sector_id)->get();
                        $notInData[$k]->sector_name = $getSub[0]->name;
                        $k++;
                    }
                }
                $userDataArray = array_merge($userDataArray, $notInData);
            }

            /*$notiShowData = [];
            foreach ($notification as $notify) {
                $notiShowData['channel'] = $notify['channel'];
                $notiShowData['contents'] = $notify['contents'];
                $notiShowData['created_by'] = $notify['created_by'];
                $notiShowData['description'] = $notify['description'];
                $notiShowData['event_id'] = $notify['event_id'];
                $notiShowData['is_recur'] = $notify['is_recur'];
                $notiShowData['notif_time'] = $notify['notif_time'];
                $notiShowData['recur_dom'] = $notify['recur_dom'];
                $notiShowData['recur_dow'] = $notify['recur_dow'];
                $notiShowData['recur_m_condition'] = $notify['recur_m_condition'];
                $notiShowData['recur_period'] = $notify['recur_period'];
                $notiShowData['recur_q_condition'] = $notify['recur_q_condition'];
                $notiShowData['recur_qe_diff_days'] = $notify['recur_qe_diff_days'];
                $notiShowData['start_dt'] = $notify['start_dt'];
                $notiShowData['status_active'] = $notify['status_active'];
            }*/
            $roleShow = [];
            $i = 0;
            foreach ($role as $ro) {
                $roleShow[$i]['id'] = $ro['id'];
                $roleShow[$i]['name'] = $ro['name'];
                $i++;
            }

            $userShow = [];
            $j = 0;
            foreach ($userDataArray as $user) {
                $userShow[$j]['id'] = $user['id'];
                $userShow[$j]['name'] = $user['name'] . ' ' . $user['last_name'];
                $userShow[$j]['email'] = $user['email'];
                $userShow[$j]['sub_name'] = $user['sub_name'];
                $userShow[$j]['sector_name'] = $user['sector_name'];
                $j++;
            }

            return response()->json([
                "code" => 200,
                "notificationdata" => $notification,
                "role" => $role,
                "roleIds" => $roleIds,
                "users" => $userDataArray,
                "userIds" => $userIds,
                "usersIdData" => $usersIdData,
                "notIn" => $notIn,
                "roleShow" => $roleShow,
                "userShow" => $userShow
            ]);
        }

        return response()->json([
            "code" => 404,
            "msg" => "data not found"
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
    }

    public function show($id)
    {

    }

    public function notificationActiveDeactive(Request $request)
    {
        $id = $request->get('notifyId');
        $event = \DB::select(\DB::raw("select * from  notif_def where id=$id"));

        $status = ($event[0]->status_active == 0) ? 1 : 0;
        $statusMessage = ($status == 0) ? 'deactive' : 'active';

        if (\DB::select(\DB::raw("update notif_def set status_active= $status where id=$id"))) {

        }
        return response()->json([
            "code" => 200,
            "msg" => "data $statusMessage successfully"
        ]);
    }

    public function notificationdelete(Request $request)
    {
        $id = $request->get('notifyId');
        $user = NotificationDefination::find($id);
        if ($user->delete()) {
            return response()->json([
                "code" => 200,
                "msg" => "deleted the record"
            ]);
        }
    }

    /***
     * @param $treeArrayGroups
     * @param $rootArray
     * @return mixed
     * Recursive Array
     */
    function transformTree($treeArrayGroups, $rootArray)
    {
        // Read through all nodes where parent is root array
        foreach ($treeArrayGroups[$rootArray['id']] as $child) {
            //echo $child['id'].PHP_EOL;
            // If there is a group for that child, aka the child has children
            if (isset($treeArrayGroups[$child['id']])) {
                // Traverse into the child
                $newChild = $this->transformTree($treeArrayGroups, $child);
            } else {
                $newChild = $child;
            }

            if ($child['id'] != '') {
                // Assign the child to the array of children in the root node
                $rootArray['tree'][] = $newChild;
            }
        }
        return $rootArray;
    }

    public function loadNotificationDefaultData(Request $request)
    {
        $notiArgs = \DB::table('event_arg')->get();
        $notiEvent = \DB::table('notif_event')->get();
        $roles = Role::get();
        $dom = [];
        for ($i = 1; $i <= 31; $i++) {
            $dom[] = $i;
        }

        $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = 2 UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), 2, '', CONCAT(id, '') from subtenant where id = 2) select id, name as label, name, parent_id from cte order by path"));
        $result = array_map(function ($value) {
            return (array)$value;
        }, $rows);

        // Group by parent id
        $treeArrayGroups = [];
        foreach ($result as $record) {
            $treeArrayGroups[$record['parent_id']][] = $record;
        }
        // Get the root
        $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
        // Transform the data
        $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

        $data = [];
        $data[] = $outputTree;

        return response()->json([
            "code" => 200,
            "roles" => $roles,
            "notiArgs" => $notiArgs,
            "notiEvent" => $notiEvent,
            "dom" => $dom,
            "sectors" => $data
        ]);
    }

    public function loadUserDataFromRole(Request $request)
    {
        $roles = $request->get('roles');
        $role = Role::whereIn('id', $roles)->get();
        $users = User::join('subtenant', 'users.subtenant_id', '=', 'subtenant.id')->select('users.id', 'users.name',
            'users.last_name', 'subtenant.name as sub_name', 'users.sector_id', 'users.subtenant_id', 'users.email')->whereHas('roles', static
        function ($query) use ($roles) {
            return $query->whereIn('id', $roles);
        })->get();

        $userIds = [];
        $i = 0;
        foreach ($users as $use) {
            $userIds[] = $use->id;
            $getSub = \DB::table('subtenant')->where('id', $use->sector_id)->get();
            $users[$i]->sector_name = $getSub[0]->name;
            $users[$i]->subtenant_id = (int)$use->subtenant_id;
            $users[$i]->is_id = $use->id;
            $users[$i]->noti_id = $use->id;
            $i++;
        }

        return response()->json([
            "code" => 200,
            "role" => $role,
            "users" => $users,
            "userIds" => $userIds
        ]);
    }

    public function saveNotification(Request $request)
    {

        if (isset($request->notification['id']) && $request->notification['id'] != '') {
            $notifDef = NotificationDefination::find($request->notification['id']);

            if (!$notifDef) {
                return response()->json([
                    "code" => 404,
                    "msg" => "data not found"
                ]);
            } else {
                //var_dump($request->all());

                $notifDef->description = $request->notification['description'];
                $notifDef->tenant_id = auth()->user()->tenant_id;//env('TENANT_ID');
                $notifDef->channel = $request->notification['channel'];
                $notifDef->event_id = $request->notification['event_id'];
                $notifDef->contents = $request->notification['contents'];
                $notifDef->subject = $request->notification['subject'];
                $notifDef->channel = $request->notification['channel'];
                //$notifDef->status_active = 0;
                $notifDef->start_dt = $request->notification['start_dt'];
                $notifDef->end_dt = $request->notification['end_dt'];
                $notifDef->notif_time = $request->notification['notif_time'];
                $notifDef->is_recur = $request->notification['is_recur'];
                $notifDef->recur_period = $request->notification['recur_period'];
                $notifDef->recur_dow = $request->notification['recur_dow'];
                $notifDef->recur_dom = $request->notification['recur_dom'];
                $notifDef->recur_m_condition = $request->notification['recur_m_condition'];
                $notifDef->recur_q_condition = $request->notification['recur_q_condition'];
                $notifDef->recur_qe_diff_days = $request->notification['recur_qe_diff_days'];
                $notifDef->created_by = auth()->user()->id;

                if ($notifDef->save()) {
                    if (count($request->users) > 0) {
                        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
                        \DB::table("notif_to_user")->where('notif_id', $notifDef->id)->delete();
                        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
                        foreach ($request->users as $user) {
                            $notifUser = \DB::table("notif_to_user")->insert(//insert(
                                [
                                    'notif_id' => $notifDef->id,
                                    'user_id' => $user,
                                ]
                            );
                        }
                    }
                    if (count($request->roles) > 0) {
                        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
                        \DB::table("notif_to_role")->where('notif_id', $notifDef->id)->delete();
                        \DB::statement('SET FOREIGN_KEY_CHECKS=1');

                        foreach ($request->roles as $role) {
                            $notifUser = \DB::table("notif_to_role")->insert(//insert(
                                [
                                    'notif_id' => $notifDef->id,
                                    'role_id' => $role,
                                ]
                            );
                        }
                    }
                }
                return response()->json([
                    "code" => 200,
                    "msg" => "data updated successfully"
                ]);
            }
        } else {
            $notifDef = new NotificationDefination();
            $notifDef->description = $request->notification['description'];
            $notifDef->tenant_id = auth()->user()->tenant_id;//env('TENANT_ID');
            $notifDef->channel = $request->notification['channel'];
            $notifDef->event_id = $request->notification['event_id'];
            $notifDef->contents = $request->notification['contents'];
            $notifDef->subject = $request->notification['subject'];
            $notifDef->channel = $request->notification['channel'];
            $notifDef->status_active = 0;
            $notifDef->start_dt = $request->notification['start_dt'];
            $notifDef->end_dt = $request->notification['end_dt'];
            $notifDef->notif_time = $request->notification['notif_time'];
            $notifDef->is_recur = $request->notification['is_recur'];
            $notifDef->recur_period = $request->notification['recur_period'];
            $notifDef->recur_dow = $request->notification['recur_dow'];
            $notifDef->recur_dom = $request->notification['recur_dom'];
            $notifDef->recur_m_condition = $request->notification['recur_m_condition'];
            $notifDef->recur_q_condition = $request->notification['recur_q_condition'];
            $notifDef->recur_qe_diff_days = $request->notification['recur_qe_diff_days'];
            $notifDef->created_by = auth()->user()->id;

            if ($notifDef->save()) {

                foreach ($request->users as $user) {
                    $notifUser = \DB::table("notif_to_user")->insert(//insert(
                        [
                            'notif_id' => $notifDef->id,
                            'user_id' => $user,
                        ]
                    );
                }

                foreach ($request->roles as $role) {
                    $notifUser = \DB::table("notif_to_role")->insert(//insert(
                        [
                            'notif_id' => $notifDef->id,
                            'role_id' => $role,
                        ]
                    );
                }
            }
            return response()->json([
                "code" => 200,
                "msg" => "data created successfully"
            ]);
        }
        //var_dump($request->notification['description']);
        //var_dump($request->notification['description']);
        //var_dump($request->users);
        //var_dump($request->roles);
    }

    public function selectUser($id)
    {
        $user = User::join('subtenant', 'users.subtenant_id', '=', 'subtenant.id')->select('users.id', 'users.name', 'users.last_name', 'subtenant.name as sub_name', 'users.sector_id', 'users.subtenant_id', 'users.email')->where('users.id', $id)->get();
        $i = 0;
        foreach ($user as $use) {
            $userIds[] = $use->id;
            $user[$i]->is_id = $use->id;
            $user[$i]->noti_id = $use->id;
            $user[$i]->subtenant_id = (int)$use->subtenant_id;
            $user[$i]->outOfRole = true;
            $getSub = \DB::table('subtenant')->where('id', $use->sector_id)->get();
            $user[$i]->sector_name = $getSub[0]->name;
            $i++;
        }
        return response()->json([
            "code" => 200,
            "user" => $user
        ]);
    }

    public function loadKpiOrgUsersNotification($orgUnit, $sector = null)
    {
        if ($orgUnit != 'null' && $orgUnit != 'undefined') {

            $users = \DB::select(\DB::raw("WITH RECURSIVE cte (level1_id, id, parent_id, subtenant_type, name, path) AS (
	-- This is end of the recursion: Select low level
	select id, id, parent_id, subtenant_type_id, name, concat( cast(id as char(200)), '_')
		from subtenant where
        id = $orgUnit -- set your arg here
	UNION ALL
    -- This is the recursive part: It joins to cte
    select c.level1_id, s.id, s.parent_id, s.subtenant_type_id, s.name, CONCAT(c.path, ',', s.id)
		from subtenant s
        inner join cte c on s.parent_id = c.id
	)
	-- select id, name, subtenant_type, parent_id
	--  cte.level1_id, cte.id, cte.parent_id, cte.subtenant_type,
	select cte.name as subname, u.*
	from cte, users u where
	u.subtenant_id = cte.id
	and u.deleted_at is null
	order by path, u.name;"));
        } else {
            $users = [];
        }
        return response()->json([
            "code" => 200,
            "data" => $users
        ]);
    }

    public function getNotificationArgs($event_id) {
        $notiArgs = \DB::select(\DB::raw("select * from event_arg arg inner join event_arg_rel rel on rel.arg_id=arg.id where rel.event_id=$event_id"));
        $notiBroadCastLink = \DB::select(\DB::raw("select * from notif_event where id=$event_id"));
	return response()->json([
            "code" => 200,
            "notiArgs" => $notiArgs,
	    "notiBroadCastLink" => ($notiBroadCastLink[0]->screen_name) ? url('').'/'.$notiBroadCastLink[0]->screen_name : ''
        ]);
    }
}
