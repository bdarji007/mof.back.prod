<?php

namespace Modules\ClientApp\Http\Controllers;

use Modules\ClientApp\Reports\ProjectTeamCostReport;
use Illuminate\Http\Request;

class ProjectTeamCostReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware("guest");
    }


    public function index(Request $request)
    {
        $language=$request->lang;
        $sect=$request->sect;
        $org=$request->org;
        $back=$request->back;
        $sid=$request->sid;
        $oid=$request->oid;



        // $trans=json_decode($request->translation);
       $report = new ProjectTeamCostReport(array(
           "language"=>$language,
           "sid"=>$sid,
           "oid"=>$oid,
           "sect"=>$sect,
           "org"=>$org,
           "back"=>$back));//,"translation"->$trans));
        $report->run();
        return view("projectteamcostreport",["report"=>$report]);
    }


}
