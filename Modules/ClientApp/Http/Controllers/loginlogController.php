<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class loginlogController extends Controller
{
    //
    public function index()
    {
        $loginlogs = \DB::table("login_activities")
            ->join('users', 'login_activities.user_id', '=', 'users.id')
            ->select('login_activities.*', 'users.name', 'users.email', 'users.sector_id', 'users.subtenant_id', 'users.last_name')
            ->where('login_activities.created_at', '>=', date(Carbon::today()))
            ->orderby('login_activities.id', 'desc')
            ->get();
        $i = 0;
        foreach ($loginlogs as $loginlog) {
//            $temp1 = explode('/', $loginlog->user_agent);
//            $loginlogs[$i]->user_agent = $temp1[0];
            $loginlogs[$i]->user_agent = $loginlog->user_agent;
            $loginlogs[$i]->name = $loginlog->name . " " . $loginlog->last_name;
            $loginlogs[$i]->orgname = $this->getnames($loginlog->subtenant_id);
            $loginlogs[$i]->id = $i + 1;
            $i++;
        }
        if ($loginlogs) {
            return response()->json([
                "code" => 200,
                "loginlogs" => $loginlogs
            ]);
        }

        return response()->json(["code" => 400]);
    }

    public function loadTenants()
    {
        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3"));*/
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }


    public function loadSubTenants($id)
    {
        //$subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), cast(id as char(200)) from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '-'), '>', s.name), s.parent_id, CONCAT(c.level, '-'), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id) select id, name from cte order by path"));
        $subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id) select id, name from cte order by path"));
        if ($subtenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $subtenants
            ]);
        }
    }

    public function loadloginlogDataSector($id)
    {
//        $loginlogsdata = [];
//        $orgidval = \DB::select(\DB::raw("select id,parent_id from subtenant where  id=$id || parent_id=$id"));
//        $orgid = $orgidval[0]->id;
//        //$orgparentid = $orgidval[0]->parent_id;

        $useridval = \DB::select(\DB::raw("select id from users where sector_id=$id"));
        $user_id = $useridval[0]->id;
//            foreach ($user_ids as $user_id) {
        //echo $user_id->id."==";
        $loginlogs = \DB::table("login_activities")
            ->join('users', 'login_activities.user_id', '=', 'users.id')
            ->select('login_activities.*', 'users.name', 'users.last_name', 'users.subtenant_id')
            ->where('login_activities.user_id', '=', $user_id)
            ->get();
        $i = 0;
        if (count($loginlogs) != 0) {
            foreach ($loginlogs as $loginlog) {
                $loginlogs[$i]->user_agent = $this->get_browser_name($loginlog->user_agent);

                $loginlogs[$i]->name = $loginlog->name . " " . $loginlog->last_name;
                $loginlogs[$i]->orgname = $this->getnames($loginlog->subtenant_id);
                $loginlogs[$i]->id = $i + 1;
                $i++;
            }
            //  $loginlogsdata[] = $loginlogs;
        }


//            }
//        }


        if ($loginlogs) {
            return response()->json([
                "code" => 200,
                "loginlogs" => $loginlogs
            ]);
        }

        return response()->json(["code" => 400, "loginlogs" => $loginlogs]);
    }

    public function loadloginlogDataOrgUnit($id)
    {
//        $orgidval = \DB::select(\DB::raw("select id,parent_id from subtenant where  id=$id || parent_id=$id"));
//        $orgid = $orgidval[0]->id;
//
//        $useridval = \DB::select(\DB::raw("select id from users where subtenant_id=$orgid"));
//        $user_id = $useridval[0]->id;
        $loginlogsdata = [];
        $loginlogs = [];
        $useridval = \DB::select(\DB::raw("select id from users where subtenant_id=$id"));
        if ($useridval) {
            $user_id = $useridval[0]->id;
//        foreach ($user_ids as $user_id) {
            //echo $user_id->id."==";
            $loginlogs = \DB::table("login_activities")
                ->join('users', 'login_activities.user_id', '=', 'users.id')
                ->select('login_activities.*', 'users.name', 'users.email', 'users.sector_id', 'users.subtenant_id', 'users.last_name')
                ->where('login_activities.user_id', '=', $user_id)
                ->get();
//            if (count($loginlogs) != 0) {
//                $loginlogsdata[] = $loginlogs;
//            }


            //}
            $i = 0;
            foreach ($loginlogs as $loginlog) {

                $loginlogs[$i]->user_agent = $loginlog->user_agent;
                $loginlogs[$i]->name = $loginlog->name . " " . $loginlog->last_name;
                $loginlogs[$i]->orgname = $this->getnames($loginlog->subtenant_id);
                $loginlogs[$i]->id = $i + 1;
                $i++;
            }
        }
        if ($loginlogs) {
            return response()->json([
                "code" => 200,
                "loginlogs" => $loginlogs
            ]);
        }

        return response()->json(["code" => 400, "loginlogs" => $loginlogsdata]);
    }

    public function loadloginDataDate($data)
    {

        $str_arr = explode(",", $data);
        $str_arr[0] = str_replace(" GMT+0300 (Arabian Standard Time)", " ", $str_arr[0]);
        $str_arr[1] = str_replace(" GMT+0300 (Arabian Standard Time)", " ", $str_arr[1]);

        $dates = strtotime($str_arr[0]);
        //echo $dates;
        $datee = strtotime($str_arr[1]);

        $startdate = date('yy-m-d H:i:s', $dates);
        $enddate = date('yy-m-d H:i:s', $datee);

        $startdate = (date('Y-m-d', strtotime($str_arr[0])));
        $enddate = (date('Y-m-d', strtotime($str_arr[1])));


        $loginlogs = \DB::table("login_activities")
            ->join('users', 'login_activities.user_id', '=', 'users.id')
            ->select('login_activities.*', 'users.name', 'users.email', 'users.sector_id', 'users.subtenant_id', 'users.last_name')
            ->whereBetween('login_activities.created_at', [$startdate, $enddate])
           // ->where('login_activities.created_atss', '<=', $enddate)
            ->get();
        $i = 0;
        foreach ($loginlogs as $loginlog) {
            $loginlogs[$i]->user_agent = $this->get_browser_name($loginlog->user_agent);
            $loginlogs[$i]->orgname = $this->getnames($loginlog->subtenant_id);
            $loginlogs[$i]->name = $loginlog->name . " " . $loginlog->last_name;
            $loginlogs[$i]->id = $i + 1;
            $i++;
        }

        if ($loginlogs) {
            return response()->json([
                "code" => 200,
                "loginlogs" => $loginlogs
            ]);
        }
        return response()->json(["code" => 400, "loginlogs" => $loginlogs]);
    }

    public function getnames($id)
    {
        $subnamevalue = \DB::select(\DB::raw("SELECT name from subtenant where id=$id"));
        $subname = $subnamevalue[0]->name;
        return $subname;

    }

    public function get_browser_name($user_agent)
    {

        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
        elseif (strpos($user_agent, 'Edge')) return 'Edge';
        elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
        elseif (strpos($user_agent, 'Safari')) return 'Safari';
        elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

        return 'Other';
    }
}
