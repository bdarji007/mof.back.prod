<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class performanceController extends Controller
{
    //
    public function index()
    {

        $perfprog = DB::select(
            "call p_unit_perf_prog(114,4,'Q1',1);"
        );
        foreach ($perfprog as $row) {
            $kpishowdata[] = [
                'KPIdata' => 'Symbol: ' . $row->kpi_symbol,
                'importance' => $row->weight,
                'importancetxt' => 'Importance: ' . $row->weight,
                'performance' => $row->kpi_perf * 100,
                'performancetxt' => 'Performance: ' . (number_format($row->kpi_perf, 2) * 100) . '%',
                'progress' => 'Progress: ' . (number_format($row->kpi_prog, 2) * 100) . '%',
                'KPIname' => $row->kpi_name,
                'adjustedweight' => $row->adjusted_weight,
                'weightedperformance' => $row->adjusted_weight * $row->kpi_perf,


            ];
        }


        header('Content-type: application/json');
        //   echo json_encode($kpishowdata);


        if ($kpishowdata) {
            return response()->json([
                "code" => 200,
                "kpishowdata" => $kpishowdata
            ]);
        }

        return response()->json(["code" => 400]);
    }

    public function loadTenants()
    {
        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3"));*/
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }


    public function sectionkpireport($value)
    {
        $debug_mode = 'false';
        $debug_show_icon = 0;
        $debug_show_icon_best = 0;
        $debug_show_icon_least = 0;
        $kpi_perf = 0;
        $debug_progmode = 'false';
        $debug_show_icon_prog = 0;
        $debug_tooltip_prog = 0;
        $valuearray = explode(",", $value);
        $supervisionback = '';
        $ministryback = '';
        $departmentback = '';
        $isSuperParam = '';

        $debugprog = \DB::select(\DB::raw("select value  from system_vars where name='show_<0_prog'"));
        $debugperf = \DB::select(\DB::raw("select value  from system_vars where name='show_>100_perf'"));

        $debugmodeprog = $debugprog[0]->value;
        $debugmodeperf = $debugperf[0]->value;
        if ($debugmodeperf == 'true') {
            $debug_mode = 'true';
        } else {
            $debug_mode = 'false';
        }
        if ($debugmodeprog == 'true') {
            $debug_progmode = 'true';
        } else {
            $debug_progmode = 'false';
        }


        $section = $valuearray[0];
        $mtp = $valuearray[1];
        $asofperiod = $valuearray[2];
        $yearno = $valuearray[3];
        if ($valuearray[4]) {
            $supervisionback = $valuearray[4];
        }
        if ($valuearray[5]) {
            $ministryback = $valuearray[5];
        }

        if ($valuearray[6]) {
            $departmentback = $valuearray[6];
        }

        if ($valuearray[7]) {
            $isSuperParam = $valuearray[7];
        }

        $isTen = $isSub = $isDirect = false;
        if ($valuearray[8]) {
            $isTen = $valuearray[8];
        }

        if ($valuearray[9]) {
            $isSub = $valuearray[9];
        }

        if ($valuearray[10]) {
            $isDirect = $valuearray[10];
        }

        $deptid = \DB::select(\DB::raw("select parent_id from subtenant where id=$section"));
        $deptval = $deptid[0]->parent_id;
        $supervision = $deptval;

        $suprid = \DB::select(\DB::raw("select parent_id from subtenant where id=$supervision"));
        $superval = $suprid [0]->parent_id;
        $department = $superval;

        $sectorpid = \DB::select(\DB::raw("select parent_id from subtenant where id=$deptval"));
        $sectorpval = $sectorpid[0]->parent_id;
        $sectorid = \DB::select(\DB::raw("select parent_id from subtenant where id=$sectorpval"));
        $sectorval = $sectorid[0]->parent_id;


        if ($sectorval == 2) {
            $sectorval = $sectorpval;
        }
        $perfprog = DB::select(
            "call p_unit_perf_prog($section,$mtp,'$asofperiod',$yearno);"
        );
//       var_dump($perfprog);
//       die();
        $acheivedcount = 0;
        $uptodatecount = 0;
        $unitperformance = 0;
        $unitprogress = 0;
        $nonuptodatecount = 0;
        $performingkpicount = 0;
        $nonperformingkpicount = 0;

        $getOrgName = '';
	$kpishowdata = array();
        foreach ($perfprog as $row) {
            $getOrgName = $row->sub_name;
            $kpistatus = \DB::select(\DB::raw("SELECT active_status FROM `kpi_def` WHERE id=$row->kpi_id"));

            if ($row->kpi_perf * 100 >= 50) {
                $acheivedcount = $acheivedcount + 1;
                $performingkpicount = $performingkpicount + 1;
            }
            if ($row->kpi_perf * 100 < 50) {

                $nonperformingkpicount = $nonperformingkpicount + 1;
            }

            $weighedperf = $row->adjusted_weight * $row->kpi_perf;
            $weighedprog = $row->adjusted_weight * $row->kpi_prog;
            $unitperformance = $unitperformance + $weighedperf;
            $unitprogress = $unitprogress + $weighedprog;
            if ($row->kpi_up_to_date == 1) {
                $uptodatecount = $uptodatecount + 1;

            }
            if ($row->kpi_up_to_date == 0) {
                $nonuptodatecount = $nonuptodatecount + 1;

            }


            if ($debug_mode == 'false') {
                if ($row->kpi_perf * 100 > 100) {
                    $kpi_perf = 100;
                    $debug_show_icon = 1;
                    $debug_show_icon_perf_tool = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                } else {
                    $kpi_perf = $row->kpi_perf * 100;
                    $debug_show_icon = 0;
                    $debug_show_icon_perf_tool = '';

                }

            } else {
                $kpi_perf = $row->kpi_perf * 100;
            }
            if ($debug_progmode == 'false') {
                if ($row->kpi_prog * 100 < 0) {
                    $kpi_prog = 0;
                    $debug_show_icon_prog = 1;
                } else {
                    $kpi_prog = $row->kpi_prog * 100;
                    $kpi_prog = (number_format($kpi_prog, 2));
                    $debug_show_icon_prog = 0;
                }

            } else {
                $kpi_prog = $row->kpi_prog * 100;
                $kpi_prog = (number_format($kpi_prog, 2));
            }

            $uptodateicon = '';
            $debugicon = '';

            if ($row->kpi_up_to_date == 0) {
                $uptodateicon = '<i class="fas fa-exclamation-circle fa-2x"></i>';
            }
            if ($debug_show_icon == 1 || $debug_show_icon_prog == 1) {
                $debugicon = '<i style="color:#ffffff" class="fas fa-exclamation-triangle fa-2x"></i>';
            }
            $gaugelink = '';
            if ($kpistatus[0]->active_status == 1) {
                $gaugelink = "/gaugechart/" . $row->kpi_id . "?query=ministry=,sector=" . $sectorval . ",department=" . $department . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno;
                /*$gaugelink = "gaugechart/" . $row->kpi_id . "?query=" . $section . "," . $mtp . "," . $asofperiod . "," . $yearno . ",fromsector," . $sectorval;*/

                if ($supervisionback != '' || $ministryback != '') {
                    if ($supervisionback == 'supervision') {
                        $superbacklink = ',' . $supervision;
                        $gaugelink = $gaugelink . $superbacklink;
                    }
                    $ministrybacklink = '';
                    if ($ministryback == 'fromministry') {
                        $ministrybacklink = ',' . $ministryback . '=true,isDepartment=false,isSupervision=' . $isSuperParam . ',' . $departmentback . ',' . $isTen . ',' . $isSub . ',' . $isDirect;
                        $gaugelink = $gaugelink . $ministrybacklink;
                    }
                    if ($ministryback == 'false') {
                        $ministrybacklink = ',fromministry=false';
                        $gaugelink = $gaugelink . $ministrybacklink;
                    }
                    if ($departmentback == 'fromDepartment') {
                        $ministrybacklink = ',isDepartment=false,isSupervision=' . $isSuperParam . ',' . $departmentback . ',' . $isTen . ',' . $isSub . ',' . $isDirect;
                        $gaugelink = $gaugelink . $ministrybacklink;
                    }
                } else {
                    /*$gaugelink = "gaugechart/" . $row->kpi_id . "?query=" . $section . "," . $mtp . "," . $asofperiod . "," . $yearno . ",fromsector," . $sectorval;*/
                    $gaugelink = "/gaugechart/" . $row->kpi_id . "?query=ministry=,sector=" . $sectorval . ",department="
                        . $department . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period="
                        . $asofperiod . ",year=" . $yearno . ",fromministry=false,isDepartment=false,isSupervision=false,,,,,";

                }
            }
            $kpishowdata[] = [
                'KPIdata' => $row->kpi_symbol,
                'active_status' => $kpistatus[0]->active_status,
                'importance' => $row->weight,
                'importanceIncrease' => 20,//($row->weight > 0 && $row->weight < 10) ? $row->weight + 20 : $row->weight,
                'importancetxt' => 'Importance: ' . $row->weight,
                'performance' => $kpi_perf,
                'performancetxt' => (number_format($kpi_perf)) . '%',
                'progress' => $kpi_prog . '%',
                'KPIname' => $row->kpi_name,
                'orgname' => $row->sub_name,
                'adjustedweight' => (number_format($row->adjusted_weight * 100, 2)) . '%',
                'weightedperformance' => (number_format(($row->adjusted_weight * $row->kpi_perf) * 100, 2)) . '%',
                'kpi_up_to_date' => $row->kpi_up_to_date,
                'debug_show_icon' => $debug_show_icon,
                "link" => ($gaugelink) ? "<a style='color:#ffffff;font-weight: bold' href= '$gaugelink'>KPI Quick View</a>" : '',
                "linkAr" => ($gaugelink) ? "<a style='color:#ffffff;font-weight: bold' href= '$gaugelink'>لمحة سريعة عن المؤشر</a>" : '',
                "button" => ($uptodateicon || $debugicon) ? '<div style="color:#ffffff" >' . $uptodateicon . $debugicon
                    . '</div>' : '',

            ];
        }
        $unitperformance = (number_format($unitperformance * 100, 2));
        $unitprogress = (number_format($unitprogress * 100, 2));
        if ($debug_progmode == 'false') {
            if ($unitprogress < 0) {
                $unitprogress = 0;
                $debug_show_icon_prog = 1;
            } else {
                $unitprogress = $unitprogress;
                $debug_show_icon_prog = 0;
            }
        }


        header('Content-type: application/json');
        //   echo json_encode($kpishowdata);


        if ($kpishowdata) {
            return response()->json([
                "code" => 200,
                "kpishowdata" => isset($kpishowdata) ? $kpishowdata : null,
                "totalkpi" => count($perfprog),
                "acheivedcount" => $acheivedcount,
                "uptodatecount" => $uptodatecount,
                "unitperformance" => $unitperformance,
                "unitprogress" => $unitprogress,
                "nonuptodatecount" => $nonuptodatecount,
                "performingkpicount" => $performingkpicount,
                "nonperformingkpicount" => $nonperformingkpicount,
                "debug_show_icon_prog" => $debug_show_icon_prog,
                "getOrgName" => $getOrgName

            ]);
        }

        //return response()->json(["code" => 400]);
	return response()->json([
                "code" => 200,
                "kpishowdata" => ($kpishowdata) ? $kpishowdata : null,
                "totalkpi" => count($perfprog),
                "acheivedcount" => $acheivedcount,
                "uptodatecount" => $uptodatecount,
                "unitperformance" => $unitperformance,
                "unitprogress" => $unitprogress,
                "nonuptodatecount" => $nonuptodatecount,
                "performingkpicount" => $performingkpicount,
                "nonperformingkpicount" => $nonperformingkpicount,
                "debug_show_icon_prog" => $debug_show_icon_prog,
                "getOrgName" => $getOrgName

            ]);
    }

    public function departmentreport($value)
    {
        $kpiperfarray = array();
        $debug_mode = 'true';
        $debug_show_icon = 0;
        $debug_show_icon_best = 0;
        $debug_show_icon_least = 0;
        $kpi_color = 0;
        $superlink = '';
        $superlinkar = '';
        $debug_progmode = 'true';
        $kpi_progsum = 1;
        $debug_show_icon_prog_tool = 0;
        $debug_show_icon_prog = 0;
        $debug_show_icon_progstrategic = 0;
        $debug_show_icon_progic = '';
        $debug_show_icon_perf_tool = '';
        $debugprog = \DB::select(\DB::raw("select value  from system_vars where name='show_<0_prog'"));
        $debugperf = \DB::select(\DB::raw("select value  from system_vars where name='show_>100_perf'"));

        $debugmodeprog = $debugprog[0]->value;
        $debugmodeperf = $debugperf[0]->value;
        if ($debugmodeperf == 'true') {
            $debug_mode = 'true';
        } else {
            $debug_mode = 'false';
        }
        if ($debugmodeprog == 'true') {
            $debug_progmode = 'true';
        } else {
            $debug_progmode = 'false';
        }

        $getKpiPerfProg = [];
        $getVisionKpiPerfProg = [];
        $countPerfProgKPI = 0;

        $valuearray = explode(",", $value);

        $dept = $valuearray[0];
        $mtp = $valuearray[1];
        $asofperiod = $valuearray[2];
        $yearno = $valuearray[3];
        $type = $valuearray[4];
        $ministryBack = $valuearray[5];
        $isSuperParam = $valuearray[6];
        $isTen = $valuearray[7];
        $isSub = $valuearray[8];
        $isDirect = $valuearray[9];

        $kpistrategic_sector = [];

        $dptperfprog = DB::select(
            " call p_unit_perf_prog_recursive($dept, $mtp,'$asofperiod',$yearno);"
        );
        $kpistrategic_perf = 0;
        $kpistrategic_perf = 0;
        $kpistrategic_prog = 0;
        $kpistrategic_prog = 0;
        $kpistrategic_kpi = 0;
        $kpistrategicvalues = [];
        $ministry = $sector = '';

        if ($type == 0 || $type == 1) {
            $dptkpistrategic = DB::select("call p_unit_perf_prog_a_scope(
				$dept, $mtp,'$asofperiod',$yearno,'O');"
            );
            $kpistrategic_perfarr = [];
            $kpistrategic_progarr = [];
            $kpistrategic_kpiarr = [];

            if ($dptkpistrategic) {
//                if ($type == 0) {
//                    $kpistrategic_perf = $dptkpistrategic[0]->kpi_perf;
//                    $kpistrategic_perf = (number_format($kpistrategic_perf * 100, 2));
//                    $kpistrategic_prog = $dptkpistrategic[0]->kpi_prog;
//                    if ($debug_mode == true) {
//                        if ($kpistrategic_prog < 0) {
//                            $kpistrategic_prog = 0;
//                            $debug_show_icon_progstrategic = 1;
//                        } else {
//                            $kpistrategic_prog = (number_format($dptkpistrategic[0]->kpi_prog * 100, 2));
//                            $debug_show_icon_progstrategic = 0;
//                        }
//
//                    }
//                    //$kpistrategic_prog =$kpistrategic_prog ;
//                    $kpistrategic_kpi = $dptkpistrategic[0]->kpi_name;
//                }

                if ($type == 1 || $type == 0) {
                    foreach ($dptkpistrategic as $dptkpistrategicval) {
                        $kpistrategic_perf = $dptkpistrategicval->kpi_perf;
                        $kpistrategic_perfarr = (number_format($kpistrategic_perf * 100, 2));
                        $kpistrategic_prog = $dptkpistrategicval->kpi_prog;
                        $kpistrategic_progarr = (number_format($kpistrategic_prog * 100, 2));
                        $kpistrategic_kpiarr = $dptkpistrategicval->kpi_name;

                        if ($debug_progmode == 'false') {
                            if ($kpistrategic_progarr < 0) {
                                $kpistrategic_progarr = 0;
                                $debug_show_icon_prog = 1;
                            } else {
                                $kpistrategic_progarr = $kpistrategic_progarr;
                                $debug_show_icon_prog = 0;
                            }
                        }
                        $kpistrategicvalues[] = [
                            'kpigoalname' => $kpistrategic_kpiarr,
                            'kpigoalperf' => $kpistrategic_perfarr,
                            'kpigoalprog' => $kpistrategic_progarr,
                            'kpigoalkpiid' => $dptkpistrategicval->kpi_id

                        ];

                    }

                }
            }

        }
        if ($type == 0) {

            \DB::statement("set @org_unit := $dept");
            \DB::statement("set @mtp_id:= $mtp");
            \DB::statement("set @year_no:= $yearno");
            \DB::statement("set @period_name := '$asofperiod'");

            $getVisionKpiPerfProg = \DB::select(\DB::raw("select 	sub_id, sub_name,
						obj_id,
						obj_name,
						sort_no,
						icon_name,
						round(sum(kpi_perf * weight) / sum(weight), 8) as perf_sum_w,
						round(sum(kpi_prog * weight) / sum(weight), 8) as prog_sum_w
from
(			select	sub.id as sub_id,
									sub.name as sub_name,
									ob.id as obj_id,
									ob.name as obj_name,
									ob.sort_no,
									ob.icon_name,
									case lower(@period_name)
											when 'q1' then kvs.q1_perf
											when 'q2' then kvs.q2_perf
											when 'q3' then kvs.q3_perf
											when 'q4' then kvs.q4_perf
											when 'h1' then kvs.h1_perf
											when 'h2' then kvs.h2_perf
											when 'y' then kvs.y_perf
											else null
									end as kpi_perf,
									case lower(@period_name)
											when 'q1' then kvs.q1_prog
											when 'q2' then kvs.q2_prog
											when 'q3' then kvs.q3_prog
											when 'q4' then kvs.q4_prog
											when 'h1' then kvs.h1_prog
											when 'h2' then kvs.h2_prog
											when 'y' then kvs.y_prog
											else null
									end as kpi_prog,
									kd.importance as weight
			from  	kpi_values_stats kvs, kpi_target kt, kpi_def kd, subtenant sub, objective ob, org_objectives oo
			where	sub.id = @org_unit and
									sub.id = oo.subtenant_id and
									oo.objective_id = kd.scope_id and
									kd.scope_table = 'objective' and
									ob.id = oo.objective_id and
									-- sub.id = kd.child_subtenant_id and
									kt.kpi_id = kd.id and
									kt.mtp_id = @mtp_id and
									kvs.kpi_target_id = kt.id and
									kvs.year_no = @year_no
) as t1
group by t1.sub_id, t1.sub_name, t1.obj_id, t1.obj_name, t1.sort_no, t1.icon_name
order by sort_no;"));

            $jj = 0;
            //var_dump($getKpiPerfProg);
            foreach ($getVisionKpiPerfProg as $val) {
                //echo $val->prog_sum_w;
                $getVisionKpiPerfProg[$jj]->prog_sum_w = number_format($val->prog_sum_w * 100, 2);
                $getVisionKpiPerfProg[$jj]->perf_sum_w = number_format($val->perf_sum_w * 100, 2);
                $jj++;
            }

            if (count($getVisionKpiPerfProg) > 3) {
                $countPerfProgKPI = 3;
            } elseif (count($getVisionKpiPerfProg) > 2) {
                $countPerfProgKPI = 4;
            } elseif (count($getVisionKpiPerfProg) > 1) {
                $countPerfProgKPI = 6;
            } else {
                $countPerfProgKPI = 12;
            }

            $sectorlink = '';
            $sectorlinkar = '';
            foreach ($dptperfprog as $row) {
//                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
//                    $kpiperfarray[] = $row->perf_sum_w;
//
//                }
                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);
                if (isset($dataarray[0])) {
                    $supervisionarray[] = $dataarray[0];


                }

            }

            $supervision = '';
            $kpi_count = 0;
            $section = '';
            $supervisionname = '';
            $sectionname = '';
            //$kpitotcount = $dptperfprog[0]->child_count_eff;
            $kpitotcount = $dptperfprog[0]->unit_count;
            $performingkpicount = $dptperfprog[0]->performing_count;
            $nonperformingkpicount = $kpitotcount - $performingkpicount;

            foreach ($dptperfprog as $row) {
//echo $row->perf_sum_w;
                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
                    $kpiperfarray[] = $row->perf_sum_w;

                }
//            else{
//                $kpiperfarray[] =0;
//            }


                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);

                $dept = preg_replace('/[^A-Za-z0-9]/', '', $dataarray[0]);
                $projectdetails = DB::select(
                    "call p_unit_proj_summary($dept,$mtp,'$asofperiod',$yearno);"
                );
                $deptnameval = \DB::select(\DB::raw("select name from subtenant where id=$dept"));
                $deptname = $deptnameval[0]->name;
                if (isset($dataarray[1])) {
                    $supervision = $dataarray[1];
                    $supervisionnameval = \DB::select(\DB::raw("select name from subtenant where id=$supervision"));
                    $supervisionname = $supervisionnameval[0]->name;
                }
                if (isset($dataarray[2])) {
                    $section = $dataarray[2];
                    $sectionnameval = \DB::select(\DB::raw("select name from subtenant where id=$section"));
                    $sectionname = $sectionnameval[0]->name;

                }


                if ($row->kpi_importance_sum == null) {
                    $row->kpi_importance_sum = 0;
                }
                if ($row->kpi_count == null) {
                    $row->kpi_count = 0;
                }
                $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$row->sub_id"));
                $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
//            if ($subtenant_type_idval == 9) {
//                $section = $supervision;
//                $sectionname = $supervisionname;
//            }
                $kpi_perfsum = (number_format($row->perf_sum_w * 100, 2));
                if ($debug_mode == 'false') {
                    if ($kpi_perfsum > 100) {
                        $kpi_perfsum = 100;
                        $debug_show_icon = 1;
                    } else {
                        $debug_show_icon = 0;
                    }

                }
                if ($kpi_perfsum >= 40 && $kpi_perfsum <= 60) {
                    $kpi_color = 1;

                } else {
                    $kpi_color = 0;
                }
                $kpid1[] = [


                    'supervision' => $supervision,
                    "dept" => $dept,
                    'section' => $section,
                ];

                $linksDept = "/departmentperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $dept . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",isDepartment=true,isSupervision=false,fromDepartment";

                $linksSuper = "/supervisionperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $dept . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",isDepartment=false,isSupervision=" . $isSuperParam . ",fromDepartment," . $isTen . ',' . $isSub;

                $linksSection = "/sectionperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $dept . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",isDepartment=false,isSupervision=" . $isSuperParam . ",fromDepartment," . $isTen . ',' . $isSub . ',' . $isDirect;

                if ($supervision == '') {
                    $supervision = $dept;
                    $deptidid = \DB::select(\DB::raw("select parent_id from subtenant where id=$dept"));
                    $deptidval = $deptidid[0]->parent_id;
                    $sectordid = \DB::select(\DB::raw("select parent_id from subtenant where id=$deptidval"));
                    $sectoridval = $sectordid[0]->parent_id;
//                $sectorlink='';
//                $sectorlinkar='';
//                if($supervision) {
//
//                    //$sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",supervision=" . $sectoridval . '>Show Section Report</a></div>';
//                   // $sectorlinkar = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",supervision=" . $sectoridval . '>عرض أداء القسم</a></div>';
//
//                   // $superlinkar = '<div style=float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=\'. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" .\'> عرض أداء المراقبة </a></div>\'<br><div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" . '> عرض أداء المراقبة </a></div>';
//                    $superlink='';
//
//
//                }
//            }
//            else {
//                if($this->arraycount($supervisionarray, $supervision)==1){
//                   // $superlink ='<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href='. "/departmentperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept".'">Show Supervision Report</a><br><a style="color:#ffffff;font-size:16px;" href='. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",sector=" . $sectoridval.'>Show Section Report</a></div>';
//                 //   $superlinkar ='<div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=\'. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" .\'> عرض أداء المراقبة </a></div>\'<br><div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href='. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" .'> عرض أداء المراقبة </a></div>';
//                   // $sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",sector=" . $sector . '>Show Section Report</a></div>';
//
//
//
//                }
                    if ($dataarray[0]) {
                        if ($this->arraycount($supervisionarray, $dataarray[0]) == 1) {
                            $sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';
                            $sectorlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';


                        } else {
                            $superlink = '';
                            $superlinkar = '';
                        }

                    }
                }

                if ($debug_progmode == 'false') {
                    if ($row->prog_sum_w < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog_tool = 1;
                        $debug_show_icon_progic = '<i style="margin-left:10px;margin-right:10px;color:#ffffff" class="fas fa-exclamation-triangle fa-1x"></i>';
                    } else {
                        $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));

                        $debug_show_icon_prog_tool = 0;
                        $debug_show_icon_progic = '';
                    }
                } else {
                    $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));
                }


                $kpishowdata[] = [
                    'Category' => 'deptreport',
                    'deptname' => ($deptname) ? $deptname : null,
                    'supervision' => $supervision,
                    'section' => $section,
                    'kpi_importancesum' => $row->kpi_importance_sum,
                    'kpi_perfsum' => $kpi_perfsum,
                    'kpi_progsum' => $kpi_progsum,
                    'orgname' => $row->sub_name,
                    'calc_level' => $row->calc_level,
                    'supervisionname' => ($supervisionname) ? $supervisionname : null,
                    'sectionname' => ($sectionname) ? $sectionname : null,
                    'kpicount' => $row->kpi_count,
                    'kpicountIncrease' => ($row->kpi_count > 0 && $row->kpi_count < 10) ? $row->kpi_count + 20 : $row->kpi_count,
                    'unitcount' => $row->unit_count,
                    "dept" => $dept,
                    "mtp" => $mtp,
                    "asofperiod" => $asofperiod,
                    "yearno" => $yearno,
                    "type" => $type,
                    "debug_show_icon" => $debug_show_icon,
                    "link" => $linksSection,
                    "supervisionlink" => $linksSuper,
                    "kpi_color" => $kpi_color,
                    "button" => $superlink,
                    "buttonar" => $superlinkar,
                    "sectorlink" => $sectorlink,
                    "sectorlinkar" => $sectorlinkar,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,
                    "debug_show_icon_perf_tool" => $debug_show_icon_perf_tool
                    //  "button"=>$button1


                ];


                $supervision = '';
                $section = '';
                $supervisionname = '';
                $sectionname = '';
                $dataarray = [];
            }


            $unitperformance = $dptperfprog[0]->perf_sum_w;
            $unitprogress = $dptperfprog[0]->prog_sum_w;

            $unitperformance = (number_format($unitperformance * 100, 2));
            $unitprogress = (number_format($unitprogress * 100, 2));
            $kpibestperforming = count($kpiperfarray) > 0 ? max($kpiperfarray) : null;
            $kpibestperforming = (number_format($kpibestperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpibestperforming > 100) {
                    $kpibestperforming = 100;
                    $debug_show_icon_best = 1;
                } else {
                    $debug_show_icon_best = 0;
                }

            }
            $kpileastperforming = count($kpiperfarray) > 0 ? min($kpiperfarray) : null;
            $kpileastperforming = (number_format($kpileastperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpileastperforming > 100) {
                    $kpileastperforming = 100;
                    $debug_show_icon_least = 1;
                } else {
                    $debug_show_icon_least = 0;
                }

            }


        }


        if ($type == 1) {
            $sector = '';
            $sectorname = '';
            $supervision = '';
            $kpi_count = 0;
            $section = '';
            $supervisionname = '';
            $sectionname = '';
            $deptname = '';
            $dept = '';
            //$kpitotcount = $dptperfprog[0]->child_count_eff;
            $kpitotcount = $dptperfprog[0]->unit_count;
            $performingkpicount = $dptperfprog[0]->performing_count;
            $nonperformingkpicount = $kpitotcount - $performingkpicount;
            $i = 0;
            $kpistrategic_sector = [];
            foreach ($dptperfprog as $row) {
//                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
//                    $kpiperfarray[] = $row->perf_sum_w;
//
//                }
                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);
                if (isset($dataarray[2])) {
                    $supervisionarray[] = $dataarray[2];


                }

            }
            $valuearray = explode(",", $value);

            $deptSA = $valuearray[0];
            \DB::statement("set @org_unit := $deptSA");
            \DB::statement("set @mtp_id:= $mtp");
            \DB::statement("set @year_no:= $yearno");
            \DB::statement("set @period_name := '$asofperiod'");

            $getKpiPerfProg = \DB::select(\DB::raw("select 	sub_id, sub_name,
						obj_id,
						obj_name,
						sort_no,
						icon_name,
						round(sum(kpi_perf * weight) / sum(weight), 8) as perf_sum_w,
						round(sum(kpi_prog * weight) / sum(weight), 8) as prog_sum_w
from
(			select	sub.id as sub_id,
									sub.name as sub_name,
									ob.id as obj_id,
									ob.name as obj_name,
									ob.sort_no,
									ob.icon_name,
									case lower(@period_name)
											when 'q1' then kvs.q1_perf
											when 'q2' then kvs.q2_perf
											when 'q3' then kvs.q3_perf
											when 'q4' then kvs.q4_perf
											when 'h1' then kvs.h1_perf
											when 'h2' then kvs.h2_perf
											when 'y' then kvs.y_perf
											else null
									end as kpi_perf,
									case lower(@period_name)
											when 'q1' then kvs.q1_prog
											when 'q2' then kvs.q2_prog
											when 'q3' then kvs.q3_prog
											when 'q4' then kvs.q4_prog
											when 'h1' then kvs.h1_prog
											when 'h2' then kvs.h2_prog
											when 'y' then kvs.y_prog
											else null
									end as kpi_prog,
									kd.importance as weight
			from  	kpi_values_stats kvs, kpi_target kt, kpi_def kd, subtenant sub, objective ob, org_objectives oo
			where	sub.id = @org_unit and
									sub.id = oo.subtenant_id and
									oo.objective_id = kd.scope_id and
									kd.scope_table = 'objective' and
									ob.id = oo.objective_id and
									-- sub.id = kd.child_subtenant_id and
									kt.kpi_id = kd.id and
									kt.mtp_id = @mtp_id and
									kvs.kpi_target_id = kt.id and
									kvs.year_no = @year_no
) as t1
group by t1.sub_id, t1.sub_name, t1.obj_id, t1.obj_name, t1.sort_no, t1.icon_name
order by sort_no;"));

            $jj = 0;
            //var_dump($getKpiPerfProg);
            foreach ($getKpiPerfProg as $val) {
                //echo $val->prog_sum_w;
                $getKpiPerfProg[$jj]->prog_sum_w = number_format($val->prog_sum_w * 100, 2);
                $getKpiPerfProg[$jj]->perf_sum_w = number_format($val->perf_sum_w * 100, 2);
                $jj++;
            }
            if (count($getKpiPerfProg) > 3) {
                $countPerfProgKPI = 3;
            } elseif (count($getKpiPerfProg) > 2) {
                $countPerfProgKPI = 4;
            } elseif (count($getKpiPerfProg) > 1) {
                $countPerfProgKPI = 6;
            } else {
                $countPerfProgKPI = 12;
            }

            foreach ($dptperfprog as $row) {

                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
                    $kpiperfarray[] = $row->perf_sum_w;

                }
//              var_dump($dptperfprog);
//                die();

                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);

                $sector = preg_replace('/[^A-Za-z0-9]/', '', $dataarray[0]);
                $projectdetails = DB::select(
                    "call p_unit_proj_summary($sector,$mtp,'$asofperiod',$yearno);"
                );
                // echo "call p_unit_proj_summary($sector,$mtp,'$asofperiod',$yearno);";
                $sectornameval = \DB::select(\DB::raw("select name from subtenant where id=$sector"));
                $sectorname = $sectornameval[0]->name;
                if (isset($dataarray[1])) {
                    $dept = $dataarray[1];
                    $deptnameval = \DB::select(\DB::raw("select name from subtenant where id=$dept"));
                    $deptname = $deptnameval[0]->name;
                    $depts[] = $dept;

                }

                if (isset($dataarray[2])) {
                    $supervision = $dataarray[2];
                    $supervisionnameval = \DB::select(\DB::raw("select name from subtenant where id=$supervision"));
                    $supervisionname = $supervisionnameval[0]->name;

                }
                if (isset($dataarray[3])) {
                    $section = $dataarray[3];
                    $sectionnameval = \DB::select(\DB::raw("select name from subtenant where id=$section"));
                    $sectionname = $sectionnameval[0]->name;

                }


                if ($row->kpi_importance_sum == null) {
                    $row->kpi_importance_sum = 0;
                }
                if ($row->kpi_count == null) {
                    $row->kpi_count = 0;
                }
                $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$row->sub_id"));
                $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
//                if ($subtenant_type_idval == 9) {
//                    $section = $supervision;
//                    $sectionname = $supervisionname;
//                }
                $kpi_perfsum = (number_format($row->perf_sum_w * 100, 2));
                if ($debug_mode == 'false') {
                    if ($kpi_perfsum > 100) {
                        $kpi_perfsum = 100;
                        $debug_show_icon = 1;
                        $debug_show_icon_perf_tool = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $debug_show_icon = 0;
                        $debug_show_icon_perf_tool = '';
                    }

                }
                if ($kpi_perfsum >= 40 && $kpi_perfsum <= 60) {
                    $kpi_color = 1;

                } else {
                    $kpi_color = 0;
                }
                $kpid1[] = [

                    'sector' => $sector,

                    'supervision' => $supervision,
                    "dept" => $dept,
                    'section' => $section,
                ];

                $linksDept = "/departmentperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $dept . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",isDepartment=true,isSupervision=false,fromDepartment";

                $linksSuper = "/supervisionperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $dept . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",isDepartment=false,isSupervision=" . $isSuperParam . ",fromDepartment," . $isTen . ',' . $isSub;

                $linksSection = "/sectionperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $dept . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",isDepartment=false,isSupervision=" . $isSuperParam . ",fromDepartment," . $isTen . ',' . $isSub . ',' . $isDirect;

                if ($supervision) {
                    if ($this->arraycount($supervisionarray, $supervision) == 1) {
                        $superlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>Show Supervision Report</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';

                        $superlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>عرض أداء المراقبة</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';


                    } else {
                        $superlink = '';
                        $superlinkar = '';
                    }

                }
                $button1 = '<i style="color:#ffffff" class="fas fa-exclamation-triangle fa-2x"></i>';;

                //$superlink='';
                // $superlink="/sectionperformance" . "?query=" . $section . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept.',sector='.$sector;
                if ($debug_progmode == 'false') {
                    if ($row->prog_sum_w < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog_tool = 1;
                        $debug_show_icon_progic = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));

                        $debug_show_icon_prog_tool = 0;
                        $debug_show_icon_progic = '';

                    }
                } else {
                    $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));
                }
                $kpishowdata[] = [
                    'Category' => 'deptreport',
                    'sectorname' => ($sectorname) ? $sectorname : null,
                    'sector' => $sector,
                    'deptname' => ($deptname) ? $deptname : null,
                    'supervision' => $supervision,
                    'section' => $section,
                    'kpi_importancesum' => $row->kpi_importance_sum,
                    'kpi_perfsum' => $kpi_perfsum,
                    'kpi_progsum' => $kpi_progsum,
                    'orgname' => $row->sub_name,
                    'calc_level' => $row->calc_level,
                    'supervisionname' => ($supervisionname) ? $supervisionname : null,
                    'sectionname' => ($sectionname) ? $sectionname : null,
                    'kpicount' => $row->kpi_count,
                    'kpicountIncrease' => ($row->kpi_count > 0 && $row->kpi_count < 10) ? $row->kpi_count + 20 : $row->kpi_count,
                    'unitcount' => $row->unit_count,
                    "dept" => $dept,
                    "mtp" => $mtp,
                    "asofperiod" => $asofperiod,
                    "yearno" => $yearno,
                    "type" => $type,
                    "debug_show_icon" => $debug_show_icon,
                    "link" => $linksSection,
                    "supervisionlink" => $linksSuper,
                    'kpi_color' => $kpi_color,
                    "button" => $superlink,
                    "buttonar" => $superlinkar,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,
                    "debug_show_icon_perf_tool" => $debug_show_icon_perf_tool,


                ];
                $supervisionarray[] = $supervision;

                $sectorname = '';
                $sector = '';
                $supervision = '';
                $section = '';
                $supervisionname = '';
                $sectionname = '';
                $dataarray = [];

                $i++;
            }
            array_unique($depts);


            $duplicate_keys = array();
            $tmp = array();

            foreach ($depts as $key => $val) {
                // convert objects to arrays, in_array() does not support objects
                if (is_object($val))
                    $val = (array)$val;

                if (!in_array($val, $tmp))
                    $tmp[] = $val;
                else
                    $duplicate_keys[] = $key;
            }

            foreach ($duplicate_keys as $key)
                unset($depts[$key]);
            $i = 0;
            foreach ($depts as $deptvalue) {
                $dptkpistrategic_sector = DB::select("call p_unit_perf_prog_a_scope(
				$deptvalue, $mtp,'$asofperiod',$yearno,'O');"
                );

                if (count($dptkpistrategic_sector) != 0) {
//                       var_dump($dptkpistrategic_sector);
//                       $kpistrategic_sector_perf = $dptkpistrategic_sector->kpi_perf;
//                       echo $kpistrategic_sector_perf;
//                       $kpistrategic_sector_perf = (number_format($kpistrategic_perf * 100, 2));
//                       $kpistrategic_sector_prog = $dptkpistrategic_sector[0]->kpi_prog;
//                        $kpistrategic_sector_prog = (number_format($kpistrategic_prog * 100, 2));
//                       $kpistrategic_sector_kpi = $dptkpistrategic_sector[0]->kpi_name;
//
//                        $kpistrategic_sector[$i]->perf=$kpistrategic_sector_perf;
//                       $kpistrategic_sector[$i]->prog=$kpistrategic_sector_prog;
//                       $kpistrategic_sector[$i]->kpi=$kpistrategic_sector_kpi;
//                        $kpistrategic_sector[$i]->dept=$dept;
                    $kpistrategic_sector[] = $dptkpistrategic_sector;


                }

                $i++;
            }

            $unitperformance = $dptperfprog[0]->perf_sum_w;
            $unitprogress = $dptperfprog[0]->prog_sum_w;

            $unitperformance = (number_format($unitperformance * 100, 2));
            $unitprogress = (number_format($unitprogress * 100, 2));
            $kpibestperforming = count($kpiperfarray) > 0 ? max($kpiperfarray) : null;
            $kpibestperforming = (number_format($kpibestperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpibestperforming > 100) {
                    $kpibestperforming = 100;
                    $debug_show_icon_best = 1;
                } else {
                    $debug_show_icon_best = 0;
                }

            }
            $kpileastperforming = count($kpiperfarray) > 0 ? min($kpiperfarray) : null;
            $kpileastperforming = (number_format($kpileastperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpileastperforming > 100) {
                    $kpileastperforming = 100;
                    $debug_show_icon_least = 1;
                } else {
                    $debug_show_icon_least = 0;
                }

            }

        }

        if ($type == 2) {
            $supervision = '';
            $kpi_count = 0;
            $section = '';
            $supervisionname = '';
            $sectionname = '';
            $sectorlink = '';
            $sectorlinkar = '';
            //$kpitotcount = $dptperfprog[0]->child_count_eff;
            $kpitotcount = $dptperfprog[0]->unit_count;
            $performingkpicount = $dptperfprog[0]->performing_count;
            $nonperformingkpicount = $kpitotcount - $performingkpicount;

            foreach ($dptperfprog as $row) {
                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);
                if (isset($dataarray[0])) {
                    $supervisionarray[] = $dataarray[0];


                }

            }
            foreach ($dptperfprog as $row) {

                if ($row->calc_level = 1 && isset($row->perf_sum_w)) {
                    $kpiperfarray[] = $row->perf_sum_w;

                } else {
                    $kpiperfarray[] = 0;
                }


                $subpath = $row->sub_path;
                $dataarray = explode(",", $subpath);

                $supervision = preg_replace('/[^A-Za-z0-9]/', '', $dataarray[0]);
                $projectdetails = DB::select(
                    "call p_unit_proj_summary($supervision,$mtp,'$asofperiod',$yearno);"
                );

                $supervisionnameval = \DB::select(\DB::raw("select name from subtenant where id=$supervision"));
                $supervisionname = $supervisionnameval[0]->name;

                if (isset($dataarray[1])) {
                    $section = $dataarray[1];
                    $sectionnameval = \DB::select(\DB::raw("select name from subtenant where id=$section"));
                    $sectionname = $sectionnameval[0]->name;

                }


                if ($row->kpi_importance_sum == null) {
                    $row->kpi_importance_sum = 0;
                }
                if ($row->kpi_count == null) {
                    $row->kpi_count = 0;
                }
                $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$row->sub_id"));
                $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
                $deptid = \DB::select(\DB::raw("select parent_id from subtenant where id=$supervision"));
                $deptval = $deptid[0]->parent_id;

                if ($subtenant_type_idval == 9) {
                    $section = $supervision;
                    $sectionname = $supervisionname;
                }
                $kpi_perfsum = (number_format($row->perf_sum_w * 100, 2));

                if ($debug_mode == 'false') {
                    if ($kpi_perfsum > 100) {
                        $kpi_perfsum = 100;
                        $debug_show_icon = 1;
                        $debug_show_icon_perf_tool = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $debug_show_icon = 0;
                        $debug_show_icon_perf_tool = '';
                    }


                }
                if ($kpi_perfsum >= 40 && $kpi_perfsum <= 60) {
                    $kpi_color = 1;

                } else {
                    $kpi_color = 0;
                }

//                if($supervision==''){
//                    $supervision=$dept;
//                    $deptidid = \DB::select(\DB::raw("select parent_id from subtenant where id=$dept"));
//                    $deptidval = $deptidid[0]->parent_id;
//                    $sectordid = \DB::select(\DB::raw("select parent_id from subtenant where id=$deptidval"));
//                    $sectoridval = $sectordid[0]->parent_id;
//                    if($supervision) {
//
//                        $sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",sector=" . $sectoridval . '>Show Section Report</a></div>';
//                        $sectorlinkar = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",sector=" . $sectoridval . '>عرض أداء القسم</a></div>';
//
//                        $superlinkar = '<div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=\'. "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" .\'> عرض أداء المراقبة </a></div>\'<br><div style="float:left" style="color:#ffffff" ><a style="margin-left: 500px;color:#ffffff;font-size:16px;" href=' . "/sectionperformance" . "?query=" . $supervision . "," . $mtp . "," . $asofperiod . "," . $yearno . "," . $dept . ",2,dept" . '> عرض أداء المراقبة </a></div>';
//                        $superlink='';
//
//
//                    }
//                }

                $linksDept = "/departmentperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $deptval . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",isDepartment=true,isSupervision=false,fromDepartment";

                $linksSuper = "/supervisionperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $deptval . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",,isDepartment=false,isSupervision=" . $isSuperParam . ",fromDepartment," . $isTen . ',' . $isSub;

                $linksSection = "/sectionperformance" . "?query=ministry=" . $ministry . ",sector=" . $sector . ",department=" . $deptval . ",supervision=" . $supervision . ",section=" . $section . ",mtp=" . $mtp . ",period=" . $asofperiod . ",year=" . $yearno . ",fromministry=" . $ministryBack . ",isDepartment=false,isSupervision=" . $isSuperParam . ",fromDepartment," . $isTen . ',' . $isSub . ',' . $isDirect;

                if ($supervision) {
                    if ($this->arraycount($supervisionarray, $supervision) == 1) {
                        $sectorlink = '<div style="color:#ffffff" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>Show Supervision Report</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>Show Section Report</a></div>';

                        $sectorlinkar = '<div style="color:#ffffff;margin-top: 20px;margin-left: -120px;" ><a style="color:#ffffff;font-size:16px;" href=' . $linksSuper . '>عرض أداء المراقبة</a><br><a style="color:#ffffff;font-size:16px;" href=' . $linksSection . '>عرض أداء القسم</a></div>';


                    } else {
                        $superlink = '';
                        $superlinkar = '';
                    }

                }
                $kpid1[] = [

                    'supervision' => $supervision,

                    'section' => $section,
                    'dept' => $deptval
                ];
                if ($debug_progmode == 'false') {
                    if ($row->prog_sum_w < 0) {
                        $kpi_progsum = 0;
                        $debug_show_icon_prog_tool = 1;
                        $debug_show_icon_progic = '<i style="color:#ffffff;margin-left:10px;margin-right:10px;" class="fas fa-exclamation-triangle fa-1x"></i>';

                    } else {
                        $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));

                        $debug_show_icon_prog_tool = 0;
                        $debug_show_icon_progic = '';

                    }
                } else {
                    $kpi_progsum = (number_format($row->prog_sum_w * 100, 2));
                }

                $kpishowdata[] = [
                    'Category' => 'deptreport',
                    'supervision' => $supervision,
                    'supervisionname' => ($supervisionname) ? $supervisionname : null,
                    'section' => $section,
                    'sectionname' => ($sectionname) ? $sectionname : null,
                    'kpi_importancesum' => $row->kpi_importance_sum,
                    'kpi_perfsum' => $kpi_perfsum,
                    'kpi_progsum' => $kpi_progsum,
                    'orgname' => $row->sub_name,
                    'calc_level' => $row->calc_level,
                    'kpicount' => $row->kpi_count,
                    'kpicountIncrease' => ($row->kpi_count > 0 && $row->kpi_count < 10) ? $row->kpi_count + 20 : $row->kpi_count,
                    'unitcount' => $row->unit_count,
                    "mtp" => $mtp,
                    "asofperiod" => $asofperiod,
                    "yearno" => $yearno,
                    "type" => $type,
                    "debug_show_icon" => $debug_show_icon,
                    "link" => $linksSection,
                    "supervisionlink" => $linksSuper,
                    "kpi_color" => $kpi_color,
                    "sectorlink" => $sectorlink,
                    "sectorlinkar" => $sectorlinkar,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,
                    "debug_show_icon_perf_tool" => $debug_show_icon_perf_tool
                ];


                $supervision = '';
                $section = '';
                $supervisionname = '';
                $sectionname = '';
                $dataarray = [];


            }
            $unitperformance = $dptperfprog[0]->perf_sum_w;
            $unitprogress = $dptperfprog[0]->prog_sum_w;

            $unitperformance = (number_format($unitperformance * 100, 2));
            $unitprogress = (number_format($unitprogress * 100, 2));
            $kpibestperforming = count($kpiperfarray) > 0 ? max($kpiperfarray) : null;
            $kpibestperforming = (number_format($kpibestperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpibestperforming > 100) {
                    $kpibestperforming = 100;
                    $debug_show_icon_best = 1;
                } else {
                    $debug_show_icon_best = 0;
                }

            }
            $kpileastperforming = count($kpiperfarray) > 0 ? min($kpiperfarray) : null;
            $kpileastperforming = (number_format($kpileastperforming * 100, 2));
            if ($debug_mode == 'false') {
                if ($kpileastperforming > 100) {
                    $kpileastperforming = 100;
                    $debug_show_icon_least = 1;
                } else {
                    $debug_show_icon_least = 0;
                }

            }
        }
        if ($debug_progmode == 'false') {
            if ($unitprogress < 0) {
                $unitprogress = 0;
                $debug_show_icon_prog = 1;
            } else {
                $unitprogress = $unitprogress;
                $debug_show_icon_prog = 0;
            }
        }

        header('Content-type: application/json');
//die();
        //var_dump($kpistrategic_sector);

//       $j=0;
//        for($i = 0; $i <= count($kpistrategic_sector[$i]); $i++) {
//            $z = 0;
//        for($z = 0; $z < count($kpistrategic_sector); $z++) {
//
//            $kpistrategic_sectornew[]=$kpistrategic_sector[$i][$z];
//
//            $j=$j+1;
//        }

        $kpistrategic_sectornew = array_reduce($kpistrategic_sector, 'array_merge', array());

        $i = 0;
        $j = 0;
        $kpistrategic_sectornewsame = [];
        foreach ($kpistrategic_sectornew as $kpistrategic_sectornewval) {

            // $j=$i-1;
            if (in_array($kpistrategic_sectornew[$i]->sub_name, $kpistrategic_sectornewsame)) {

                $kpistrategic_sectornew[$i]->sub_name = "";
            } else {
                // echo "no ====".$kpistrategic_sectornew[$i]->sub_name;
                array_push($kpistrategic_sectornewsame, $kpistrategic_sectornew[$i]->sub_name);
            }


            $i++;
        }

        // var_dump($kpistrategic_sectornewsame);

        /////project details
        // $projectdetails='';

//        if($type==1) {
//            $projectdetails = DB::select(
//                "call p_unit_proj_summary($sector,$mtp,'$asofperiod',$yearno);"
//            );
//        }

        //  echo "call p_unit_proj_summary($dept,$mtp,'$asofperiod',$yearno)";
        // die();
//var_dump($projectdetails);
//die();
        if ($kpishowdata) {
            if ($type != 1) {
                return response()->json([
                    "code" => 200,
                    "kpid1" => $kpid1,
                    "kpishowdata" => $kpishowdata,
                    "unitperformance" => $unitperformance,
                    "unitprogress" => $unitprogress,
                    "kpi_count" => $kpitotcount,
                    'unitcount' => $row->unit_count,
                    "kpistrategic_perf" => $kpistrategic_perf,
                    "kpistrategic_prog" => $kpistrategic_prog,
                    "kpistrategic_kpi" => $kpistrategic_kpi,
                    "kpibestperforming" => $kpibestperforming,
                    "kpileastperforming" => $kpileastperforming,
                    "performingcount" => $performingkpicount,
                    "nonperformingcount" => $nonperformingkpicount,
                    "kpistrategic_sector" => $kpistrategic_sectornew,
                    "debug_show_icon_best" => $debug_show_icon_best,
                    "debug_show_icon_least" => $debug_show_icon_least,
                    "debug_show_icon_prog" => $debug_show_icon_prog,
                    "debug_progmode" => $debug_progmode,
                    "debug_show_icon_progstrategic" => $debug_show_icon_progstrategic,
                    "kpistrategicvalues" => $kpistrategicvalues,
                    "projectdetails" => $projectdetails,
                    "getKpiPerfProg" => $getKpiPerfProg,
                    "getVisionKpiPerfProg" => $getVisionKpiPerfProg,
                    "countPerfProgKPI" => $countPerfProgKPI,


                ]);
            } else {
                return response()->json([
                    "code" => 200,
                    "kpid1" => $kpid1,
                    "kpishowdata" => $kpishowdata,
                    'unitcount' => $row->unit_count,
                    "unitperformance" => $unitperformance,
                    "unitprogress" => $unitprogress,
                    "kpi_count" => $kpitotcount,
                    "kpistrategic_perf" => $kpistrategic_perfarr,
                    "kpistrategic_prog" => $kpistrategic_progarr,
                    "kpistrategic_kpi" => $kpistrategic_kpiarr,
                    "kpibestperforming" => $kpibestperforming,
                    "kpileastperforming" => $kpileastperforming,
                    "performingcount" => $performingkpicount,
                    "nonperformingcount" => $nonperformingkpicount,
                    "kpistrategic_sector" => $kpistrategic_sectornew,
                    "kpistrategicvalues" => $kpistrategicvalues,
                    "debug_show_icon_best" => $debug_show_icon_best,
                    "debug_show_icon_least" => $debug_show_icon_least,
                    "debug_show_icon_prog" => $debug_show_icon_prog,
                    "debug_progmode" => $debug_progmode,
                    "debug_show_icon_progstrategic" => $debug_show_icon_progstrategic,
                    "debug_show_icon_prog_tool" => $debug_show_icon_prog_tool,
                    "debug_show_icon_progic" => $debug_show_icon_progic,
                    "projectdetails" => $projectdetails,
                    "getKpiPerfProg" => $getKpiPerfProg,
                    "getVisionKpiPerfProg" => $getVisionKpiPerfProg,
                    "countPerfProgKPI" => $countPerfProgKPI,

                ]);
            }
        }

        return response()->json(["code" => 400]);
    }


    public function loadSubTenants($id)
    {
        //$subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), cast(id as char(200)) from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '-'), '>', s.name), s.parent_id, CONCAT(c.level, '-'), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id) select id, name from cte order by path"));
        $subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id and subtenant_type_id !=6 UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id and subtenant_type_id !=6) select id, name from cte order by path"));


        // $subtenants = \DB::select(\DB::raw("select id, name from subtenant where parent_id = $id and subtenant_type_id=4"));
        foreach ($subtenants as $subtenant) {
//            $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$subtenant->id"));
//            $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
            //echo "in";
            $word = "==>";

            if (strpos($subtenant->name, $word) !== false) {
//               //echo $subtenant->name;
                unset($subtenant->name);
                unset($subtenant->id);
                unset($subtenant);

            }
//
//
        }
////        var_dump($subtenants);
////die();
        //  $subtenants=  array_filter($subtenants);
        foreach ($subtenants as $key => $val) {
            if ($val === null || $val === '')
                unset($subtenants[$key]);
        }
        if ($subtenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $subtenants
            ]);
        }
    }

    public function getsubtenanttype($value)
    {
        $subtenant = $value;
        $subtenant_type_id = \DB::select(\DB::raw("select subtenant_type_id from subtenant where id=$value"));
        $subtenant_type_idval = $subtenant_type_id[0]->subtenant_type_id;
        if ($subtenant_type_idval) {
            return response()->json([
                "code" => 200,
                "subtenanttype" => $subtenant_type_idval
            ]);
        }
    }

    public function arraycount($array, $value)
    {
        $counter = 0;
        foreach ($array as $thisvalue) /*go through every value in the array*/ {
            if ($thisvalue === $value) { /*if this one value of the array is equal to the value we are checking*/
                $counter++; /*increase the count by 1*/
            }
        }
        return $counter;
    }


}
