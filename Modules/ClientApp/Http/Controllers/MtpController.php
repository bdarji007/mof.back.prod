<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Modules\ClientApp\Entities\Fiscalyear;
use Modules\ClientApp\Entities\Mtp;
use Illuminate\Http\Request;

class MtpController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:mtp-view|mtp-create|mtp-edit|mtp-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:mtp-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:mtp-edit', ['only' => ['edit', 'update', 'show']]);
        $this->middleware('permission:mtp-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loadmtp = Mtp::all();
        foreach ($loadmtp as $key => $mtp) {
            $loadfystart = Fiscalyear::select('start_date')->where('id', $mtp->mtp_start)->get();
            $loadfyend = Fiscalyear::select('end_date')->where('id', $mtp->mtp_end)->get();
            //var_dump($loadfystart);

            $loadmtp[$key]->mtp_start = $loadfystart[0]['start_date'];
            $loadmtp[$key]->mtp_end = $loadfyend[0]['end_date'];

        }
        if ($loadmtp) {
            return response()->json([
                "code" => 200,
                "mtpdata" => $loadmtp
            ]);
        }

        return response()->json(["code" => 400]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lastFy = DB::table('mtp')->latest('id')->first();

        $fiscalyear = new Mtp();
        $fiscalyear->tenant_id = env('TENANT_ID');
        $fiscalyear->prev_mtp = $lastFy->id;
        $fiscalyear->name = $request->name;
        $fiscalyear->mtp_start = $request->mtp_start;
        $fiscalyear->mtp_end = $request->mtp_end;
        $fiscalyear->kpi_margin_pct = $request->kpi_margin_pct;

        //$fiscalyear->save();

        /*$fiscalyear = Mtp::create(
            [
                'prev_mtp' => $lastFy->id,
                'tenant_id ' => env('TENANT_ID'),
                'name ' => $request->name,
                'mtp_start' => $request->mtp_start,
                'mtp_end' => $request->mtp_end,
                'kpi_margin_pct' => $request->kpi_margin_pct
            ]
        );*/

        if ($fiscalyear->save()) {
            $event = \DB::select(\DB::raw("select * from  fiscal_year where id >= $request->mtp_start and id <=$request->mtp_end"));
            DB::delete("DELETE FROM `mtp_fiscal_years` WHERE mtp_id=$fiscalyear->id");

            foreach ($event as $key => $val) {
                DB::table('mtp_fiscal_years')->insert(
                    [
                        'mtp_id' => $fiscalyear->id,
                        'fiscal_year_id' => $val->id,
                        'year_no' => $key+1
                    ]
                );
            }
            return response()->json([
                "code" => 200,
                "msg" => "data inserted successfully"
            ]);
        }

        return response()->json(["code" => 400]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Modules\ClientApp\Entities\Mtp  $mtp
     * @return \Illuminate\Http\Response
     */
    public function show(Mtp $mtp, $id)
    {
        $fiscalyear = Mtp::Where('id', $id)->first();

        if ($fiscalyear) {
            return response()->json([
                "code" => 200,
                "data" => $fiscalyear
            ]);
        }

        return response()->json([
            "code" => 404,
            "msg" => "data not found"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Modules\ClientApp\Entities\Mtp  $mtp
     * @return \Illuminate\Http\Response
     */
    public function edit(Mtp $mtp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Modules\ClientApp\Entities\Mtp  $mtp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mtp $mtp, $id)
    {
        $fiscalyear = Mtp::find($id);

        if (!$fiscalyear) {
            return response()->json([
                "code" => 404,
                "msg" => "data not found"
            ]);
        } else {
            $fiscalyear->mtp_start = $request->mtp_start;
            $fiscalyear->mtp_end = $request->mtp_end;
            $fiscalyear->name = $request->name;
            $fiscalyear->kpi_margin_pct = $request->kpi_margin_pct;
            $event = \DB::select(\DB::raw("select * from  fiscal_year where id >= $request->mtp_start and id <=$request->mtp_end"));
            DB::delete("DELETE FROM `mtp_fiscal_years` WHERE mtp_id=$id");

            foreach ($event as $key => $val) {
                DB::table('mtp_fiscal_years')->insert(
                    [
                        'mtp_id' => $id,
                        'fiscal_year_id' => $val->id,
                        'year_no' => $key+1
                    ]
                );
            }
            if ($fiscalyear->update())  {
                return response()->json([
                    "code" => 200,
                    "msg" => "data updated successfully"
                ]);
            }
        }

        return response()->json([
            "code" => 400,
            "msg" => "error updating the data"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Modules\ClientApp\Entities\Mtp  $mtp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mtp $mtp, $id)
    {
        $query = Mtp::find($id);
        if (!$query) {
            return response()->json([
                "code" => 404,
                "msg" => "data not found"
            ]);
        }
        if ($query->delete()) {

            return response()->json([
                "code" => 200,
                "msg" => "deleted the record"
            ]);
        }

        return response()->json(["code" => 400]);
    }

    public function loadmtpfiscaldata(Request $request, $id) {
        $query = $event = \DB::select(\DB::raw("select * from mtp_fiscal_years mfy INNER JOIN mtp m on m.id=mfy.mtp_id INNER JOIN fiscal_year fy on fy.id = mfy.fiscal_year_id where mtp_id='".$id."'"));
        if (!$query) {
            return response()->json([
                "code" => 404,
                'msg' => 'data not found',
                'mtpfydata' => [],
            ]);
        } else {
            return response()->json([
                "code" => 200,
                "mtpfydata" => $query
            ]);
        }
        return response()->json(["code" => 400]);
    }
}
