<?php

namespace Modules\ClientApp\Http\Controllers;

use Modules\ClientApp\Reports\ProjectByStatusReport;
use Illuminate\Http\Request;

class ProjectByStatusReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware("guest");
    }


    public function index(Request $request)
    {
        $language=$request->lang;
        $sect=$request->sect;
        $org=$request->org;
        $back=$request->back;
        $sid=$request->sid;
        $oid=$request->oid;
        $uid=$request->uid;



        // $trans=json_decode($request->translation);
       $report = new ProjectByStatusReport(array(
           "language"=>$language,
           "sid"=>$sid,
           "oid"=>$oid,
           "sect"=>$sect,
           "org"=>$org,
           "back"=>$back,
           "uid"=>$uid,
        ));//,"translation"->$trans));
        $report->run();
        return view("projectbystatusreport",["report"=>$report]);
    }


}
