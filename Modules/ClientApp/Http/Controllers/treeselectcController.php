<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Modules\ClientApp\Entities\SubTenant;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;

class treeselectcController extends Controller
{
    public function index()
    {
    }

    /***
     * @return \Illuminate\Http\JsonResponse
     * Load Sectors
     */
    public function loadTenants()
    {
        //$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s
        //.subtenant_type_id=3"));

        //It's show another options
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));


        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }

    /***
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * Load Organization Units
     */
    public function subtenanttree($id, $from)
    {

        $isAnother = true;
        if (in_array($id, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])) {
            $isAnother = false;
        }
        $tenants1 = SubTenant::with('tree')->Where('parent_id', '<>', 0)->Where('parent_id', '<>', 1)->Where('parent_id', '<>', 2)->Where('parent_id', '<>', 3)->Where('parent_id', '<>', null)->whereNotNull('id')->Where('parent_id', $id)->get();

        $tenants = SubTenant::with('children')->orWhere('parent_id', $id)->get();
        $i = 0;
        $j = 0;
        if ($tenants) {
            foreach ($tenants as $tenant) {
                $tenants[$i]['id'] = $tenant->id;
                $tenants[$i]['label'] = $tenant->name;
                $i = $i + 1;
            }
            $j = 0;
            foreach ($tenants1 as $tenant12) {
                $tenants1[$j]['id'] = $tenant12->id;
                $tenants1[$j]['label'] = $tenant12->name;
                $j = $j + 1;
            }

            $resultNew = array();
            //if (auth()->user()->hasRole(['Admin'])) {
            if (auth()->user()->currentRole == 'Admin') {
                $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = $id UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), $id, '', CONCAT(id, '') from subtenant where id = $id) select id, name as label, name, parent_id from cte order by path"));
                $result = array_map(function ($value) {
                    return (array)$value;
                }, $rows);

                // Group by parent id
                $treeArrayGroups = [];
                foreach ($result as $record) {
                    $treeArrayGroups[$record['parent_id']][] = $record;
                }
                // Get the root
                $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
                // Transform the data
                $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

                $data = [];
                $data[] = $outputTree;
            } else {
                if ($from == 'true') {
                    $getSectors = [];
                    $dd = 0;
                    $getSec = User::select('sector_id', 'subtenant_id')->where('id', auth()->user()->id)->get();
                    if ($id == $getSec[0]['sector_id']) {
                        $id = $getSec[0]['subtenant_id'];

                        $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = $id UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), $id, '', CONCAT(id, '') from subtenant where id = $id) select id, name as label, name, parent_id from cte order by path"));
                        $result = array_map(function ($value) {
                            return (array)$value;
                        }, $rows);

                        // Group by parent id
                        $treeArrayGroups = [];
                        foreach ($result as $record) {
                            $treeArrayGroups[$record['parent_id']][] = $record;
                        }
                        // Get the root
                        $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
                        // Transform the data
                        $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

                        $data = [];
                        $data[] = $outputTree;
                    } else {
                        $getRole = Role::where('name', auth()->user()->currentRole)->pluck('id')->all();
                        /*$workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $roles[0] . "'"));*/
                        $workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $getRole[0] . "' and sector_id='".$id."'"));
                        $getBehalfSector = [];
                        if (count($workbelf)) {
                            $i = 0;
                            foreach ($workbelf as $behalf) {
                                $getBehalfSector[$i]['subtenant_id'] = $behalf->subtenant_id;
                                $getBehalfSector[$i]['sector_id'] = $behalf->sector_id;
                                $i++;
                            }
                        }
                        $getSectors = [];
                        foreach($getBehalfSector as $sector) {
                            $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = '" . $sector['subtenant_id'] . "' UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), '" . $sector['subtenant_id'] . "', '', CONCAT(id, '') from subtenant where id = '" . $sector['subtenant_id'] . "') select id, name as label, name, parent_id from cte order by path"));
                            $result = array_map(function ($value) {
                                return (array)$value;
                            }, $rows);

                            // Group by parent id
                            $treeArrayGroups = [];
                            foreach ($result as $record) {
                                $treeArrayGroups[$record['parent_id']][] = $record;
                            }
                            // Get the root
                            $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
                            // Transform the data
                            $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

                            $data = [];
                            $data[] = $outputTree;
                            $resultNew=array_merge($resultNew, $data);

                        }
                    }
                } else {
                    $getSectors = [];
                    $dd = 0;
                    $getSec = User::select('sector_id', 'subtenant_id')->where('id', auth()->user()->id)->get();
                    $getSectors[$dd]['subtenant_id'] = ($getSec[0]['subtenant_id']);
                    $getSectors[$dd]['sector_id'] = ($getSec[0]['sector_id']);
                    $roles = [];
                    foreach (auth()->user()->roles as $role) {
                        $roles[$role->id] = $role->id;
                    }
                    sort($roles);
                    $getRole = Role::where('name', auth()->user()->currentRole)->pluck('id')->all();
                    /*$workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $roles[0] . "'"));*/
                    $workbelf = \DB::select(\DB::raw("SELECT * FROM `role_work_on_behalf_sectors` WHERE role_id='" . $getRole[0] . "'"));
                    $getBehalfSector = [];
                    if (count($workbelf)) {
                        $i = 0;
                        foreach ($workbelf as $behalf) {
                            $getBehalfSector[$i]['subtenant_id'] = $behalf->subtenant_id;
                            $getBehalfSector[$i]['sector_id'] = $behalf->sector_id;
                            $i++;
                        }
                    }

                    $kk = 1;

                    /*var_dump(array_merge($getSectors, $getBehalfSector));
                    die;*/
                    $isExits = [];
                    foreach ((array_merge($getSectors, $getBehalfSector)) as $key => $sector) {
                        if (in_array($sector['sector_id'], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) || $sector['sector_id'] ==
                            $id) {
                            if (!in_array($sector['subtenant_id'], $isExits)) {
                                $isExits[] = $sector['subtenant_id'];
                                $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = '" . $sector['subtenant_id'] . "' UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), '" . $sector['subtenant_id'] . "', '', CONCAT(id, '') from subtenant where id = '" . $sector['subtenant_id'] . "') select id, name as label, name, parent_id from cte order by path"));
                                $result = array_map(function ($value) {
                                    return (array)$value;
                                }, $rows);

                                // Group by parent id
                                $treeArrayGroups = [];
                                foreach ($result as $record) {
                                    $treeArrayGroups[$record['parent_id']][] = $record;
                                }
                                // Get the root
                                $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
                                // Transform the data
                                $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

                                $data = [];
                                $data[] = $outputTree;
                                $resultNew = array_merge($resultNew, $data);
                            }
                        } else if ($isAnother && !in_array($sector['subtenant_id'], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])) {
                            if (!in_array($sector['subtenant_id'], $isExits)) {
                                $isExits[] = $sector['subtenant_id'];
                                $rows = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = '" . $sector['subtenant_id'] . "' UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), '" . $sector['subtenant_id'] . "', '', CONCAT(id, '') from subtenant where id = '" . $sector['subtenant_id'] . "') select id, name as label, name, parent_id from cte order by path"));
                                $result = array_map(function ($value) {
                                    return (array)$value;
                                }, $rows);

                                // Group by parent id
                                $treeArrayGroups = [];
                                foreach ($result as $record) {
                                    $treeArrayGroups[$record['parent_id']][] = $record;
                                }
                                // Get the root
                                $rootArray = $result[0]['id'] != '' ? $result[0] : $result[1];
                                // Transform the data
                                $outputTree = $this->transformTree($treeArrayGroups, $rootArray);

                                $data = [];
                                $data[] = $outputTree;
                                $resultNew = array_merge($resultNew, $data);
                            }
                        }
                    }
                }
            }
            //$data[] = $resultNew;
            //var_dump($resultNew);
            return response()->json([
                "code" => 200,
                "subTenants" => $tenants1,
                "subTenantsdept" => $tenants,
                "tree" => ($resultNew) ? $resultNew : $data,
            ]);
        }
        return response()->json(["code" => 400]);
    }

    /***
     * @param $treeArrayGroups
     * @param $rootArray
     * @return mixed
     * Recursive Array
     */
    function transformTree($treeArrayGroups, $rootArray)
    {
        // Read through all nodes where parent is root array
        foreach ($treeArrayGroups[$rootArray['id']] as $child) {
            //echo $child['id'].PHP_EOL;
            // If there is a group for that child, aka the child has children
            if (isset($treeArrayGroups[$child['id']])) {
                // Traverse into the child
                $newChild = $this->transformTree($treeArrayGroups, $child);
            } else {
                $newChild = $child;
            }

            if ($child['id'] != '') {
                // Assign the child to the array of children in the root node
                $rootArray['tree'][] = $newChild;
            }
        }
        return $rootArray;
    }

    public function processList(array $list)
    {
        $listResult = ['keepValue' => false, // once set true will propagate upward
            'value' => []];

        foreach ($list as $name => $item) {
            if (is_null($item)) { // see is_scalar test
                continue;
            }

            if (is_scalar($item)) { // keep the value?
                if (!empty($item) || strlen(trim($item)) > 0) {
                    $listResult['keepValue'] = true;
                    $listResult['value'][$name] = $item;
                }
            } else { // new list... recurse

                $itemResult = processList($item);
                if ($itemResult['keepValue']) {
                    $listResult['keepValue'] = true;
                    $listResult['value'][$name] = $itemResult['value'];
                }
            }
        }
        return $listResult;
    }
}
