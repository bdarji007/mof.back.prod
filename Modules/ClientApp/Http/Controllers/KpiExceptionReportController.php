<?php

namespace Modules\ClientApp\Http\Controllers;

use Modules\ClientApp\Reports\KpiExceptionReport;
use Illuminate\Http\Request;

class KpiExceptionReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware("guest");
    }


    public function index(Request $request)
    {
        $language=$request->lang;
        $sid=$request->sid;
        $oid=$request->oid;
        $uid=$request->uid;

              // $trans=json_decode($request->translation);

       $report = new KpiExceptionReport(array("language"=>$language,
       "sid"=>$sid,
       "oid"=>$oid,
       "uid"=>$uid,

));//,"translation"->$trans));
        $report->run();
        return view("kpiexceptionreport",["report"=>$report]);
    }


}
