<?php

namespace Modules\ClientApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class strategycostdistributionlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:strategy-costdistribution-view|strategy-costdistribution-create|strategy-costdistribution-edit|strategy-costdistribution-delete',
            ['only' => ['linkedlist', 'show']]);
        //$this->middleware('permission:strategy-view', ['only' => ['show']]);
        $this->middleware('permission:strategy-costdistribution-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:strategy-costdistribution-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:strategy-costdistribution-delete', ['only' => ['destroy']]);
    }
    //
    public function index(Request $request)//strategylist
    {
        $prc_id = $request->prc_id;
        $obj_id = \DB::select(\DB::raw("select * from org_objectives where id=$prc_id"));
        if ($obj_id) {
            $ob_id = $obj_id[0]->objective_id;
            $sid = $obj_id[0]->sector_id;
            $oid = $obj_id[0]->subtenant_id;
            // var_dump($ob_id);
            $strategylist11 = \DB::table("objective")
                ->select('objective.id', 'objective.name', 'objective.name_en', 'objective.objective_type', 'objective.tenant_id', 'org_objectives.obj_cost_pct')
                ->join('org_objectives', 'org_objectives.objective_id', '=', 'objective.id')
                ->Where('org_objectives.sector_id', $sid)->Where('org_objectives.subtenant_id', $oid)
                ->get();
            if ($strategylist11) {
                foreach ($strategylist11 as $strategylist1) {

                    $cat_id = $strategylist1->objective_type;
                    $tenant_id = $strategylist1->tenant_id;

                    if ($cat_id != NULL) {
                        $a = array("Vision" => "v", "Goal" => "g", "Target" => "t");

                        $cat_name = array_search($cat_id, $a);//\DB::select(\DB::raw("select name from objective_type where id=$cat_id"));


                        if (isset($cat_name)) {
                            // var_dump($cat_name[0]->name);

                            $strategylist1->category_name = $cat_name;
                        }

                    } else {

                        $strategylist1->category_name = NULL;

                    }
                    // $strategylist1->sector_name=$sectorname;

                    $tenant_name = \DB::select(\DB::raw("select name from tenant where id=$tenant_id"));
                    $strategylist1->tenant_name = $tenant_name[0]->name;

                }

                return response()->json([
                    "code" => 200,
                    "data" => $strategylist11
                ]);
            }
        } else {
            return response()->json([
                "code" => 200,
                "data" => []
            ]);
        }

    }


    public function StrategyListTableSector(Request $request)//filter by sector
    {
        $parent_id = $request->parent;
        $strategylist11 = \DB::table("org_objectives")
            ->select('org_objectives.id', 'org_objectives.tenant_id', 'org_objectives.sector_id', 'org_objectives.subtenant_id', 'org_objectives.objective_id', 'org_objectives.obj_cost_pct')
            ->where('org_objectives.sector_id', $parent_id)
            ->get();
        if ($strategylist11) {
            foreach ($strategylist11 as $strategylist1) {
                $subtenant_id = $strategylist1->subtenant_id;
                $sector_id = $strategylist1->sector_id;
                $tenant_id = $strategylist1->tenant_id;

                if ($subtenant_id != NULL) {
                    $sub_name = \DB::select(\DB::raw("select name  from subtenant where id=$subtenant_id;"));
                }

                if ($sector_id != null)
                    $sector_name = \DB::select(\DB::raw("select name from subtenant s where s.tenant_id=$tenant_id and id=$sector_id"));

                $tenant_name = \DB::select(\DB::raw("select name from tenant s where s.id=$tenant_id"));

                $subtenantname = $subtenant_id != null ? $sub_name[0]->name : "";
                $sectorname = $sector_id != null ? $sector_name[0]->name : "";
                $tenantname = $tenant_name[0]->name;
                if ($subtenant_id != $sector_id) {
                    $strategylist1->subtenant_name = $subtenantname;
                    $strategylist1->sector_name = $sectorname;
                    $strategylist1->tenant_name = $tenantname;

                }
                if ($subtenant_id == $sector_id) {
                    $strategylist1->subtenant_name = "";
                    $strategylist1->sector_name = $sectorname;
                    $strategylist1->tenant_name = $tenantname;

                }
            }
        }
        if ($strategylist11) {
            return response()->json([
                "code" => 200,
                "data" => $strategylist11
            ]);
        }
        if (!$strategylist11) {
            return response()->json([
                "code" => 200,
                "data" => []
            ]);
        }


    }


    public function StrategyListTableOrg(Request $request)
    {
        $parent_id = $request->parent;
        $strategylist11 = \DB::table("org_objectives")
            ->select('org_objectives.id', 'org_objectives.tenant_id', 'org_objectives.sector_id', 'org_objectives.subtenant_id', 'org_objectives.objective_id', 'org_objectives.obj_cost_pct')
            ->where('org_objectives.subtenant_id', $parent_id)
            ->get();
        if ($strategylist11) {
            foreach ($strategylist11 as $strategylist1) {
                $subtenant_id = $strategylist1->subtenant_id;
                $sector_id = $strategylist1->sector_id;
                $tenant_id = $strategylist1->tenant_id;

                if ($subtenant_id != NULL) {
                    $sub_name = \DB::select(\DB::raw("select name  from subtenant where id=$subtenant_id;"));
                }

                if ($sector_id != null)
                    $sector_name = \DB::select(\DB::raw("select name from subtenant s where s.tenant_id=$tenant_id and id=$sector_id"));

                $tenant_name = \DB::select(\DB::raw("select name from tenant s where s.id=$tenant_id"));

                $subtenantname = $subtenant_id != null ? $sub_name[0]->name : "";
                $sectorname = $sector_id != null ? $sector_name[0]->name : "";
                $tenantname = $tenant_name[0]->name;
                if ($subtenant_id != $sector_id) {
                    $strategylist1->subtenant_name = $subtenantname;
                    $strategylist1->sector_name = $sectorname;
                    $strategylist1->tenant_name = $tenantname;

                }
                if ($subtenant_id == $sector_id) {
                    $strategylist1->subtenant_name = "";
                    $strategylist1->sector_name = $sectorname;
                    $strategylist1->tenant_name = $tenantname;

                }
            }
        }
        if ($strategylist11) {
            return response()->json([
                "code" => 200,
                "data" => $strategylist11
            ]);
        }
        if (!$strategylist11) {
            return response()->json([
                "code" => 200,
                "data" => []
            ]);
        }


    }


    public function loadTenants()
    {
        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3"));*/
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }

    public function loadObjectives()
    {
        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3"));*/
        $tenants = \DB::select(\DB::raw("select id, name from objective s where s.tenant_id=1"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }

    public function loadObjcat()
    {
        $id = env('TENANT_ID');

        $tenants = \DB::select(\DB::raw("select id, name,objective_type from objective s where s.tenant_id=$id"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }


    public function loadSubTenants($id)
    {
        //$subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), cast(id as char(200)) from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '-'), '>', s.name), s.parent_id, CONCAT(c.level, '-'), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id) select id, name from cte order by path"));
        $subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id) select id, name from cte order by path"));
        if ($subtenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $subtenants
            ]);
        }
    }


    public function loadlinkSubTenants($id)
    {
        //  $sector_id= collect(\DB::select(\DB::raw("select sector_id  from strategy_subtenant_rel where strategy_id=$id;")));

        // $sid=$sector_id[0]->sector_id;
        // $sector_name= \DB::select(\DB::raw("select name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3 and s.id=$sid"));

        $subtenants = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id) select id, name from cte order by path"));
        if ($subtenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $subtenants
            ]);
        }
    }


    public function loadlinkTenants()
    {
        /*$tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id=3"));*/
        $tenants = \DB::select(\DB::raw("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }


    public function loadlinkTenantsreal()
    {
        $id = env('TENANT_ID');
        $tenants = \DB::select(\DB::raw("select id, name from tenant where id=$id"));
        if ($tenants) {
            return response()->json([
                "code" => 200,
                "tenants" => $tenants
            ]);
        }
    }


    public function loadCategory()
    {
        // $a=array("Vision"=>"v","Goal"=>"g","Target"=>"t");

        // $cat_name= array_search($cat_id,$a);//\DB::select(\DB::raw("select name from objective_type where id=$cat_id"));
        $kpiCat = array(
            array(
                "id" => "v",
                "name" => "Vision",
                "name_ar" => "رؤية"
            ),
            array(
                "id" => "g",
                "name" => "Goal",
                "name_ar" => "هدف"
            ),
            array(
                "id" => "t",
                "name" => "Target",
                "name_ar" => "غاية"
            )
        );
        // $kpiCat = \DB::select(\DB::raw("select id, name from objective_type"));
        if ($kpiCat) {
            return response()->json([
                "code" => 200,
                "kpiCat" => $kpiCat
            ]);
        }
    }

    public function store(Request $request)
    {
        $sum_id = 0;
        // var_dump("aaa");
        $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where objective_id=$request->objective;")))->first();
        if ($user_info)
            $sum_id = $user_info->sum;

        $sum = $sum_id + $request->obj_cost_pct;
        // var_dump($sum);

        if ($sum <= 1) {
            $prc_update = \DB::table("org_objectives")->insertGetId(//insert(
                [
                    'tenant_id' => env('TENANT_ID'),
                    'sector_id' => $request->linkSector,
                    'subtenant_id' => $request->linkOrg,
                    'objective_id' => $request->objective,
                    'obj_cost_pct' => $request->obj_cost_pct,


                ]);
            if ($prctype) {
                return response()->json([
                    "code" => 200,
                    // "msg" => "تم إنشاء طلب الإجازة"
                ]);
            } else {
                return response()->json([
                    "code" => 201,
                    // "msg" => "تم إنشاء طلب الإجازة"
                ]);

            }
        } else {
            return response()->json([
                "code" => 202,
                "msg" => "objective cost issue"
            ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // var_dump($id);

        $ob_id = \DB::select(\DB::raw("select objective_id from org_objectives where id=$id;"));
    //    var_dump($ob_id);

        // if($ob_id){
        $strategy_info = \DB::table("objective")
            ->select('org_objectives.id', 'objective.name', 'objective.objective_type', 'objective.tenant_id', 'org_objectives.obj_cost_pct', 'org_objectives.sector_id', 'org_objectives.subtenant_id', 'org_objectives.objective_id')
            ->join('org_objectives', 'org_objectives.objective_id', '=', 'objective.id')
            ->where('org_objectives.id', $id)
            ->first();
        // }
            // else
            // {
            //     $strategy_info = \DB::table("org_objectives")
            //     ->select('org_objectives.id', 'org_objectives.obj_cost_pct', 'org_objectives.sector_id', 'org_objectives.subtenant_id')
            //     //->join('org_objectives', 'org_objectives.objective_id', '=', 'objective.id')
            //     ->where('org_objectives.id', $id)
            //     ->first();

            // }

        $subtenant_id = $strategy_info->subtenant_id;
        $sector_id = $strategy_info->sector_id;
        $tenant_id = $strategy_info->tenant_id;

        if ($subtenant_id != NULL) {
            $sub_name = \DB::select(\DB::raw("select name  from subtenant where id=$subtenant_id;"));
        }

        if ($sector_id != null)
            $sector_name = \DB::select(\DB::raw("select name from subtenant s where s.tenant_id=$tenant_id and id=$sector_id"));

        $tenant_name = \DB::select(\DB::raw("select name from tenant s where s.id=$tenant_id"));

        $subtenantname = $subtenant_id != null ? $sub_name[0]->name : "";
        $sectorname = $sector_id != null ? $sector_name[0]->name : "";
        $tenantname = $tenant_name[0]->name;


        $strategy = [];
        if ($strategy_info) {
            /*echo $process_info->date_from;
            die;*/
            $strategy['id'] = $strategy_info->id;
            $strategy['name'] = $strategy_info->name;
            $strategy['cat'] = $strategy_info->objective_type;
            $strategy['linkTenant'] = $tenantname;
            $strategy['linkSector'] = $sector_id;//$sectorname ;
            $strategy['linkOrg'] = $subtenant_id;
            $strategy['objective'] = $strategy_info->objective_id;
            $strategy['obj_cost_pct'] = $strategy_info->obj_cost_pct;


            return response()->json([
                "code" => 200,
                "data" => $strategy,
            ]);
        } else {
            return response()->json([
                "code" => 201,
                // "msg" => "غير موجود"
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) //edit org
    {

        $cid = $request->cat;

        if ($cid == "v") {
            $prc_update = \DB::table("org_objectives")
                ->where('id', $id)
                ->update([
                    'tenant_id' => env('TENANT_ID'),
                    'sector_id' => NULL,
                    'subtenant_id' => NULL,
                    'objective_id' => $request->objective,
                    'obj_cost_pct' => $request->obj_cost_pct,

                ]);
        }
        if ($cid == "t") {
            $prc_update = \DB::table("org_objectives")
                ->where('id', $id)
                ->update([
                    'tenant_id' => env('TENANT_ID'),
                    'sector_id' => $request->linkSector,
                    'subtenant_id' => $request->linkSector,
                    'objective_id' => $request->objective,
                    'obj_cost_pct' => $request->obj_cost_pct,

                ]);


        }
        if ($cid == "g") {
            $prc_update = \DB::table("org_objectives")
                ->where('id', $id)
                ->update([
                    'tenant_id' => env('TENANT_ID'),
                    'sector_id' => $request->linkSector,
                    'subtenant_id' => $request->linkOrg,
                    'objective_id' => $request->objective,
                    'obj_cost_pct' => $request->obj_cost_pct,

                ]);

        }
        if ($prc_update) {
            // $prc_update->save();

            return response()->json([
                "code" => 200,
                // "msg" =>'updated'// 'تم تعديل طلب الإجازة'
            ]);

        } else {
            return response()->json([
                "code" => 201,
                // "msg" => 'not updated'//'لا يمكن الحفظ لأن البيانات تم تعديلها من قبل مستخدم آخر, رجاء إعادة فتح الصفحة'
            ]);

        }
    }


    public function destroy($id) //delete org

    {

        $prctype1 = \DB::table("org_objectives")->Where('objective_id', $id)->delete();
        $prctype = \DB::table("objective")->Where('id', $id)->delete();

        if (!$prctype) {
            return response()->json([
                "code" => 404,
                // "msg" => "not deleted"//"لا يمكن حذف طلب الإجازة"
            ]);
        }

        // $prctype->delete();

        return response()->json([
            "code" => 200,
            // "msg" =>"deleted" //"تم حذف طلب الإجازة"
        ]);


    }

//link part

    public function linkedlist()
    {
        $strategylist11 = \DB::table("org_objectives")
            ->select('org_objectives.id', 'org_objectives.tenant_id', 'org_objectives.sector_id', 'org_objectives.subtenant_id', 'org_objectives.objective_id', 'org_objectives.obj_cost_pct', 'objective.objective_type')
            //->select('org_objectives.id','org_objectives.tenant_id','org_objectives.sector_id','org_objectives.subtenant_id')
            // ->distinct('org_objectives.sector_id','org_objectives.subtenant_id')
            ->join('objective', 'org_objectives.objective_id', '=', 'objective.id')
            ->get();
        if ($strategylist11) {
            foreach ($strategylist11 as $strategylist1) {
                $subtenant_id = $strategylist1->subtenant_id;
                $sector_id = $strategylist1->sector_id;
                $tenant_id = $strategylist1->tenant_id;

                if ($subtenant_id != NULL) {
                    $sub_name = \DB::select(\DB::raw("select name  from subtenant where id=$subtenant_id;"));
                }

                if ($sector_id != null)
                    $sector_name = \DB::select(\DB::raw("select name from subtenant s where s.tenant_id=$tenant_id and id=$sector_id"));

                $tenant_name = \DB::select(\DB::raw("select name from tenant s where s.id=$tenant_id"));

                $subtenantname = $subtenant_id != null ? $sub_name[0]->name : "";
                $sectorname = $sector_id != null ? $sector_name[0]->name : "";
                $tenantname = $tenant_name[0]->name;
                if ($subtenant_id != $sector_id) {
                    $strategylist1->subtenant_name = $subtenantname;
                    $strategylist1->sector_name = $sectorname;
                    $strategylist1->tenant_name = $tenantname;

                }
                if ($subtenant_id == $sector_id) {
                    $strategylist1->subtenant_name = "";
                    $strategylist1->sector_name = $sectorname;
                    $strategylist1->tenant_name = $tenantname;

                }
            }
        }
        if ($strategylist11) {
            return response()->json([
                "code" => 200,
                "data" => $strategylist11
            ]);
        }
        if (!$strategylist11) {
            return response()->json([
                "code" => 200,
                "data" => []
            ]);
        }


    }


    public function store1(Request $request, $id) //create strategy
    {
        $sum_id = 0;
        //  var_dump("aaa");

        $obj = \DB::select(\DB::raw("select * from org_objectives where id=$id"));
        if ($obj) {
            $sid = $obj[0]->sector_id;
            $oid = $obj[0]->subtenant_id;
            $tid = env('TENANT_ID');

            if ($oid != NULL && $sid != NULL)
            $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where sector_id=$sid and subtenant_id=$oid and tenant_id=$tid;")))->first();

        if (($sid != NULL) && ($oid == NULL))
            $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where sector_id=$sid  and tenant_id=$tid;")))->first();

        if (($sid == NULL) && ($oid == NULL))
            $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where tenant_id=$tid;")))->first();

            if (($sid == NULL) && ($oid != NULL))
            $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where subtenant_id=$oid  and tenant_id=$tid;")))->first();

            // $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where sector_id=$sid and subtenant_id=$oid;")))->first();
            if ($user_info)
                $sum_id = $user_info->sum;

            $sum = $sum_id + $request->obj_cost_pct;
            //var_dump($sum);

            if ($sum <= 1) {


                $user_info = collect(\DB::select(\DB::raw("select 1, max(cast(id as integer))+1 as id from objective;")))->first();
                $obj_id = $user_info->id;
                $prctype = \DB::table("objective")->insert(//insert(
                    [
                        'id' => $obj_id,
                        'name' => $request->StrategyName,
                        'objective_type' => $request->Category,
                        'tenant_id' => env('TENANT_ID'),//1,
                        'parent_id' => NULL,


                    ]
                );

                if ($prctype) {

                    $prc_update = \DB::table("org_objectives")->insertGetId(//insert(
                        [
                            'tenant_id' => env('TENANT_ID'),
                            'sector_id' => $sid,
                            'subtenant_id' => $oid,
                            'objective_id' => $obj_id,
                            'obj_cost_pct' => $request->obj_cost_pct,


                        ]

                    );
                    if ($prc_update) {
                        // $prc_update->save();

                        return response()->json([
                            "code" => 200,
                            // "msg" =>'updated'// 'تم تعديل طلب الإجازة'
                        ]);

                    } else {
                        return response()->json([
                            "code" => 201,
                            // "msg" => 'not updated'//'لا يمكن الحفظ لأن البيانات تم تعديلها من قبل مستخدم آخر, رجاء إعادة فتح الصفحة'
                        ]);

                    }
                } else {
                    return response()->json([
                        "code" => 201,
                        // "msg" => "تم إنشاء طلب الإجازة"
                    ]);

                }
            }


        }
    }


    public function update1(Request $request, $id) //create org
    {
        $sum_id = 0;
        $sid = $request->linkSector;
        $oid = $request->linkOrg;
        $tid = env('TENANT_ID');
        if ($oid != "" && $sid != "")
            $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where sector_id=$sid and subtenant_id=$oid and tenant_id=$tid;")))->first();

        if (($sid != "") && ($oid == ""))
            $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where sector_id=$sid  and tenant_id=$tid;")))->first();

        if (($sid == "") && ($oid == ""))
            $user_info = collect(\DB::select(\DB::raw("select sum(obj_cost_pct) as sum from org_objectives where tenant_id=$tid;")))->first();

// var_dump($user_info);
        if ($user_info)
            $sum_id = $user_info->sum;

        $sum = $sum_id + $request->obj_cost_pct;
        //var_dump($sum);


        if ($sum <= 1) {

            $category_id = collect(\DB::select(\DB::raw("select objective_type  from objective where id=$request->objective;")));

            $cid = $category_id[0]->objective_type;
            // var_dump($cid);
            if ($cid == "v")
                $prctype = \DB::table("org_objectives")->insertGetId(//insert(
                    [
                        'tenant_id' => env('TENANT_ID'),
                        'sector_id' => null,
                        'subtenant_id' => null,
                        'objective_id' => $id,
                        'obj_cost_pct' => $request->obj_cost_pct,


                    ]
                );
            if ($cid == "t")
                $prctype = \DB::table("org_objectives")->insertGetId(//insert(
                    [
                        'tenant_id' => env('TENANT_ID'),
                        'sector_id' => $request->linkSector,
                        'subtenant_id' => $request->linkSector,
                        'objective_id' => $id,
                        'obj_cost_pct' => $request->obj_cost_pct,


                    ]
                );
            if ($cid == "g")
                $prctype = \DB::table("org_objectives")->insertGetId(//insert(
                    [
                        'tenant_id' => env('TENANT_ID'),
                        'sector_id' => $request->linkSector,
                        'subtenant_id' => $request->linkOrg,
                        'objective_id' => $id,
                        'obj_cost_pct' => $request->obj_cost_pct,


                    ]
                );

            if ($prctype) {

                return response()->json([
                    "code" => 200,
                    // "msg" => "تم إنشاء طلب الإجازة"
                ]);
            }
        } else {
            return response()->json([
                "code" => 201,
                "msg" => "distribution greater than 1"
            ]);

        }

    }

    public function destroy1(Request $request) //delete strategy

    {

        $id = $request->linkId;

        $prctype = \DB::table("org_objectives")->Where('id', $id)->delete();
        // var_dump($sid.$tid.$oid);

        if (!$prctype) {
            return response()->json([
                "code" => 404,
                // "msg" => "لا يمكن حذف طلب الإجازة"
            ]);
        } else {
            return response()->json([
                "code" => 200,
                // "msg" => "تم حذف طلب الإجازة"
            ]);


        }
    }


}
