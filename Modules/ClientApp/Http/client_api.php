<?php
use App\Http\Controllers\MtpController;
use App\Http\Controllers\FiscalyearController;

//Route::get('auth', 'Auth\LoginController@index');

Route::post('/auth', 'Auth\LoginController@NormalLogin');
Route::post('/auth/password/reset/request', "Auth\ForgotPasswordController@resetSendEmail");
Route::post('/auth/password/reset/', "Auth\ResetPasswordController@resetPass");

Route::group(['middleware' => ['auth.jwt']], function () {

    /** Fiscal Years */
    Route::get("loadfiscalyeardata", "FiscalyearController@index");
    Route::post("loadfiscalyeardata", "FiscalyearController@store");
    Route::get("loadfiscalyeardata/{id}", "FiscalyearController@show");
    Route::post("loadfiscalyeardata/{id}", "FiscalyearController@update");
    Route::delete("loadfiscalyeardata/{id}", "FiscalyearController@destroy");

    /** MTP */
    Route::get("loadmtpdata", "MtpController@index");
    Route::post("loadmtpdata", "MtpController@store");
    Route::get("loadmtpdata/{id}", "MtpController@show");
    Route::post("loadmtpdata/{id}", "MtpController@update");
    Route::delete("loadmtpdata/{id}", "MtpController@destroy");
    Route::get("loadmtpfiscaldata/{id}", "MtpController@loadmtpfiscaldata");

    Route::get('/test123', "UserController@test123");
    Route::get('/updateBranch/{role}/{user}', "UserController@updateBranch");
    Route::get('/', 'ClientAppController@index');
    Route::get("/auth/{id}", "HomeController@authUser");
    Route::get("/getApps", "UserController@getApps");
    Route::get("/user/{id}/profile", "UserController@getProfile");//->middleware('permissions:read|1');
    Route::post("/user/{id}/profile", "UserController@updateProfile");//->middleware('permissions:update|1');
    Route::put("/user/{id}/credentials", "UserController@changeuserPassword");//->middleware('permissions:update|1');
    Route::get("/user/{id}/getsectorg", "UserController@getsectorg");//->middleware('permissions:read|1');

    /** User */
    Route::get("user", "UserController@index");//->middleware('permissions:read|1');
    Route::get("/loadKpiSectorUsers/{sector}/{orgUnit}", "UserController@loadKpiSectorUsers");
    Route::get("/loadKpiOrgUsers/{orgUnit}/{sector}", "UserController@loadKpiOrgUsers");
    Route::get("/user/{id}", "UserController@show");//->middleware('permissions:read|1');
    Route::post("user", "UserController@store");//->middleware('permissions:create|1');
    Route::put("user/{id}", "UserController@update");//->middleware('permissions:update|1');
    Route::delete("user/{id}", "UserController@destroy");//->middleware('permissions:delete|1');
    Route::get("allGroup", "UserController@getGroups");//->middleware('permissions:read|2');
    Route::get("allTenant", "UserController@getTenants");//->middleware('permissions:read|11');
    /** Tenant */
    Route::get("tenant", "TenantController@index");//->middleware('permissions:read|11');
    Route::get("/tenant/{id}", "TenantController@show");//->middleware('permissions:read|11,dynamic');
    Route::post("tenant", "TenantController@store");//->middleware('permissions:create|11');
    Route::put("tenant/{id}", "TenantController@update");//->middleware('permissions:update|11');
    Route::delete("tenant/{id}", "TenantController@destroy");//->middleware('permissions:delete|11');
    Route::get("tenantdata", "TenantController@tenantdata");//->middleware('permissions:read|11');
    Route::get("tenantdataorg/{orgdata}", "TenantController@tenantdataorg");//->middleware('permissions:read|11');
    Route::get("/mindmapdata/{id}/{linkflag}", "mindmapcontroller@mindmapdata");//->middleware('permissions:read|11');
    Route::get("/loadSection/{id}", "mindmapcontroller@loadSection");
    /** Group */
    Route::get("group", "GroupController@index");//->middleware('permissions:read|2');
    Route::get("/group/{id}", "GroupController@show");//->middleware('permissions:read|2');
    Route::post("group", "GroupController@store");//->middleware('permissions:create|2');
    Route::put("group/{id}", "GroupController@update");//->middleware('permissions:update|2');
    Route::delete("group/{id}", "GroupController@destroy");//->middleware('permissions:delete|2');
    /** Group Type */
    Route::get("groupType", "GroupTypeController@index");//->middleware('permissions:read|2');
    Route::get("groupType/{id}", "GroupTypeController@show");//->middleware('permissions:read|2');
    /** Task */
    Route::get("task", "UserController@index");//->middleware('permissions:read|7');
    Route::get("/task/{id}", "UserController@show");//->middleware('permissions:read|7');
    Route::post("task", "UserController@store");//->middleware('permissions:create|7');
    Route::put("task/{id}", "UserController@update");//->middleware('permissions:update|7');
    Route::delete("task/{id}", "UserController@destroy");//->middleware('permissions:delete|7');
    /** Process */
    Route::get("process", "ProcessController@index");//->middleware('permissions:read|6');
    Route::get("/process/{id}", "ProcessController@show");//->middleware('permissions:read|6');
    Route::post("process", "ProcessController@store");//->middleware('permissions:read|6');
    Route::put("process/{id}", "ProcessController@update");//->middleware('permissions:read|6');
    Route::delete("process/{id}", "ProcessController@destroy");//->middleware('permissions:read|6');
    Route::post('process/{id}/setting', 'ProcessController@setProcessSetting');//->middleware('permissions:create|6');
    Route::get('process/{id}/setting', 'ProcessController@getProcessSetting');//->middleware('permissions:create|6');
    Route::get('process/{id}/task', 'ProcessController@processTasks');//->middleware('permissions:read|6');
    Route::post('process/{key}/instance', 'ProcessController@startProcessInstance');//->middleware('permissions:read|6');

    Route::get('process-definition/{id}/xml', 'ProcessController@getProcessDefinationXML');//->middleware('permissions:read|6');

    Route::get('process-definition/{id}/count', 'ProcessController@processDefinitionCount');//->middleware('permissions:read|6');

    Route::get('process-definition/{id}/statistics', 'ProcessController@getProcessDefinationStatistics');//->middleware('permissions:read|6');

    Route::get('process-instance-history/{id}', 'ProcessController@getHistoryProcessDefination');//->middleware('permissions:read|6');

    Route::get('processinstance/{id}', 'ProcessController@getProcessInstanceDetails');//->middleware('permissions:read|6');

    Route::get('singleprocessinstance/{id}', 'ProcessController@getSingleProcessInstance');//->middleware('permissions:read|6');

    Route::get('process-instance/{id}/activity-instance', 'ProcessController@getProcessActiveInstance');//->middleware('permissions:read|6');

    /** PRC Types */
    Route::get("PRCTypes", "PRCTypeController@index");
    Route::post("PRCTypes", "PRCTypeController@store");
    Route::get("PRCTypes/{id}", "PRCTypeController@show");
    Route::post("PRCTypes/{id}", "PRCTypeController@update");
    Route::delete("PRCTypes/{id}", "PRCTypeController@destroy");
    /** Priority Types */
    Route::get("PriorityType", "PriorityTypeController@index");
    Route::post("PriorityType", "PriorityTypeController@store");
    Route::get("PriorityType/{id}", "PriorityTypeController@show");
    Route::post("PriorityType/{id}", "PriorityTypeController@update");
    Route::delete("PriorityType/{id}", "PriorityTypeController@destroy");

    /** Permissions */
    Route::get("permission", "UserController@index");//->middleware('permissions:read|0|permissions');
    Route::get("/permission/{id}", "UserController@show");//->middleware('permissions:read|0|permissions');
    Route::post("permission", "UserController@store");//->middleware('permissions:create|0|permissions');
    Route::put("permission/{id}", "UserController@update");//->middleware('permissions:update|0|permissions');
    Route::delete("permission/{id}", "UserController@destroy");//->middleware('permissions:delete|0|permissions');
    /** Vacation */
    Route::get("vacation", "UserController@index");//->middleware('permissions:read|0');
    Route::get("/vacation/{id}", "UserController@show");//->middleware('permissions:read|0');
    Route::post("vacation", "UserController@store");//->middleware('permissions:create|0');
    Route::put("vacation/{id}", "UserController@update");//->middleware('permissions:update|0');
    Route::delete("vacation/{id}", "UserController@destroy");//->middleware('permissions:delete|0');

    Route::post('group_tasks', 'TaskController@groupTasks');
    Route::post('user_tasks', 'TaskController@userTasks');
    Route::post('user_task_details', 'TaskController@taskDetails');

    Route::post('claim_task', 'TaskController@cliamTask');
    Route::post('unclaim_task', 'TaskController@unClaimTask');
    Route::post('add_identity_link', 'TaskController@addIdentityLink');
    Route::post('remove_identity_link', 'TaskController@removeIdentityLink');

    Route::post('set_follow_up_date', 'TaskController@setFollowUpDate');
    Route::post('set_due_date', 'TaskController@setDueDate');
    Route::post('submit_task_form', 'TaskController@submitTaskForm');

    // Route::post('get_comment_list', 'TaskController@getCommentList');
    Route::post('create_comment_list', 'TaskController@createComment');

    /** Priority Types */
    Route::get("TaskTodos/{taskid}", "TaskTodosController@index");
    Route::post("TaskTodos", "TaskTodosController@store");
    Route::get("TaskTodos/{taskid}/{id}", "TaskTodosController@show");
    Route::put("TaskTodos/{id}", "TaskTodosController@update");
    Route::delete("TaskTodos/{todo_id}", "TaskTodosController@destroy");

    /** Roles */
    Route::get("roles", "RolesController@index");
    Route::get("getPermissions", "RolesController@getPermissions");
    Route::get("/roles/{id}", "RolesController@show");
    Route::post("roles", "RolesController@store");
    Route::put("roles/{id}", "RolesController@update");
    Route::delete("roles/{id}", "RolesController@destroy");
    Route::get("rolesObject", "RolesController@getRoleObject");
    Route::get("rolesList", "UserController@getRoleList");
    Route::get("rolespermissions/{perm}", "RolesController@rolespermissions");

    /** Roles */
    Route::get("object_models", "ObjectModelController@index");
    Route::get("/object_models/{id}", "ObjectModelController@show");
    Route::post("object_models", "ObjectModelController@store");
    Route::put("object_models/{id}", "ObjectModelController@update");
    Route::delete("object_models/{id}", "ObjectModelController@destroy");
    //Route::get("object_models" , "RolesController@getRoleList");


    /** Roles */
    Route::get("fg_form", "formGeneratorController@index");
    Route::get("fg_form/{id}/{kpi_id}", "formGeneratorController@getForm");
    Route::post("fg_form", "formGeneratorController@store");

    Route::get("/kpidefList", "formGeneratorController@kpidefList");
    Route::get("/kpidefListHistory/{start_date}/{end_date}", "formGeneratorController@kpidefListHistory");

    Route::get("/fetchCitiesForCountry/{id}", "formGeneratorController@fetchCitiesForCountry");
    Route::post("/fetchSectorChild/{id}", "formGeneratorController@fetchSectorChild");
    Route::get("/fetchUserData/{id}/{type}", "formGeneratorController@fetchUserData");
    Route::get("/fetchBenchMarkData/{id}", "formGeneratorController@fetchBenchMarkData");
    Route::post("/fetchProcessObject/{id}", "formGeneratorController@fetchProcessObject");

    Route::get("/fg_form_tabledata/{table}", "formGeneratorController@gettabledata");
    Route::get("/fg_form_tabledatabyName/{table}", "formGeneratorController@gettabledatabyName");
    Route::get("/tabledatabyId/{formId}/{id}", "formGeneratorController@tabledatabyId");
    Route::get("/tabledatabyIdagain/{formId}/{id}", "formGeneratorController@tabledatabyIdagain");
    Route::post("/tableeditadataid/{formId}/{dataid}/{data}", "formGeneratorController@tableeditadataid");
    Route::delete("tabledatadelete/{formId}/{id}", "formGeneratorController@destroy");

    Route::get("/loadTenants", "formGeneratorController@loadTenants");
    Route::get("/loadSubTenants/{id}", "formGeneratorController@loadSubTenants");
    Route::get("/loadCategory", "formGeneratorController@loadCategory");
    Route::get("/loadMtp", "formGeneratorController@loadMtp");
    Route::get("/loadMtpFiscalYear/{mtp_id}", "formGeneratorController@loadMtpFiscalYear");
    Route::get("/loadMtpAndDefault", "formGeneratorController@loadMtpAndDefault");
    Route::get("/loadfiscal/{mtpstart_date}/{mtpend_date}", "formGeneratorController@loadFiscal");
    Route::get("organizationchart", "organizationchart@index");

    Route::get("tables", "translationController@index");
    Route::get("gettablecolumns/{tablename}", "translationController@gettablecolumns");
    Route::post("Translations", "translationController@store");
    Route::get("loadtranslations", "translationController@loadtranslations");
    Route::get("translationdatabyId/{id}", "translationController@translationdatabyId");
    Route::get("gettranslations", "translationController@gettranslations");
    Route::delete("translationdatadelete/{id}", "translationController@destroy");

    Route::post("/kpiApproveReject", "formGeneratorController@kpiApproveReject");
    Route::post("/kpiActiveInactive", "formGeneratorController@kpiActiveInactive");
    Route::get("kpiexceptionlist/{id}", "formGeneratorController@kpiexceptionlist");
    Route::get("kpivalues/{id}/{showfuture}", "formGeneratorController@kpivalues");
    Route::get("kpivaluesremark/{id}/{showfuture}", "formGeneratorController@kpivaluesremark");
    Route::get("kpivalueshistory/{id}", "formGeneratorController@kpivalueshistory");
    Route::get("kpivaluesbyId/{id}", "formGeneratorController@kpivaluesbyId");
    Route::post("/kpivaluesUpdate", "formGeneratorController@kpivaluesUpdate");
    Route::post("/kpivalueshistorysave", "formGeneratorController@storehistory");
    Route::post("/kpibaseyvaluesave", "formGeneratorController@kpibaseyvaluesave");
    Route::get("/kpivaluetypechangecheck/{id}", "formGeneratorController@kpivaluetypechangecheck");
    Route::post("/kpivaluesdelete/{id}/{val}", "formGeneratorController@kpivaluesdelete");
    Route::delete("/removeKpiPermanently/{id}", "formGeneratorController@removeKpiPermanently");


    Route::get("gaugechart/{mtp_date}/{kpi_id}/{periodicity}", "GaugeController@index");
    Route::get("quartermap/{target_id}/{periodicity}/{valType}/{mtp_date}/{kpi_id}", "GaugeController@quarterMap");
    Route::get("mtp", "GaugeController@mtp");
    Route::get("loadkpi/{id}", "GaugeController@loadkpi");
    Route::get("loadkpisymbol/{id}", "GaugeController@loadkpisymbol");
    Route::get("getPeriodId/{id}", "GaugeController@getPeriodId");


    Route::get("mtpDashboard", "DashboardKPIController@mtp");
    Route::get("dashboardVal/{mtp_date}/{sector}", "DashboardKPIController@dashboardVal");
    Route::get("dashboardTableVal/{mtp_date}/{sector}", "DashboardKPIController@dashboardTableVal");
    Route::get("staticdashboardTableVal/{mtp_date}/{sector}/{getAll}", "DashboardKPIController@staticdashboardTableVal");
    //Route::get("/loadTenants" , "DashboardKPIController@loadTenants");
    //Route::get("/loadSubTenants/{id}" , "DashboardKPIController@loadSubTenants");
    Route::get("/loadKpiDataSector/{id}", "formGeneratorController@loadKpiDataSector");
    Route::get("/loadKpiDataOrgUnit/{id}", "formGeneratorController@loadKpiDataOrgUnit");
    Route::get("/getUserOfData/{id}", "formGeneratorController@getUserOfData");
    Route::get("/getUserOfAuditing/{id}", "formGeneratorController@getUserOfAuditing");

    Route::get("loginlogs", "loginlogController@index");
    Route::get("/logloadTenants", "loginlogController@loadTenants");
    Route::get("/logloadSubTenants/{id}", "loginlogController@loadSubTenants");
    Route::get("/loadloginlogDataSector/{id}", "loginlogController@loadloginlogDataSector");
    Route::get("/loadloginlogDataOrgUnit/{id}", "loginlogController@loadloginlogDataOrgUnit");
    Route::get("/loadloginDataDate/{id}", "loginlogController@loadloginDataDate");

    Route::get("audittrial", "audittrialController@index");
    Route::get("/logloadTenants", "audittrialController@loadTenants");
    Route::get("/logloadSubTenants/{id}", "audittrialController@loadSubTenants");
    Route::get("/loadusers", "audittrialController@loadusers");
    Route::get("/loadscreens", "audittrialController@loadscreens");
    Route::get("/loadaudittrialDataSector/{id}", "audittrialController@loadaudittrialDataSector");
    Route::get("/loadaudittrialDataOrgUnit/{id}", "audittrialController@loadaudittrialDataOrgUnit");
    Route::get("/loadaudittrialDataDate/{id}", "audittrialController@loadaudittrialDataDate");
    Route::get("/audittrialfilter/{value}", "audittrialController@audittrialfilter");

//process------------------------------------------------------------------------------------------------------------------------

    Route::get("processlistTableVal", "processlistController@index");

    Route::get("processlistTableValSector/{parent}", "processlistController@ProcessListTableSector");
    Route::get("processlistTableValOrg/{parent}", "processlistController@ProcessListTableOrg");

    Route::get("processlist1/{id}", "processlistController@show");
    Route::post("processlist1", "processlistController@store");
    Route::put("processlist1/{id}", "processlistController@update");
    Route::delete("processlist1/{id}", "processlistController@destroy");

    Route::get("linkedlistTableVal/{prc_id}", "processlistController@linkedlist");
    Route::post("linkedlist1", "processlistController@store1");
    Route::put("linkedlist1/{id}", "processlistController@update1");
    Route::delete("linkedlist1/{linkSector}/{linkOrg}", "processlistController@destroy1");

    Route::get("/prcloadTenants1", "processlistController@loadTenants");
    Route::get("/prcloadCategory1", "processlistController@loadCategory");
    Route::get("/prcloadSubTenants1/{id}", "processlistController@loadSubTenants");
    Route::get("/prcloadSubTenants11/{id}", "processlistController@loadlinkSubTenants");
    Route::get("/prcloadTenants11", "processlistController@loadlinkTenants");
//strategy---------------------------------------------------------------------------------------------------
    Route::get("strategylistTableVal", "strategylistController@index");

    Route::get("strategylistTableValSector/{parent}", "strategylistController@StrategyListTableSector");
    Route::get("strategylistTableValOrg/{parent}", "strategylistController@StrategyListTableOrg");

    Route::get("strategylist1/{id}", "strategylistController@show");
    Route::post("strategylist1", "strategylistController@store");
    Route::put("strategylist1/{id}", "strategylistController@update");
    Route::delete("strategylist1/{id}", "strategylistController@destroy");

    Route::get("strategylinkedlistTableVal/{prc_id}", "strategylistController@linkedlist");
    Route::post("strategylinkedlist1", "strategylistController@store1");
    Route::put("strategylinkedlist1/{id}", "strategylistController@update1");
    Route::delete("strategylinkedlist1/{linkTenant}/{linkSector}/{linkOrg}/{linkId}", "strategylistController@destroy1");

    Route::get("/stgloadTenants1", "strategylistController@loadTenants");
    Route::get("/stgloadCategory1", "strategylistController@loadCategory");
    Route::get("/stgloadSubTenants1/{id}", "strategylistController@loadSubTenants");
    Route::get("/stgloadSubTenants11/{id}", "strategylistController@loadlinkSubTenants");
    Route::get("/stgloadTenants11", "strategylistController@loadlinkTenants");
    Route::get("/stgloadTenantsreal11", "strategylistController@loadlinkTenantsreal");


//strategycostdistribution-------------------------------------------------------------------------------------------------
    Route::get("strategylistTableVal_cd/{prc_id}", "strategycostdistributionlistController@index");
    Route::get("strategylistTableValSector_cd/{parent}", "strategycostdistributionlistController@StrategyListTableSector");
    Route::get("strategylistTableValOrg_cd/{parent}", "strategycostdistributionlistController@StrategyListTableOrg");

    Route::get("strategylist1_cd/{id}", "strategycostdistributionlistController@show");
    Route::put("strategylist11_cd/{id}", "strategycostdistributionlistController@update");
    Route::put("strategylist1_cd/{id}", "strategycostdistributionlistController@store1");
    Route::delete("strategylist1_cd/{id}", "strategycostdistributionlistController@destroy");

    Route::get("strategylinkedlistTableVal_cd", "strategycostdistributionlistController@linkedlist");
    Route::post("strategylinkedlist1_cd", "strategycostdistributionlistController@store");
    Route::put("strategylinkedlist1_cd/{id}", "strategycostdistributionlistController@update1");
    Route::delete("strategylinkedlist1_cd/{linkId}", "strategycostdistributionlistController@destroy1");

    Route::get("/stgloadObjectives_cd", "strategycostdistributionlistController@loadObjectives");
    Route::get("/stgloadTenants1_cd", "strategycostdistributionlistController@loadTenants");
    Route::get("/stgloadCategory1_cd", "strategycostdistributionlistController@loadCategory");

    Route::get("/stgloadSubTenants1_cd/{id}", "strategycostdistributionlistController@loadSubTenants");
    Route::get("/stgloadSubTenants11_cd/{id}", "strategycostdistributionlistController@loadlinkSubTenants");
    Route::get("/stgloadTenants11_cd", "strategycostdistributionlistController@loadlinkTenants");
    Route::get("/stgloadTenantsreal11_cd", "strategycostdistributionlistController@loadlinkTenantsreal");
    Route::get("/stgloadObjcat11_cd", "strategycostdistributionlistController@loadObjcat");


//holiday--------------------------------------------------------------------------------------------------------------

    Route::get("/scrape/{year}", "HolidayController@scrape");
    Route::get("/holiday", "HolidayController@index");
    Route::get("/holiday/{id}", "HolidayController@show");
    Route::post("/holiday", "HolidayController@store");
    Route::put("/holiday/{id}", "HolidayController@update");
    Route::delete("/holiday/{id}", "HolidayController@destroy");


    Route::get("/allholidays", "HolidayController@allholidays");
    Route::get("/years", "HolidayController@years");
    Route::get("/date/{id}/{year}", "HolidayController@date");

//--------------------------------------------------------------------------------------------------------------------

    Route::get("categorylistTableVal", "processcategoryController@index");
    Route::get("categorylist1/{id}", "processcategoryController@show");
    Route::post("categorylist1", "processcategoryController@store");
    Route::put("categorylist1/{id}", "processcategoryController@update");
    Route::delete("categorylist1/{id}", "processcategoryController@destroy");

    Route::get("perfprog", "performanceController@index");
    Route::get("/sectionkpireport/{value}", "performanceController@sectionkpireport");
    Route::get("/departmentreport/{value}", "performanceController@departmentreport");
    Route::get("/loadSubTenantsdept/{id}", "performanceController@loadSubTenants");
    Route::get("/getsubtenanttype/{value}", "performanceController@getsubtenanttype");


    Route::get("/ministrydepartmentreport/{value}", "ministryperformanceController@departmentreport");
    Route::get("/maintenanceData", "maintenanceController@loadMaintenance");
    Route::get("/maintenanceData/{id}/{mtp_id}", "maintenanceController@loadMaintenanceById");
    Route::get("/updatedb_kpivaluestates/{id}/{mtp_id}", "maintenanceController@updatedbKpivaluestates");

    Route::get("/getAllNotification", "formGeneratorController@getAllNotification");
    Route::get("/readNotification/{id}", "formGeneratorController@readNotification");
    Route::get("/subtenanttree/{id}/{from}", "treeselectcController@subtenanttree");

    Route::get("/projectlist", "projectsController@projectlist");
    Route::get("/getprojecttypes", "projectsController@getprojecttypes");
    Route::post("/projectsstore", "projectsController@store");
    Route::post("/projectApproveReject", "projectsController@projectApproveReject");
    Route::get("/loadTenantsministry", "projectsController@loadTenantsministry");
    Route::post("/projectfilter", "projectsController@loadprojectearch");
    Route::get("/projectlistbyId/{id}", "projectsController@projectlistbyId");
    Route::get("loadNotificationDefaultData", "NotificationController@loadNotificationDefaultData");
    Route::post("loadUserDataFromRole", "NotificationController@loadUserDataFromRole");
    Route::post("saveNotification", "NotificationController@saveNotification");
    Route::get("notificationlist", "NotificationController@index");
    Route::get("notificationedit/{id}", "NotificationController@edit");
    Route::get("notificationview/{id}", "NotificationController@notificationview");
    Route::post("notificationactivedeactive", "NotificationController@notificationActiveDeactive");
    Route::post("notificationdelete", "NotificationController@notificationdelete");
    Route::get("selectUser/{id}", "NotificationController@selectUser");

    Route::get("/projectaluesupdatelast/{id}", "projectsController@projectaluesupdatelast");
    Route::post("/projectsvaluesstore", "projectsController@projectsvaluesstore");
    Route::post("/projectoperationalstatus", "projectsController@projectoperationalstatus");
    Route::get("/risklistbyId/{id}", "projectsController@risklistbyId");
    Route::get("/readingslist/{id}", "projectsController@readingslist");
    Route::get("/remarkreadingslist/{id}", "projectsController@remarkreadingslist");
    Route::get("/projectnextupdateddate/{id}", "projectsController@projectnextupdateddate");



    Route::get("/loadKpiOrgUsersNotification/{orgUnit}/{sector}", "NotificationController@loadKpiOrgUsersNotification");
    Route::get("/getNotificationArgs/{event_id}", "NotificationController@getNotificationArgs");

    Route::get("loadNotificationDefaultData1", "riskController@loadNotificationDefaultData");
    Route::get("/loadKpiOrgUsersNotification1/{orgUnit}/{sector}", "riskController@loadKpiOrgUsersNotification");
    Route::get("/risklist", "riskController@risklist");
    Route::get("/loaduserlist", "riskController@userlist");
    Route::get("/loadriskcatlist", "riskController@riskcatlist");

    Route::post("/riskstore", "riskController@store");
    Route::get("/projectlistfilter/{value}", "riskController@loadprojectearch");
    Route::get("risklist1/{id}", "riskController@show");
    Route::put("risklist1/{id}", "riskController@update");
    Route::delete("risklist1/{id}", "riskController@destroy");

    Route::get("loadprojectmanagerDefaultData", "projectsController@loadprojectmanagerDefaultData");
    Route::get("loadstrategicDefaultData", "projectsController@loadstrategicDefaultData");
    Route::get("/loadKpiOrgUserspm/{orgUnit}/{sector}", "projectsController@loadKpiOrgUserspm");
    Route::get("/loadKpiOrgUsersstr/{orgUnit}/{sector}", "projectsController@loadKpiOrgUsersstr");
    Route::get("getorgunitname/{id}", "projectsController@getorgunitname");
    Route::post("saveprojectteam", "projectsController@saveprojectteam");
    Route::get("/getuserroles", "projectsController@getuserroles");
    Route::post("projectcoststore", "projectsController@projectcoststore");
    Route::get("/projectcostlist", "projectsController@projectcostlist");
    Route::get("/projectcostbyId/{id}", "projectsController@projectcostbyId");
    Route::delete("projectcost/{id}", "projectsController@destroy");
    Route::post("/projectcostfilter", "projectsController@loadprojectcostsearch");
    Route::get("selectUserteam/{id}", "projectsController@selectUserteam");
    Route::get("checkteamname/{id}", "projectsController@checkteamname");
    Route::get("loadprojectteamgetbyid/{id}", "projectsController@loadprojectteamgetbyid");
    Route::get("loadprojectteammembergetbyid/{id}", "projectsController@loadprojectteammembergetbyid");
    Route::post("projectdashboardreport", "projectsController@projectdashboardreport");
    Route::post("projectviewdetails", "projectsController@projectviewdetails");
    Route::get("getprojectlist", "projectsController@getprojectlist");
    Route::delete("/removeprojectPermanently/{id}", "projectsController@removeprojectPermanently");
    Route::get("/loadfiscalfrommtp/{mtpid}", "projectsController@loadfiscalfrommtp");
    Route::get("/getprojectreadigdetails/{readingid}", "projectsController@getprojectreadigdetails");
    Route::get("/projectvaluechangecheck/{projectid}", "projectsController@projectvaluechangecheck");

    Route::get("loadSectorOrg/{id}", "formGeneratorController@loadSectorOrg");
    Route::get("getgrades/{id}", "formGeneratorController@getgrades");
    Route::get("getgradebyid/{id}", "formGeneratorController@getgradebyid");

    Route::get("loadSTPDefaultData", "StpController@loadSTPDefaultData");
    Route::get("loadSTPUserFiscalYearBySector/{orgid}/{mtp}/{stpid}/{fy_id}", "StpController@loadSTPUserBySector");
    Route::post("saveStp", "StpController@saveStp");
    Route::get("/loadStpDataSector/{id}", "StpController@loadStpDataSector");
    Route::get("stpeditview/{id}", "StpController@stpeditview");
    Route::get("stpviewonly/{id}", "StpController@stpviewonly");
    Route::delete("/removestpPermanently/{id}", "StpController@removeStpPermanently");
    Route::post("updateEmpCount", "StpController@updateEmpCount");
    Route::post("selectProjects", "StpController@selectProjects");
    Route::post("stpApproveReject", "StpController@stpApproveReject");
    Route::post("stpActiveInactive", "StpController@stpActiveInactive");
    Route::post("saveStpCostUpdation", "StpController@saveStpCostUpdation");
    Route::get("stpviewupdation/{id}", "StpController@stpviewupdation");
    Route::get("loadDefaultSectorOrgMtpFiscal/{orgid}/{mtp}/{period}/{fiscal}/{fromList}", "StpController@loadDefaultSectorOrgMtpFiscal");
    Route::get("loadProcessProjectData/{orgid}/{mtp}/{period}/{fiscal}", "StpController@loadProcessProjectData");
});
Route::get("test", "HomeController@index");
Route::post("test", "HomeController@index");
Route::post("notificationsaveImage", "HomeController@notificationsaveImage");

Route::get("ProjectRiskReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectRiskReportController@index");
Route::post("ProjectRiskReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectRiskReportController@index");

Route::get("ProjectBudgetSummaryReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectBudgetSummaryReportController@index");
Route::post("ProjectBudgetSummaryReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectBudgetSummaryReportController@index");
Route::get("ProjectBudgetDetailsReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectBudgetDetailsReportController@index");
Route::post("ProjectBudgetDetailsReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectBudgetDetailsReportController@index");

// Route::get("ProjectBudgetDetailsReport/{cluster}", "ProjectBudgetDetailsReportController@index");
// Route::post("ProjectBudgetDetailsReport/{cluster}", "ProjectBudgetDetailsReportController@index");

Route::get("ProjectByStatusReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectByStatusReportController@index");
Route::post("ProjectByStatusReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectByStatusReportController@index");

Route::get("KpiStatusReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "KpiStatusReportController@index");
Route::post("KpiStatusReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "KpiStatusReportController@index");

Route::post("KpiPivotReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "KpiPivotReportController@index");
Route::get("KpiPivotReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "KpiPivotReportController@index");

Route::post("KpiExceptionReport/{sid}/{oid}/{lang}/{uid}", "KpiExceptionReportController@index");
Route::get("KpiExceptionReport/{sid}/{oid}/{lang}/{uid}", "KpiExceptionReportController@index");


// Route::get('/KpiStatusReport/{lang}/KpiValuesReport/{mtp_id}/{kpi_id}/{org_unit}/{kpi_symbol}/{kpi_name}', "KpiValuesReportController@statuslink");
// Route::get('/KpiStatusReport/{lang}/KpiValuesReport/{mtp_id}/{kpi_id}/{orseg_unit}', "KpiValuesReportController@statuslink");
Route::get('/KpiStatusReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}/KpiValuesReport/{cluster}', "KpiValuesReportController@statuslink");
Route::get('/ProjectRiskReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}/ProjectRiskSubReport/{cluster}', "ProjectRiskSubReportController@statuslink");
Route::get('/ProjectBudgetSummaryReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}/ProjectBudgetDetailsReport/{cluster}', "ProjectBudgetDetailsReportController@statuslink");
Route::post('/ProjectBudgetSummaryReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}/ProjectBudgetDetailsReport/{cluster}', "ProjectBudgetDetailsReportController@statuslink");

Route::post("KpiPerformanceReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "KpiPerformanceReportController@index");
Route::get("KpiPerformanceReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "KpiPerformanceReportController@index");

Route::post("UnitPerformanceReport/{sid}/{oid}/{lang}/{uid}", "UnitPerformanceReportController@index");
Route::get("UnitPerformanceReport/{sid}/{oid}/{lang}/{uid}", "UnitPerformanceReportController@index");

Route::post("TopWorthProjectReport/{sid}/{oid}/{lang}/{uid}", "TopWorthProjectReportController@index");
Route::get("TopWorthProjectReport/{sid}/{oid}/{lang}/{uid}", "TopWorthProjectReportController@index");

Route::get("ProjectTeamCostReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectTeamCostReportController@index");
Route::post("ProjectTeamCostReport/{sid}/{oid}/{lang}/{sect}/{org}/{back}/{uid}", "ProjectTeamCostReportController@index");


?>
