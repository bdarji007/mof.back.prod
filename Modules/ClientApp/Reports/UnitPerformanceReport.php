<?php

namespace Modules\ClientApp\Reports;

use \koolreport\pivot\processes\Pivot;
use \koolreport\processes\Filter;
use \koolreport\processes\ColumnMeta;
use \koolreport\processes\CalculatedColumn;
use \koolreport\processes\Group;
use \koolreport\processes\ColumnRename;
use \koolreport\processes\Sort;
use \koolreport\processes\Custom;
use \koolreport\processes\Limit;
use \koolreport\pivot\processes\PivotExtract;
use \koolreport\processes\OnlyColumn;
use \koolreport\processes\CopyColumn;
use \koolreport\processes\ValueMap;
use \koolreport\cleandata\FillNull;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;

error_reporting(E_ALL ^ E_NOTICE);

class UnitPerformanceReport extends \koolreport\KoolReport
{
    use \koolreport\clients\jQuery;
    use \koolreport\clients\Bootstrap;
    use \koolreport\clients\FontAwesome;

    use \koolreport\inputs\Bindable;
    use \koolreport\inputs\POSTBinding;


    protected $language;

    function __construct(array $params = array())
    {

        $this->language = $params['language'];
        // $this->test=$params['test'];
        parent::__construct($params);
    }

    protected function defaultParamValues()
    {

        $this->src("mysql")->query("select name,value from system_vars")->pipe($this->dataStore("debug_mode"))->requestDataSending();

        $debug_modeprog = $this->dataStore("debug_mode")->get(0, "value");
        $debug_modeperf = $this->dataStore("debug_mode")->get(1, "value");

        $month =  date('n');
        $currentmtp = \DB::select(\DB::raw("select mtp.id, mtp.name, fys.start_date, curdate(), fye.end_date from mtp , fiscal_year fys, fiscal_year fye where
mtp.tenant_id = 1  and fys.id = mtp.mtp_start and fye.id = mtp.mtp_end and
CURDATE() >= fys.start_date and CURDATE() <= fye.end_date"));
        $currentmtpID = $currentmtp[0]->id;

        $currentmtpstartdate = $currentmtp[0]->start_date;
        $currentmtpenddate = $currentmtp[0]->end_date;
        $getAllYears = \DB::select(\DB::raw("SELECT @rownum:=@rownum+1 as no, f.*  FROM (SELECT @rownum:=0) r, `fiscal_year` as f WHERE start_date >= '$currentmtpstartdate' and start_date <= '$currentmtpenddate'"));

        $currentYear = 1;
        foreach ($getAllYears as $key => $years) {
            if($years->start_date < date('Y-m-d') && $years->end_date > date('Y-m-d')) {
                $currentYear = $years->no;
            }
        }

        $currentYearReal = $currentYear;

        $currentPeriod = '';
        if(in_array($month, [4,5,6])) {
            $currentPeriod = 1;
        } else if(in_array($month, [7,8,9])) {
            $currentPeriod = 2;
        } else if(in_array($month, [10,11,12])) {
            $currentPeriod = 3;
        } else if(in_array($month, [1,2,3])) {
            $currentPeriod = 4;
        }

        $currentPeriodReal = $currentPeriod;

        if($currentYear == 1 && $currentPeriod == 1) {
            $currentmtpID = count($currentmtp) > 0 ? $currentmtpID -1 : $currentmtpID;
            $currentPeriod = 4;
            $currentYear = 3;
        }
        if($currentPeriod == 1 && $currentYear != 1) {
            $currentPeriod = 4;
            $currentYear = $currentYear -1;
        }

        return array(
            "sector" => "",
            "section" => "",
            "mtp" => ($currentPeriodReal == 1 && $currentYearReal == 1) ? $currentmtpID : $currentmtp[0]->id,
            "periodicity" => 'Y',
            "year_no" => 1,
            "top_performing" => 5,
            "performing" => 80,
            "nonperforming" => 50,
            "expand" => 1,
            //  "filter"=>1,
            "expand1" => "",
            "radiolist" => "3",
            "radiolist1" => "3",
            "sector1" => "",
            "section1" => "",
            "mtp1" => 4,
            "periodicity1" => 'Y',
            "year_no1" => 1,
            "top_performing1" => 5,
            "performing1" => 80,
            "nonperforming1" => 50,
            "debug_modeperf" => $debug_modeperf,

            // "mtp" => 4,
            // "periodicity" => 'Y',
            // "year_no" => 1,
            // "top_performing" => 5,
            // "performing"=>80,
            // "nonperforming"=>50,
            // "test"=>array(1,2,3),

        );
    }

    protected function bindParamsToInputs()
    {
        return array(
            "sector",
            "section",
            "mtp",
            "periodicity",
            "year_no",
            "top_performing",
            "performing",
            "nonperforming",
            "expand",
            // "filter",
            "expand1",
            "radiolist",
            "radiolist1",
            "sector1",
            "section1",
            "mtp1",
            "periodicity1",
            "year_no1",
            "top_performing1",
            "performing1",
            "nonperforming1",
            "debug_modeperf",

// "test",

        );
    }

    public function settings()
    {
        return array(

            "dataSources" => array(
                "mysql" => array(
                    'host' => env('DB_HOST'),
                    'username' => env('DB_USERNAME'),
                    'password' => env('DB_PASSWORD'),
                    'dbname' => env('DB_DATABASE'),
                    'charset' => 'utf8',
                    'class' => "\koolreport\datasources\MySQLDataSource",
                ),
            )


        );
    }

    function setup()
    {
        if (empty($_POST['sector']))
            $this->params['sector'] = $this->params['sid'];

        if (empty($_POST['section']))
            $this->params['section'] = $this->params['oid'];
        // var_dump($this->params["section"]);

        if ($this->params["sector"] == "null")
            $this->params['sector'] = "";

        if ($this->params["section"] == "null")
            $this->params['section'] = "";
        $limit = '';

        if ($this->params['sector'] != 'null' && $this->params['sector'] != '') {
            if ($this->params['sector']) {
                $id = $this->params['sector'];

                $ddd = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id) select id, name from cte order by path"));
                $sectorKeys = [];
                foreach ($ddd as $dd) {
                    if ($dd->id) {
                        $sectorKeys[] = $dd->id;
                    }
                }
            }

            if (!empty($_POST['sector']) && (int)$_POST['sector'] != (int)$this->params['sid']) {
                if (!in_array($_POST['section'], $sectorKeys)) {
                    $this->params['section'] = "";
                }
                //$this->params['section'] = "";
            } else {
                if (!in_array($this->params['section'], $sectorKeys)) {
                    $this->params['section'] = "";
                } else {
                    $this->params['section'] = $this->params['oid'];
                }
            }

            if($this->params['sid'] == $this->params['sector']) {
                if(!empty($_POST['section'])) {
                    $this->params['section'] = (!in_array($this->params['section'], $sectorKeys)) ? $this->params['oid']
                        : $_POST['section'];
                }
            }

            if(!empty($_POST['sector']) && $_POST['sector'] == 2) {
                $this->params['section'] = "";
            }
        }
// var_dump($this->params['section']);
        $language = '';
        if (isset($this->params['language']) && !empty($this->params['language'])) {
            $language = $this->params['language'];
        }
        // var_dump($this->params['expand']);
        if (isset($_POST['expand'])) {

            $this->params["radiolist"] = $this->params["radiolist1"];
            $this->params["sector"] = $this->params["sector1"];
            $this->params["section"] = $this->params["section1"];
            $this->params["mtp"] = $this->params["mtp1"];
            $this->params["periodicity"] = $this->params["periodicity1"];
            $this->params["year_no"] = $this->params["year_no1"];
            $this->params["top_performing"] = $this->params["top_performing1"];
            $this->params["performing"] = $this->params["performing1"];
            $this->params["nonperforming"] = $this->params["nonperforming1"];

        }
        if (isset($_POST['expand1'])) {

            $this->params["expand"] = $this->params["expand1"];

        }


        if (isset($this->params['sector']) && !empty($this->params['sector'])) {
            $parent_id = $this->params["sector"];
            if (isset($this->params['section']) && !empty($this->params['section']) && $this->params['section'] != "null") {
                $parent_id = $this->params["section"];
            } else {
                $parent_id = $this->params["sector"];
            }
        } else {

            $parent_id = 2;

        }
        // $date2=date("Y-m-d");
        $limit = $this->params['top_performing'];

        $performing_id = $this->params['performing'] / 100;
        $nonperforming_id = $this->params['nonperforming'] / 100;

        // if($this->params['expand']==2)
        // {
        //     $this->params['radiolist']=$this->params['radiolist1'];
        // }
        //   echo $this->params['expand'];
        //  ->query("set @period_name = 'Y';")
        //  ->query("set @year_no = 1;")
        //  ->query(" set @mtp_id = 4;")
        //  ->query("  set @sub_id = 114;")
        //  /**set your arg here, value list is {Q1, Q2, Q3, Q4, H1, H2, Y} for quarter/half annual/annual**/
        /**set your arg here**/
        // var_dump($this->params["radiolist"]);
        //----------------------------------------performing--------------------------------
        $this->src("mysql")
            // ->query("call p_unit_perf_prog_recursive_limit(2,4,'Y',1,0.8,0.5,5);")
            ->query("CALL p_unit_perf_prog_recursive_limit($parent_id,:mtp_id,:periodicity_id,:year_id,$performing_id,$nonperforming_id,1000);")
            ->params(array(":sector_id" => $this->params["sector"], ":mtp_id" => $this->params["mtp"], ":year_id" => $this->params["year_no"], ":periodicity_id" => $this->params["periodicity"], ":performing_id" => $this->params["performing"]))
            // ":limit" => $this->params["top_performing"]))


            ->pipe(new ColumnMeta(array(

                "perf_sum_w1" => array(
                    "type" => "number",
                )
            )))
            ->pipe(new CopyColumn(array(
                "perf_sum_w1" => "perf_sum_w",
                // "copy_of_amount"=>"amount"
            )))
            ->pipe(new ValueMap(array(
                "perf_sum_w1" => array(
                    "{func}" => function ($value) {
                        // switch ($value) {
                        static $i = 1;
                        $i++;
                        return $i;

                    }
                )
            )))
            //  ->pipe(new Sort(array(
            //     "perf_sum_w1"=>"desc",
            //     // "name"=>"desc"
            // )))
            ->pipe(new Custom(function ($row) {
                if ($row["perf_sum_w"] != NULL) {
                    $perf_value = $row["perf_sum_w"] * 100;
                    $row["perf_sum_w"] = round($perf_value, 2);
                    if ($this->params['debug_modeperf'] == false && $row["perf_sum_w"] > 100)
                        $row["perf_sum_w"] = 100;


                }
                if ($row["dept_name"] == NULL) {
                    $row["dept_name"] = "";
                }
                if ($row["sub_name"] == NULL) {
                    $row["sub_name"] = "";
                }
                if ($row["sector_name"] == NULL) {
                    $row["sector_name"] = "";
                }
                return $row;
            }))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['perf_sum_w'])) {
                        $row['perf_sum_w'] = $row['perf_sum_w'] . '%';
                    }
                    return $row;
                },

            ]))
            // ->pipe(new Limit(array($this->params['top_performing'])))
            ->pipe(new CalculatedColumn(array(
                "id" => "{#}+1",
                "perf_name" => array(
                    "exp" => function ($data) {
                        if ($data != NULL) {
                            $perdata = str_replace("%", "", $data["perf_sum_w"]);
                            if (($perdata < $this->params["nonperforming"]) && ($perdata != NULL)) {
                                $translation = ($this->params['language'] == 'en' ? 'Not Performing' : 'غير مؤدي');
                                return $translation;
                            }
                            if (($perdata >= $this->params["performing"]) && ($perdata != NULL)) {
                                $translation = ($this->params['language'] == 'en' ? 'Performing' : 'مؤدي');
                                return $translation;
                            } else
                                return "rest";
                        }
                        return NULL;
                    }),

            )))
            ->pipe(new OnlyColumn(array(
                "perf_name",
                "sector_name",
                "dept_name",
                "sub_name",
                "sub_id",
                "under_sector_name",
                "perf_sum_w",
                "perf_sum_w1"
            )))
            ->pipe(new Filter(array(
                array("perf_name", "!=", "rest"),
                //
            )))
            ->pipe(new Filter(array(
                array("perf_name", "=", "Performing"),
                "or",
                array("perf_name", "=", "مؤدي"),

            )))
            //  ->pipe(new Limit(array($this->params['top_performing'])))
            // ->pipe($this->dataStore('test'))

            ->saveTo($node_perf);
        // ->pipe(new Sort(array(
        //     "perf_sum_w"=>"desc"
        // )))

        $node_perf->pipe(new Pivot(array(
            "dimensions" => array(
                "row" => "perf_name,sector_name,dept_name,sub_name,perf_sum_w",
                // "column"=>"kpi_symbol,kpi_name,next_reading_date"
            ),
            'aggregates' => array(
                'sum' => 'perf_sum_w1',
                'count' => 'perf_sum_w'
            )

        )))->saveTo($node3);
//------------------barchart-count-----------------------------------
        $node3->pipe(new PivotExtract(array(
            "row" => array(
                "parent" => array(),
            ),
            "column" => array(
                "parent" => array(),
            ),
            "measures" => array(
                "perf_sum_w - count",
            ),
        )))
            ->pipe($this->dataStore('chartTable1'));
        $node3->pipe($this->dataStore('performing'));


        // $source ->pipe(new Filter(array(
        //     array("perf_name","=","nonperforming")
        // )))

        //    if($this->params['section']=="")
        $node_perf->pipe($this->dataStore('performing1'));

        // if ((isset($_POST['sector'])) && (!empty($_POST['sector']))) {
        if ($this->params['sector'] != "") {
            if ($this->params['section'] != "") {
                $node_perf->pipe(new Group(array(
                    "by" => "sub_name",
                    "count" => "perf_sum_w"
                )))
                    ->pipe(new Filter(array(
                        array("sub_id", "!=", $this->params["section"])
                    )))
                    ->pipe($this->dataStore('performing11'));

            } else {
                $node_perf->pipe(new Group(array(
                    "by" => "dept_name",
                    "count" => "perf_sum_w"
                )))
                    ->pipe($this->dataStore('performing11'));

            }


        }
        // if (empty($_POST['sector'])) {
        if ($this->params['sector'] == "") {

            $node_perf->pipe(new Group(array(
                "by" => "sector_name",
                "count" => "perf_sum_w"
            )))
                // ->pipe(new FillNull(array(
                //     "newValue"=>""
                // )))
                ->pipe($this->dataStore('performing11'));
        }


        // if ((isset($_POST['sector'])) && (!empty($_POST['sector']))) {
        if ($this->params['sector'] != "") {

            if ($this->params['section'] != "") {
                $node_perf->pipe(new Group(array(
                    "by" => "sub_name",
                    "count" => "sub_name"
                )))
                    ->pipe(new Filter(array(
                        array("sub_id", "!=", $this->params["section"])
                    )))
                    ->pipe($this->dataStore('performing1pie'));

            } else {
                $node_perf->pipe(new Group(array(
                    "by" => "dept_name",
                    "count" => "sub_name"

                    // "count"=>"dept_name"
                )))
                    ->pipe($this->dataStore('performing1pie'));

            }


        }
        // if (empty($_POST['sector'])) {
        if ($this->params['sector'] == "") {

            $node_perf->pipe(new Group(array(
                "by" => "sector_name",
                "count" => "sub_name"

                // "count" => "sector_name"
            )))
                // ->pipe(new FillNull(array(
                //     "newValue"=>""
                // )))
                ->pipe($this->dataStore('performing1pie'));
        }


        //  var_dump($this->params['radiolist']);
        //------------------nonperforming(node2)--------------------------


        $this->src("mysql")
            ->query("CALL p_unit_perf_prog_recursive_limit($parent_id,:mtp_id,:periodicity_id,:year_id,$performing_id,$nonperforming_id,1000);")
            ->params(array(":sector_id" => $this->params["sector"], ":mtp_id" => $this->params["mtp"], ":year_id" => $this->params["year_no"], ":periodicity_id" => $this->params["periodicity"], ":nonperforming_id" => $this->params["nonperforming"]))
            // ":limit" => $this->params["top_performing"]))


            ->pipe(new ColumnMeta(array(

                "perf_sum_w1" => array(
                    "type" => "number",
                    // "label"=>"Order Date",
                    // 'suffix' => "%",
                )
            )))
            ->pipe(new Custom(function ($row) {
                if ($row["perf_sum_w"] != NULL) {
                    $perf_value = $row["perf_sum_w"] * 100;
                    $row["perf_sum_w"] = round($perf_value, 2);
                    if ($this->params['debug_modeperf'] == false && $row["perf_sum_w"] > 100)
                        $row["perf_sum_w"] = 100;


                }
                if ($row["dept_name"] == NULL) {
                    $row["dept_name"] = "";
                }
                if ($row["sub_name"] == NULL) {
                    $row["sub_name"] = "";
                }
                if ($row["sector_name"] == NULL) {
                    $row["sector_name"] = "";
                }
                return $row;
            }))
            ->pipe(new CopyColumn(array(
                "perf_sum_w1" => "perf_sum_w",
                // "copy_of_amount"=>"amount"
            )))
            ->pipe(new ValueMap(array(
                "perf_sum_w1" => array(
                    "{func}" => function ($value) {
                        // switch ($value) {
                        static $i = 1;
                        $i++;
                        return $i;

                    }
                )
            )))
            ->pipe(new Sort(array(
                "perf_sum_w1" => "asc",
                // "name"=>"desc"
            )))
            // ->pipe(new Limit(array($this->params['top_performing'])))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['perf_sum_w'])) {
                        $row['perf_sum_w'] = $row['perf_sum_w'] . '%';
                    }
                    return $row;
                },

            ]))
            ->pipe(new CalculatedColumn(array(
                "id" => "{#}+1",
                "perf_name" => array(
                    "exp" => function ($data) {
                        if ($data != NULL) {
                            $perdata = str_replace("%", "", $data["perf_sum_w"]);
                            if (($perdata < ($this->params["nonperforming"])) && ($perdata != NULL)) {
                                $translation = ($this->params['language'] == 'en' ? 'Not Performing' : 'غير مؤدي');
                                return $translation;
                            } else if (($perdata >= ($this->params["performing"])) && ($perdata != NULL)) {
                                $translation = ($this->params['language'] == 'en' ? 'Performing' : 'مؤدي');
                                return $translation;
                            } else
                                return "rest";
                        } else
                            return NULL;
                    }),

            )))
            ->saveTo($source_nonperf);

        $source_nonperf->pipe(new OnlyColumn(array(
            "perf_name",
            "sector_name",
            "dept_name",
            "sub_name",
            "sub_id",
            "under_sector_name",
            "perf_sum_w",
            "perf_sum_w1"
        )))
            ->pipe(new Filter(array(
                array("perf_name", "!=", "rest"),
                //
            )))
            ->pipe(new Filter(array(
                array("perf_name", "=", "Not Performing"),
                "or",
                array("perf_name", "=", "غير مؤدي"),

            )))
            // ->pipe(new Sort(array(

            //     "perf_sum_w"=>"asc"
            //  )))
            // ->pipe(new Limit(array($this->params['top_performing'])))

            ->saveTo($node_nonperf)
            // ->pipe($source)


            ->pipe(new Pivot(array(
                "dimensions" => array(
                    "row" => "perf_name,sector_name,dept_name,sub_name,perf_sum_w",
                    // "column"=>"kpi_symbol,kpi_name,next_reading_date"
                ),
                'aggregates' => array(
                    'sum' => 'perf_sum_w1',
                    'count' => 'perf_sum_w'
                )

            )))->saveTo($node2);
        // if($this->params['section']=="")
        $node2->pipe($this->dataStore('nonperforming'));
//node2
//--------------------------Barchart Count nonperf----------------------
        $node2->pipe(new PivotExtract(array(
            "row" => array(
                "parent" => array(),
            ),
            "column" => array(
                "parent" => array(),
            ),
            "measures" => array(
                "perf_sum_w - count",
            ),
        )))
            ->pipe($this->dataStore('chartTable2'));
//------------------------------------------------------------
        $node_nonperf->pipe($this->dataStore('nonperforming1'));
//node_perf

        // if (isset($_POST['sector']) && (!empty($_POST['sector']))) {
        if ($this->params['sector'] != "") {

            if ($this->params['section'] != "") {

                $node_nonperf->pipe(new ColumnRename(array(
                    "perf_sum_w" => "nonperf_sum_w",
                )))
                    ->pipe(new Group(array(
                        "by" => "sub_name",
                        "count" => "nonperf_sum_w"
                    )))
                    ->pipe(new Filter(array(
                        array("sub_id", "!=", $this->params["section"])
                    )))
                    ->pipe($this->dataStore('nonperforming11'));
            } else {

                $node_nonperf->pipe(new ColumnRename(array(
                    "perf_sum_w" => "nonperf_sum_w",
                )))
                    ->pipe(new Group(array(
                        "by" => "dept_name",
                        "count" => "nonperf_sum_w"
                    )))
                    // ->pipe(new FillNull(array(
                    //     "newValue"=>""
                    // )))
                    ->pipe($this->dataStore('nonperforming11'));


            }
        }
        // if (empty($_POST['sector'])) {
        if ($this->params['sector'] == "") {

            $node_nonperf->pipe(new ColumnRename(array(
                "perf_sum_w" => "nonperf_sum_w",
            )))
                ->pipe(new Group(array(
                    "by" => "sector_name",
                    "count" => "nonperf_sum_w"
                )))
                // ->pipe(new FillNull(array(
                //     "newValue"=>""
                // )))
                ->pipe($this->dataStore('nonperforming11'));


        }
        // if (isset($_POST['sector']) && (!empty($_POST['sector']))) {
        if ($this->params['sector'] != "") {

            if ($this->params['section'] != "") {

                $node_nonperf->pipe(new ColumnRename(array(
                    "perf_sum_w" => "nonperf_sum_w",
                )))
                    ->pipe(new Group(array(
                        "by" => "sub_name",
                        "count" => "sub_name"
                    )))
                    ->pipe(new Filter(array(
                        array("sub_id", "!=", $this->params["section"])
                    )))
                    ->pipe($this->dataStore('nonperforming1pie'));
            } else {

                $node_nonperf->pipe(new ColumnRename(array(
                    "perf_sum_w" => "nonperf_sum_w",
                )))
                    ->pipe(new Group(array(
                        "by" => "dept_name",
                        "count" => "sub_name"
                    )))
                    // ->pipe(new FillNull(array(
                    //     "newValue"=>""
                    // )))
                    ->pipe($this->dataStore('nonperforming1pie'));


            }
        }
        // if (empty($_POST['sector'])) {
        if ($this->params['sector'] == "") {

            $node_nonperf->pipe(new ColumnRename(array(
                "perf_sum_w" => "nonperf_sum_w",
            )))
                ->pipe(new Group(array(
                    "by" => "sector_name",
                    "count" => "sub_name"
                )))
                // ->pipe(new FillNull(array(
                //     "newValue"=>""
                // )))
                ->pipe($this->dataStore('nonperforming1pie'));


        }


//----------------------------------------combined------------------------------


        $this->src("mysql")
// ->query("call p_unit_perf_prog_recursive_limit(2,4,'Y',1,0.8,0.5,5);")

            ->query("CALL p_unit_perf_prog_recursive_limit($parent_id,:mtp_id,:periodicity_id,:year_id,$performing_id,$nonperforming_id,:limit);")
            ->params(array(":sector_id" => $this->params["sector"], ":mtp_id" => $this->params["mtp"], ":year_id" => $this->params["year_no"], ":periodicity_id" => $this->params["periodicity"], ":performing_id" => $this->params["performing"], ":limit" => $this->params["top_performing"]))
// ->pipe(new Filter(array(
//     array("perf_sum_w",">=",0.8)
// )))
// ->pipe(new Limit(array($this->params['top_performing'])))

// ->pipe(new ValueMap(array(
//     "dept_name"=>array(
//         NULL=>"-",

//     )
// )))
            ->saveTo($nodex);


// ->pipe(new Limit(array($this->params['top_performing'])))

        $nodex->pipe(new ColumnMeta(array(

            "perf_sum_w1" => array(
                "type" => "number",

            )
        )))
            ->pipe(new Custom(function ($row) {
                if ($row["perf_sum_w"] != NULL) {
                    $perf_value = $row["perf_sum_w"] * 100;
                    $row["perf_sum_w"] = round($perf_value, 2);
                    if ($this->params['debug_modeperf'] == false && $row["perf_sum_w"] > 100)
                        $row["perf_sum_w"] = 100;


                }
                if ($row["dept_name"] == NULL) {
                    $row["dept_name"] = "";
                }
                if ($row["sub_name"] == NULL) {
                    $row["sub_name"] = "";
                }
                if ($row["sector_name"] == NULL) {
                    $row["sector_name"] = "";
                }
                return $row;
            }))
            ->pipe(new CopyColumn(array(
                "perf_sum_w1" => "perf_sum_w",
                // "copy_of_amount"=>"amount"
            )))
            ->pipe(new ValueMap(array(
                "perf_sum_w1" => array(
                    "{func}" => function ($value) {
                        // switch ($value) {
                        static $i = 1;
                        $i++;
                        return $i;

                    }
                )
            )))
            ->pipe(new Sort(array(
                "perf_sum_w1" => "asc",
                // "name"=>"desc"
            )))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['perf_sum_w'])) {
                        $row['perf_sum_w'] = $row['perf_sum_w'] . '%';
                    }
                    return $row;
                },

            ]))
// ->pipe(new Limit(array($this->params['top_performing'])))
            ->pipe(new CalculatedColumn(array(
                "id" => "{#}+1",
                "perf_name" => array(
                    "exp" => function ($data) {

                        $perdata = str_replace("%", "", $data["perf_sum_w"]);
                        if (($perdata < $this->params["nonperforming"]) && ($perdata != NULL)) {
                            $translation = ($this->params['language'] == 'en' ? 'Not Performing' : 'غير مؤدي');
                            return $translation;
                        } else if (($perdata >= $this->params["performing"]) && ($perdata != NULL)) {
                            $translation = ($this->params['language'] == 'en' ? 'Performing' : 'مؤدي');
                            return $translation;
                        } else
                            return "rest";


                    }),

            )))
            ->pipe(new OnlyColumn(array(
                "perf_name",
                "dept_name",
                "sector_name",
                "sub_name",
                // "kpi_name",
                // "kpi_symbol",
                "perf_sum_w",
                "perf_sum_w1"
            )))
            ->pipe(new Filter(array(
                array("perf_name", "!=", "rest")
            )))
// ->pipe($this->dataStore('test'))

            ->pipe(new Pivot(array(
                "dimensions" => array(
                    "row" => "perf_name,sector_name,dept_name,sub_name,perf_sum_w",
                    // "column"=>"kpi_symbol,kpi_name,next_reading_date"
                ),
                'aggregates' => array(
                    'sum' => 'perf_sum_w1',
                    'count' => 'perf_sum_w1'
                )

            )))
            ->pipe($this->dataStore("combined"));

        //-------------------To get work on behalf roles----------------------------------------------------------------------------------

        $userDetails = User::find($this->params["uid"]);
        if ($this->params["uid"] != "null") {

            /*$this->src("mysql")
                ->query("select model_has_roles.role_id, model_has_roles.model_id, role_work_on_behalf_sectors.sector_id, role_work_on_behalf_sectors.subtenant_id from model_has_roles INNER JOIN role_work_on_behalf_sectors ON role_work_on_behalf_sectors.role_id = model_has_roles.role_id")
                ->pipe(new Filter(array(
                    array("model_id", "=", $this->params["uid"])
                )))
                ->pipe($this->dataStore('role1'))->requestDataSending();*/
            $getRole = Role::where('name', $userDetails->currentRole)->pluck('id')->all();
            $this->src("mysql")
                ->query("select model_has_roles.role_id, model_has_roles.model_id, role_work_on_behalf_sectors.sector_id, role_work_on_behalf_sectors.subtenant_id from model_has_roles INNER JOIN role_work_on_behalf_sectors ON role_work_on_behalf_sectors.role_id = model_has_roles.role_id
                where 1=1
            " . (" and model_id IN ('" . $this->params["uid"] . "')") . "
            " . (" and model_has_roles.role_id in ('" . $getRole[0] . "')") . "
            ")->pipe($this->dataStore('role1'))->requestDataSending();


        }

        $role_sector = $this->dataStore("role1")->only("sector_id")->data();
        $role_subtenant = $this->dataStore("role1")->only("subtenant_id")->data();
        $role_id = $this->dataStore("role1")->get(0, "role_id");
        $i = 0;

        // var_dump($role_sector)--fetching role sector;
        foreach ($role_sector as $key => $value) {
            foreach ($value as $k => $v) {
                $r_sect[$i] = $v;
                $i = $i + 1;
            }
        }
        $r_sect[$i] = (int)$this->params["sid"];
        if(in_array(2, $r_sect)) {
            $merge= [2,3,4,5,6,7,8,9,10];
            $r_sect = $merge;//array_diff( $merge, [2] );
        }
        $m = 0;

        foreach ($role_subtenant as $key => $value) {
            foreach ($value as $k => $v) {
                $r_sub[$m] = $v;
                $m = $m + 1;
            }
        }
        $p_sub = $r_sub;
        $p_sub[$m] = (int)$this->params["oid"];

//----------------------------merging sid with role sector-----------------------------------------------------------------------------------

        if ($this->params["sid"] == "null") {

            $this->src("mysql")
                ->query("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)")//and s.subtenant_type_id in (2,3)
                ->pipe($this->dataStore('sector1'));
        }
        if ($this->params["sid"] != "null") {
            $this->src("mysql")
                ->query("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)")
                ->pipe(new Filter(array(
                    array("id", "in", $r_sect)
                )))
                ->saveTo($node);
            $node->pipe($this->dataStore('sector1'));


        }

        //----------------------------merging sid with role subtenant----------------------------------------------------------------------------------
        if ($this->params["sector"] != null) {

// ----------------------------------------selecting roles corresponding to this sector--------------------------------------------
            if ($this->params["oid"] != "null") {
                if (!empty(array_intersect($p_sub, $sectorKeys))) {
                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name, path from cte order by path")
                        ->params(array(":sector_id" => $this->params["sector"]))
                        ->pipe(new Filter(array(
                            array("id", "in", $p_sub)
                        )))
                        ->pipe(new Custom(function ($row) {
                            if ($row["id"] != NULL)
                                return $row;
                        }))
                        ->pipe($this->dataStore('section111'))->requestDataSending(); //sections corresponding to that sector
                } else {
                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name, path from cte order by path")
                        ->params(array(":sector_id" => $this->params["sector"]))
                        ->pipe($this->dataStore('section222'))->requestDataSending(); //sections
                }
            }

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
//-----------------------------generating each org unit datastore------------------------------
            if ($this->datastore('section111')->count() > 0) {
                foreach ($r_section as $key => $v) {

                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = :section_id UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), :section_id, '', CONCAT(id, '') from subtenant where id = :section_id) select id,  name from cte order by path")
                        ->params(array(":section_id" => $v))
                        ->saveTo($node_test);

                    $section_name = "sect" . $v;
                    $node_test->pipe($this->dataStore($section_name));

                }


            }


            //-------------------------------------------------------------------------------------------------------------------------------
            if ($this->params["oid"] == "null") {

                $this->src("mysql")
                    ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name from cte order by path")
                    ->params(array(":sector_id" => $this->params["sector"]))
                    ->pipe($this->dataStore('section1'));
            }

        }


        $this->src("mysql")
            ->query("select id,name from mtp")
            // ->pipe($this->dataStore('mtp1'));
            ->pipe($this->dataStore('mtp1'));

        $this->src("mysql")
            ->query("select id, name from kpi_cat")
            ->pipe($this->dataStore('category1'));

        $this->src("mysql")
            ->query("select * from trans_table")
            ->pipe($this->dataStore('translation'))->requestDataSending();
        $this->src("mysql")
            ->query("select id,name from subtenant where id=$parent_id")
            ->pipe($this->dataStore('sector_name'))->requestDataSending();
    }
}
