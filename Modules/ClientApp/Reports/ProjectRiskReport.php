<?php

namespace Modules\ClientApp\Reports;

use \koolreport\processes\Filter;
use \koolreport\processes\ColumnMeta;
use \koolreport\processes\CalculatedColumn;
use \koolreport\processes\Custom;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;

error_reporting(E_ALL ^ E_NOTICE);


class ProjectRiskReport extends \koolreport\KoolReport
{
    use \koolreport\clients\jQuery;
    use \koolreport\clients\Bootstrap;

//  use \koolreport\clients\FontAwesome;
    use \koolreport\laravel\Friendship;
    use \koolreport\inputs\Bindable;
    use \koolreport\inputs\POSTBinding;

    public $sect;

    function __construct(array $params = array())
    {
        $this->sect = $this->params['sect'];
        $this->org = $this->params['org'];
        $this->sid = $this->params['sid'];
        $this->oid = $this->params['oid'];
        parent::__construct($params);
    }

    protected function defaultParamValues()
    {
        return array(
            "sector" => null,
            "section" => null,
            "project_name" => null,
            "project_symbol" => null,
            "only_open_risk" => null,
            "tenant_id" => 1,//env('TENANT_ID'),

            // "debug_modeprog"=>false,
            // "debug_modeperf"=>true,
            //
        );
    }

    protected function bindParamsToInputs()
    {
        return array(
            "sector",
            "section",
            "project_name",
            "project_symbol",
            "only_open_risk",
            "tenant_id",


        );
    }

    public function settings()
    {
        return array(
            "dataSources" => array(
                "mysql" => array(
                    'host' => env('DB_HOST'),
                    'username' => env('DB_USERNAME'),
                    'password' => env('DB_PASSWORD'),
                    'dbname' => env('DB_DATABASE'),
                    'charset' => 'utf8',
                    'class' => "\koolreport\datasources\MySQLDataSource",
                ),
            )
        );
    }

    function setup()
    {

//  if(empty($_POST['sector']))
//                     $this->params['sector']= $this->params['sid'];

//                     if(empty($_POST['section']))
//                     $this->params['section']= $this->params['oid'];
//                     // var_dump($this->params["section"]);

//                     if($this->params["sector"]=="null")
//                     $this->params['sector']="";
//          if (isset($this->params['sect']) && !empty($this->params['sect']) && empty($_POST['sector']) &&  $this->params['sect']!="null" && $this->params['sect']!="undefined") {

//             $this->params['sector']= $this->params['sect'];
//             $this->params['sect']="null";
//         }

//         if (isset($this->params['org']) && !empty($this->params['org']) && empty($_POST['section']) &&  $this->params['org']!="null" && $this->params['org']!="undefined") {

//             $this->params['section']= $this->params['org'];
//             $this->params['org']="null";
//             // var_dump($this->params["section"]);
//             // var_dump($this->params["org"]);

//         }
        if (empty($_POST['sector']))
            $this->params['sector'] = $this->params['sid'];

        if (empty($_POST['section']))
            $this->params['section'] = $this->params['oid'];

        if ($this->params['sector'] == "" || $this->params['sector'] == "null")
            $this->params['sector'] = null;

        if ($this->params['section'] == "" || $this->params['section'] == "null")
            $this->params['section'] = null;

        if ($this->params['sector'] != 'null' && $this->params['sector'] != '') {
            if ($this->params['sector']) {
                $id = $this->params['sector'];

                $ddd = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id) select id, name from cte order by path"));
                $sectorKeys = [];
                foreach ($ddd as $dd) {
                    if ($dd->id) {
                        $sectorKeys[] = $dd->id;
                    }
                }
            }

            if (!empty($_POST['sector']) && (int)$_POST['sector'] != (int)$this->params['sid']) {
                if (!in_array($_POST['section'], $sectorKeys)) {
                    $this->params['section'] = "";
                }
                //$this->params['section'] = "";
            } else {
                if (!in_array($this->params['section'], $sectorKeys)) {
                    $this->params['section'] = "";
                } else {
                    $this->params['section'] = $this->params['oid'];
                }
            }

            if($this->params['sid'] == $this->params['sector']) {
                if(!empty($_POST['section'])) {
                    $this->params['section'] = (!in_array($this->params['section'], $sectorKeys)) ? $this->params['oid']
                        : $_POST['section'];
                }
            }

            if(!empty($_POST['sector']) && $_POST['sector'] == 2) {
                $this->params['section'] = "";
            }
        }

        // $this->params["sector"] = $this->params["sector"] == "" ? null : $this->params["sector"];
        // $this->params["section"] = $this->params["section"] == "" ? null : $this->params["section"];


        // //  $sql1=$this->params['sector']!=null?"sector_id=$this->params['sector']":"1=1".($this->params["sector"]!=null?""sector_id=$this->params['sector']":"1=1")." ;
        // set @tenant_id = 1; -- 1 for MOF, value exists in the application as environment variable
        // set @sector_id = null; -- put value for selected sector, or null for the ministry
        // set @org_unit = null; -- put value for selected org unit or null
        // set @proj_id = null; -- put value for selected project or null
        // set @only_open_risk = null; -- put true to show only projects with open_risk_count > 0 otherwise false or null
        $this->src("mysql")
            ->query("select tab_i.*, ps.name as proj_status_name, ps.color_code from (
                select 	p.id, p.symbol, p.name_short, p.sector_id, sub.name as org_unit_name,
                                        p.start_dt_actual, p.end_dt_base, p.status_operational,
                                         (select	pvs.proj_status from 	proj_value_stats pvs, proj_values pv where
                                                                pvs.id = pv.id and
                                                                pv.project_id = p.id and
                                                                pv.period_dt = 	(select max(pv_i.period_dt) from proj_values pv_i, proj_value_stats pvs_i where
                                                                                                                                            pv_i.project_id = pv.project_id and
                                                                                                                                            pvs_i.id = pv_i.id and
                                                                                                                                            pvs_i.proj_status is not null
                                                                                                                    )
                                         ) as proj_status,
                                        (select count(1) from proj_risks pr where pr.project_id = p.id and pr.status = 1) as open_risk_count
                            from project p, subtenant sub where
                                        p.tenant_id = :tenant_id and
                                        sub.id = ifnull(p.subtenant_id, p.sector_id) and
                                        (p.sector_id = :sector_id or :sector_id is null) and
                                        (p.subtenant_id = :section_id or :section_id is null) and
                                        p.status_operational <> 3 and
                                        p.has_risks = 1
                ) as tab_i
                left join proj_status ps
                            on	tab_i.proj_status = ps.id
                where if(:only_open_risk, tab_i.open_risk_count>0, true)
                ;")
            ->params(array(":sector_id" => $this->params["sector"], ":section_id" => $this->params["section"], ":tenant_id" => $this->params["tenant_id"], ":only_open_risk" => $this->params["only_open_risk"]))
            ->pipe($this->dataStore('project_details'));

        //-------------------To get work on behalf roles----------------------------------------------------------------------------------

        $userDetails = User::find($this->params["uid"]);
        if ($this->params["uid"] != "null") {

            /*$this->src("mysql")
                ->query("select model_has_roles.role_id, model_has_roles.model_id, role_work_on_behalf_sectors.sector_id, role_work_on_behalf_sectors.subtenant_id from model_has_roles INNER JOIN role_work_on_behalf_sectors ON role_work_on_behalf_sectors.role_id = model_has_roles.role_id")
                ->pipe(new Filter(array(
                    array("model_id", "=", $this->params["uid"])
                )))
                ->pipe($this->dataStore('role1'))->requestDataSending();*/

            $getRole = Role::where('name', $userDetails->currentRole)->pluck('id')->all();
            $this->src("mysql")
                ->query("select model_has_roles.role_id, model_has_roles.model_id, role_work_on_behalf_sectors.sector_id, role_work_on_behalf_sectors.subtenant_id from model_has_roles INNER JOIN role_work_on_behalf_sectors ON role_work_on_behalf_sectors.role_id = model_has_roles.role_id
                where 1=1
            " . (" and model_id IN ('" . $this->params["uid"] . "')") . "
            " . (" and model_has_roles.role_id in ('" . $getRole[0] . "')") . "
            ")->pipe($this->dataStore('role1'))->requestDataSending();


        }

        $role_sector = $this->dataStore("role1")->only("sector_id")->data();
        $role_subtenant = $this->dataStore("role1")->only("subtenant_id")->data();
        $role_id = $this->dataStore("role1")->get(0, "role_id");
        $i = 0;

        // var_dump($role_sector)--fetching role sector;
        foreach ($role_sector as $key => $value) {
            foreach ($value as $k => $v) {
                $r_sect[$i] = $v;
                $i = $i + 1;
            }
        }
        $r_sect[$i] = (int)$this->params["sid"];
        if(in_array(2, $r_sect)) {
            $merge= [2,3,4,5,6,7,8,9,10];
            $r_sect = $merge;//array_diff( $merge, [2] );
        }
        $m = 0;

        foreach ($role_subtenant as $key => $value) {
            foreach ($value as $k => $v) {
                $r_sub[$m] = $v;
                $m = $m + 1;
            }
        }
        $p_sub = $r_sub;
        $p_sub[$m] = (int)$this->params["oid"];

//----------------------------merging sid with role sector-----------------------------------------------------------------------------------

        if ($this->params["sid"] == "null") {

            $this->src("mysql")
                ->query("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)")//and s.subtenant_type_id in (2,3)
                ->pipe($this->dataStore('sector1'));
        }
        if ($this->params["sid"] != "null") {
            $this->src("mysql")
                ->query("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)")
                ->pipe(new Filter(array(
                    array("id", "in", $r_sect)
                )))
                ->saveTo($node);
            $node->pipe($this->dataStore('sector1'));


        }

        //----------------------------merging sid with role subtenant----------------------------------------------------------------------------------
        if ($this->params["sector"] != null) {

// ----------------------------------------selecting roles corresponding to this sector--------------------------------------------
            if ($this->params["oid"] != "null") {
                if (!empty(array_intersect($p_sub, $sectorKeys))) {
                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name, path from cte order by path")
                        ->params(array(":sector_id" => $this->params["sector"]))
                        ->pipe(new Filter(array(
                            array("id", "in", $p_sub)
                        )))
                        ->pipe(new Custom(function ($row) {
                            if ($row["id"] != NULL)
                                return $row;
                        }))
                        ->pipe($this->dataStore('section111'))->requestDataSending(); //sections corresponding to that sector
                } else {
                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name, path from cte order by path")
                        ->params(array(":sector_id" => $this->params["sector"]))
                        ->pipe($this->dataStore('section222'))->requestDataSending(); //sections
                }
            }

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
//-----------------------------generating each org unit datastore------------------------------
            if ($this->datastore('section111')->count() > 0) {
                foreach ($r_section as $key => $v) {

                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where id = :section_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '>', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :section_id, '', CONCAT(id, '') from subtenant where id = :section_id) select id,  name from cte order by path")
                        ->params(array(":section_id" => $v))
                        ->saveTo($node_test);

                    $section_name = "sect" . $v;
                    $node_test->pipe($this->dataStore($section_name));

                }


            }


            //-------------------------------------------------------------------------------------------------------------------------------
            if ($this->params["oid"] == "null") {

                $this->src("mysql")
                    ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name from cte order by path")
                    ->params(array(":sector_id" => $this->params["sector"]))
                    ->pipe($this->dataStore('section1'));
            }

        }

        $this->src("mysql")
            ->query("select id,name,status_operational from project")
            ->pipe(new Filter(array(
                array("status_operational", "!=", 3),
                //
            )))
            ->pipe($this->dataStore('project_name'));

        $this->src("mysql")
            ->query("select id,symbol,status_operational from project")
            ->pipe(new Filter(array(
                array("status_operational", "!=", 3),
                //
            )))
            ->pipe($this->dataStore('project_symbol'));

        // $this->src("mysql")
        //     ->query(" SELECT project_id, COUNT(*) as count
        //     FROM proj_risks
        //     GROUP BY project_id;")
        //     ->pipe($this->dataStore('risk_details'));


        $this->src("mysql")
            ->query("select * from trans_table")
            ->pipe($this->dataStore('translation'))->requestDataSending();
        // $this->src("mysql")
        //     ->query("select id,name from subtenant")
        //     ->pipe($this->dataStore('org_name'))->requestDataSending();
    }
}
