<?php

use \koolreport\widgets\koolphp\Table;
use \koolreport\processes\CalculatedColumn;
use \koolreport\inputs\BSelect;
use \koolreport\inputs\Select;
use \koolreport\processes\Sort;
use \koolreport\inputs\Select2;
use \koolreport\datagrid\DataTables;
use \koolreport\sparklines;
use \koolreport\inputs\DateTimePicker;
use \koolreport\inputs\CheckBoxList;
use Modules\ClientApp\Reports\ProjectBudgetSummaryReport;
use \koolreport\processes\Filter;
use \koolreport\widgets\google\BarChart;
use \koolreport\widgets\google\ColumnChart;

$language = '';
if (isset($this->params['language']) && !empty($this->params['language'])) {
    $language = $this->params['language'];

}

//-------------------------generating datastore for section------------------------------
if ($this->params["sector"] != null) {

    // ----------------------------------------selecting roles corresponding to this sector--------------------------------------------
    if ($this->datastore('section111')->count() > 0) {
        if ($this->params["oid"] != "null") {

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        // echo "Key=" . $k . ", Value=" . $v;
                        // echo "<br>";
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
            $store = [];


            foreach ($r_section as $key => $v) {
                $section_name = "sect" . $v;
                $store = array_merge($store, $this->dataStore($section_name)->data());
            }
        }
    }

    if ($this->datastore('section222')->count() > 0) {
        if ($this->params["oid"] != "null") {
            $store = $this->dataStore("section222")
                ->filter(function ($row) {
                    return $row["id"] != NULL;
                });

        }
    }

    if ($this->params["oid"] == "null") {
        $store = $this->dataStore("section1")
            ->filter(function ($row) {
                return $row["id"] != NULL;
            });

    }
}
//----------------------------------------------------------------------------------

$sector_name = $this->dataStore('sector_name');
// echo "<pre>";
// var_dump($this->dataStore('sector_test')->toTableArray());


$sector11 = $sector_name->get(0, "name");
$transtable = $this->dataStore('translation');

?>

<!DOCTYPE html>
<?php if ($language == 'ar')
    $dir = "rtl";

else
    $dir = "ltr";
?>
<html dir="<?php echo $dir; ?>">
<!-- <html dir="rtl" onchange="console.log('value of name field is changed')"> -->
<head>
    <meta charset="utf-8">
    <title>
    </title>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

</head>
<style>
    .buttons-print {
        background-color: #ffffff;
        border: none;
    }

    table {
        width: 100%;
        table-layout: fixed;
        /* transform: scale(0.50) translate(-100px,0px); */

    }


    .color {
        border: 0px solid black;
    }

    .insideBorder {
        border: 10px solid white;
    }

    .line {
        width: 1320px;
        border-bottom: 1px solid black;
        position: absolute;
    }

    .dataTables_filter input {
        width: 450px
    }

    .cssHeader {
        background-color: #73818f;
        color: #fff;
        text-align: <?php echo $language=='ar'?'right':'left' ;?>;
        font-size: 12px;
        overflow-wrap: break-word;
    }

    .cssItem {
        background-color: #fdffe8;
        font-size: 12px;
    }

    .container {
        display: flex;
        width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .container1 {
        display: inline-flex;
        column-width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .header img {
        float: left;
        width: 100px;
        height: 100px;
        background: #555;
    }

    .select {
        margin: 10px 10px 0px 325px;
    }

    .checkbox input[type="checkbox"]
        /* input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"]{ */
    {
        position: relative;
        margin-left: 0px;
        float: right;
        font-weight: 700;
    }

    .checkbox label {

        /* float:right; */
        font-weight: 300;
        text-align: left;
        padding: 10px;
        /* margin-top:10px; */
        margin-bottom: 10px;
        /* margin-left:20px; */

    }

    .decoratedLine1, .decoratedLine2, .decoratedLine3, .decoratedLine4, .decoratedLine5 {
        overflow: hidden;
    }

    .decoratedLine1 > span, .decoratedLine2 > span, .decoratedLine3 > span, .decoratedLine4 > span, .decoratedLine5 > span {
        position: relative;
        display: inline-block;
    }

    .decoratedLine1 > span:before, .decoratedLine2 > span:before, .decoratedLine3 > span:before, .decoratedLine4 > span:before, .decoratedLine5 > span:before {
        content: '';
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        width: 591px;
        margin: 0 20px;
    }

    .decoratedLine1 > span:before {
        border-bottom: 12px solid #A9A9A9;

    }

    .decoratedLine2 > span:before {
        border-bottom: 12px solid #808080;
    }

    .decoratedLine1En > span:before, .decoratedLine2En > span:before, .decoratedLine3En > span:before, .decoratedLine4En > span:before, .decoratedLine5En > span:before {
        right: 100%;
    }

    .decoratedLine1Ar > span:before, .decoratedLine2Ar > span:before, .decoratedLine3Ar > span:before, .decoratedLine4Ar > span:before, .decoratedLine5Ar > span:before {
        left: 100%;
    }

    /* .table {
    /* font-size: 50%; */
    /* transform: <\?php echo $language=='ar'?'scale(0.7) translate(205px,50px); ':"" ;?>; */
    /* margin-right:20px;
    padding-right:20px; */
    /* } */


</style>
<body>
<p id="printarea"></p>
<div id="printissue">
    <div id="page"
         style="background-color:#ffffff;margin-left:10px;margin-right:5px;margin-top:0px;margin-bottom:30px;padding-top:30px;padding-right:10px;padding-left:10px;">
        <h4 class="mb-0 pt-2" style="text-align:center;color: #20a8d8;font-size: 20px;font-weight: normal;">
            <?php $textbit = 'title';
            $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
            echo $translation ?>
        </h4>
        <scan class="form-group"
              style="float:<?php echo $language == 'ar' ? 'left' : 'right'; ?>;padding-right:150px;padding-left:150px;">
            <script> document.write(new Date().toDateString()); </script>
        </scan>

        <?php if ($language == 'ar') {
            $dir = "rtl";
            $lin = "left";
        } else {
            $dir = "ltr";
            $lin = "right";
        }
        ?>
        <div style="background-color:#ffffff;border: 1px solid #a5aeb7;position:relative;" class="col-md-12"
             style="float:right;">
            <!-- <div class="col-md-10javascript:history.go(-1)"></div> -->
            <!-- <\?php   if (isset($this->params['sect']) && !empty($this->params['sect']) && empty($_POST['sector']) &&  $this->params['sect']!="null" && $this->params['sect']!="undefined") ?> -->
            <!-- <span id="backlink1" style="display:<\?php echo $this->params['back']==1?'block':'none'; ?>">
            <button onClick="linktokpi();" style="float:left;border:none;background-color: #ffffff;"><i
                        style="padding-top:10px;color:#a9a9a9;font-size:12px;" class="fa fa-arrow-left "></i></button>
            </span> -->
            <div id="button1" dir="rtl" style="float:<?php echo $lin; ?>">
                <button onclick="myFunction();" style="float:left;border:none;background-color: #ffffff;"><i
                        style="padding-top:5px;color:#a9a9a9;;" class="fa fa-angle-up 4x"></i></button>
                <button onClick="javascript:printDiv()"
                        style="float:left;border:none;background-color: #ffffff;"><i
                        style="padding-top:5px;color:#a9a9a9;font-size:12px" class="fa fa-print "></i></button>

            </div>
        </div>
        <br/>
        <?php $new = $this->dataStore('project_details')->process(new CalculatedColumn(array("sl" => "{#}+1"))); ?>
        <?php $style = "";
        if (empty($_POST)) {
            $style = 'display:none !important;';
        } else {
            $style = 'display:block !important;';
        } ?>
        <?php if ($language == 'ar') {
            $dir = "rtl";
            $lin = "right";
        } else {
            $dir = "ltr";
            $lin = "left";
        }
        ?>

        <div class="col-md-12" id="myDIV"
             style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;<?php echo $style; ?>">
            <form id="form1" method="post">
                <div class="col-md-4 form-group" style="float:<?php echo $lin; ?>">
                    <strong>
                        <!-- sector -->
                        <?php $textbit = 'sector';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php
                    select2::create(array(
                        // "multiple"=>false,
                        "name" => "sector",
                        "defaultOption" => array("--" => ""),
                        "dataStore" => $this->dataStore("sector1"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                             $('#section').val(null).trigger('change');
                            $('#form1').submit();
                        }"
                        )
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float: <?php echo $lin; ?>">
                    <strong>
                        <!-- org_unit -->
                        <?php $textbit = 'org_unit';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php

                    Select2::create(array(
                        // "multiple"=>true,
                        "name" => "section",
                        "defaultOption" => array("--" => ""),
                        "dataStore" => $store,
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){
                             $('#form1').submit();
                               }"
                        )

                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float: <?php echo $lin; ?>">
                    <strong>
                        <!-- project_symbol -->
                        <?php $textbit = 'status_operational';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "status_operational",
                        "defaultOption" => array("--" => ""),
                        "data" => array(
                            // $language == 'en' ? $transtable->where('key_name', 'Planning')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'planning')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar") => 1,
                            // $language == 'en' ? $transtable->where('key_name', 'Progressing')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'progressing')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar") => 2,
                            // $language == 'en' ? $transtable->where('key_name', 'On_hold')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'on_hold')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar") => 3,
                            // $language == 'en' ? $transtable->where('key_name', 'closed')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'closed')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar") => 4,
                            "planning" => 0,
                            "progressing" => 1,
                            "on_hold" => 2,
                            "closed" => 3,

                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float:<?php echo $lin; ?>">
                    <strong>
                        <!-- project_name -->
                        <?php $textbit = 'project_list';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "id",
                        "dataStore" => $this->dataStore("project_name"),
                        "defaultOption" => array("--" => ""),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float: <?php echo $lin; ?>">
                    <strong>
                        <?php $textbit = 'mtp';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'kpi_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'kpi_status_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php
                    select2::create(array(
                        // "multiple"=>true,
                        "name" => "mtp",
                        "placeholder" => "اختر ",
                        "dataStore" => $this->dataStore("mtp1"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ), "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){

                    $('#form1').submit();
                }"
                        )
                    ));
                    ?>
                </div>
                <!-- </div> -->

                <!-- <br> -->
            </form>
            <!-- </div> -->
        </div>
        <!-- <div class="card"> -->
        <div class="col-md-12" id="pivot"
             style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:10px;padding-right:10px;">
            <div class="col-md-7">
                <div class="removeTable">
                    <?php
                    DataTables::create(array(
                        "dataSource" => $new,
                        "name" => "example",
                        "columns" => array(
                            "sl" => array(
                                "label" => "#",
                            ),
                            "symbol" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'project_symbol')->where('key_pos', 'project_budget_summary_report')->get(0,
                                    "value_en") : $transtable->where('key_name', 'project_symbol')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar")
                                // "رمز",
                            ),
                            "name_short" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'project_name')->where('key_pos', 'project_budget_summary_report')->get(0,
                                    "value_en") : $transtable->where('key_name', 'project_name')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"),
                                //"الاسم",
                                "cssStyle" => "overflow-wrap:beak-word;",

                                "formatValue" => function ($value, $row) {
                                    $language1 = $this->params['language'];
                                    $uid1 = $this->params['uid'];
                                    $sid1 = $this->params['sid'];
                                    $oid1 = $this->params['oid'];
                                    // $mtp1 = $this->params['mtp'];
                                    $proj_id1 = $row['id'];
                                    $Mixed = array("proj_id" => $proj_id1, "uid" => $uid1, "sid" => $sid1, "oid" => $oid1,);

                                    $url = $language1 . "/ProjectBudgetDetailsReport/" . http_build_query(array(
                                            "array" => $Mixed
                                        ));

                                    return "<a href=" . $url . ">$value</a>";
                                }

                            ),
                            "resp_org_unit" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'responsible_unit')->where('key_pos', 'project_budget_summary_report')->get(0,
                                    "value_en") : $transtable->where('key_name', 'responsible_unit')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"),
                                //"org unit",
                                "cssStyle" => "overflow-wrap:beak-word;",

                            ),

                            "budget_base" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'planned_budget')->where('key_pos', 'project_budget_summary_report')->get(0,
                                    "value_en") : $transtable->where('key_name', 'planned_budget')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                                "cssStyle" => "overflow-wrap:beak-word;",
                                "formatValue" => function ($value, $row) {

                                    $data = number_format((float)($value), 3);
                                    // $data_val = preg_replace("/\.?0+$/", "", $data);//floatval($data);
                                    return "<div style=''>KWD $data</div>";

                                }

                            ),
                            "actual_cost" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'actual_cost')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'actual_cost')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"),//"communication officer",
                                "cssStyle" => "overflow-wrap:beak-word;",
                                "formatValue" => function ($value, $row) {

                                    $data = number_format((float)($value), 3);
                                    //$data_val = preg_replace("/\.?0+$/", "", $data);//floatval($data);
                                    return "<div style=''>KWD $data</div>";

                                }

                            ),
                            "remaining_budget" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'remaining_budget')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'remaining_budget')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                                "formatValue" => function ($value, $row) {
                                    $language = $this->params['language'];
                                    $transtable = $this->dataStore('translation');
                                    $data = number_format((float)($value), 3);
                                    // $data_val = preg_replace("/\.?0+$/", "", $data);//floatval($data);
                                    // if ($value < 0) {
                                    //     return "<div style='color:red;'>KWD $data</div>";
                                    // } else {
                                    //     return "<div>KWD $data</div>";

                                    // }

                                    return "<div>KWD $data</div>";

                                }
                                //  "Next Value Date"  ,
                            ),
                            "cost_variance" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'cost_variance')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'cost_variance')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                                "formatValue" => function ($value, $row) {
                                    $language = $this->params['language'];
                                    $transtable = $this->dataStore('translation');
                                    $data = number_format((float)($value), 3);

                                    // if ($value < 0) {
                                    //     return "<div style='color:red;'>KWD $data</div>";
                                    // } else if ($data == 0.00) {
                                    //     return "<div>-</div>";

                                    // } else {
                                    //     return "<div>KWD $data</div>";

                                    // }
                                    if ($data == 0.000) {
                                        return "<div>-</div>";

                                    } else {
                                        return "<div>KWD $data</div>";

                                    }


                                }
                                //  "Next Value Date"  ,
                            ),

                            "b_etc" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'etc')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'etc')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                                "formatValue" => function ($value, $row) {
                                    $remaining = $row['remaining_budget'];
                                    $data = number_format((float)($value), 3);
                                    //$data_val = preg_replace("/\.?0+$/", "", $data_val);//floatval($data);

                                    // if ($value > $remaining) {
                                    //     return "<div style='color:red;'>KWD $data</div>";
                                    // } else if ($data == 0.000) {
                                    //     return "<div>-</div>";

                                    // } else {
                                    //     return "<div>KWD $data</div>";

                                    // }

                                    if ($data == 0.000) {
                                        return "<div>-</div>";

                                    } else {
                                        return "<div>KWD $data</div>";

                                    }

                                }
                            ),
                            "b_eac" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'eac')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'eac')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                                "formatValue" => function ($value, $row) {
                                    $planned = $row['budget_base'];
                                    $data = number_format((float)($value), 3);
                                    // $data_val = preg_replace("/\.?0+$/", "", $data);//floatval($data);
                                    // if ($value > $planned) {
                                    //     return "<div style='color:red;'>KWD $data</div>";
                                    // } else if ($data == 0.00) {
                                    //     return "<div>-</div>";

                                    // } else {
                                    //     return "<div>KWD $data</div>";

                                    // }

                                    if ($data == 0.000) {
                                        return "<div>-</div>";

                                    } else {
                                        return "<div>KWD $data</div>";

                                    }

                                }
                            ),

                            "b_var_at_complete" => array(
                                "label" => $language == 'en' ? $transtable->where('key_name', 'variance_at_complete')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'variance_at_complete')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"), //"تاريخ القراءة التالية",
                                "formatValue" => function ($value, $row) {

                                    $data = number_format((float)($value), 3);
                                    // if ($value < 0) {
                                    //     return "<div style='color:red;'>KWD $data</div>";
                                    // } else if ($data == 0.00) {
                                    //     return "<div>-</div>";

                                    // } else {
                                    //     return "<div>KWD $data</div>";

                                    // }

                                    if ($data == 0.000) {
                                        return "<div>-</div>";

                                    } else {
                                        return "<div>KWD $data</div>";

                                    }

                                }


                            ),
                            "status_operational" => array(
                                "cssStyle" => "overflow-wrap:beak-word;"

                            ),
                            "id" => array(
                                "cssStyle" => "overflow-wrap:beak-word;"

                            ),

                        ),
                        "cssClass" => array(
                            "table" => "table table-striped table-bordered color  ",
                            "th" => "cssHeader insideBorder ",
                            "tr" => "cssItem color",
                            "td" => "insideBorder"
                        ),
                        "options" => array(
                            "columnDefs" => array(
                                array("width" => 100, "targets" => 2),
                                array("width" => 80, "targets" => 3),

                            ),
                            "searching" => true,
                            "paging" => true,
                            "orders" => array(
                                array(0, "asc")
                            )
                        )


                    ));


                    ?>
                </div>
                <br>
                <br>
                <div><strong><?php $textbit = 'title';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation ?></strong></div>
                <br>
                <br>
                <div id="summary">
                    <div class="row">

                        <div class="col-md-2 form-group head"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'planned_budget';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $bb_sum = $this->dataStore('project_details')->sum("budget_base");
                                $bb_sum = number_format((float)($bb_sum), 3);
                                echo "KWD " . $bb_sum; ?>

                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'actual_cost';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $ac_sum = $this->dataStore('project_details')->sum("actual_cost");
                                $ac_sum = number_format((float)($ac_sum), 3);
                                echo "KWD " . $ac_sum;
                                ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'remaining_budget';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $rb_sum = $this->dataStore('project_details')->sum("remaining_budget");
                                $rb_sum = number_format((float)($rb_sum), 3);
                                echo "KWD " . $rb_sum;
                                ?>
                            </strong>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'cost_variance';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $cv_sum = $this->dataStore('project_details')->sum("cost_variance");;
                                $cv_sum = number_format((float)($cv_sum), 3);
                                echo "KWD " . $cv_sum;
                                ?>

                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'etc';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $b_etc_sum = $this->dataStore('project_details')->sum("b_etc");;
                                $b_etc_sum = number_format((float)($b_etc_sum), 3);
                                echo "KWD " . $b_etc_sum; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'eac';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $b_eac_sum = $this->dataStore('project_details')->sum("b_eac");;
                                $b_eac_sum = number_format((float)($b_eac_sum), 3);
                                echo "KWD " . $b_eac_sum; ?>

                            </strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'variance_at_complete';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $b_var_at_complete_sum = $this->dataStore('project_details')->sum("b_var_at_complete");;
                                $b_var_at_complete_sum = number_format((float)($b_var_at_complete_sum), 3);
                                echo "KWD " . $b_var_at_complete_sum; ?>
                            </strong>
                        </div>
                    </div>
                </div>


            </div>
            <div id="charts" class="col-md-4">
                <div class="" id="barchart" style="text-align:center;">
                    <?php

                    $actual = $this->dataStore('project_details')->sum("actual_cost");
                    $remaining = $this->dataStore('project_details')->sum("remaining_budget");
                    $planned = $this->dataStore('project_details')->sum("budget_base");

                    $x = 0;
                    $y = 0;

                    $translation_planned = ($language == 'en' ? $transtable->where('key_name', 'planned_budget')->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', 'planned_budget')->where('key_pos', 'project_view')->get(0, "value_ar"));
                    $translation_actual = ($language == 'en' ? $transtable->where('key_name', 'actual_cost')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'actual_cost')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                    $translation_remaining = ($language == 'en' ? $transtable->where('key_name', 'remaining_budget')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'remaining_budget')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));


                    $translation_planned_vs_actual = $translation_planned . "\t" . "vs" . "\t" . $translation_actual;
                    $translation_planned_vs_remaining = $translation_planned . "\t" . "vs" . "\t" . $translation_remaining;
                    $translation_actual_vs_remaining = $translation_actual . "\t" . "vs" . "\t" . $translation_remaining;


                    $category_amount = array(
                        array("category" => $translation_planned_vs_actual, "first" => $planned, "second" => $actual),
                        array("category" => $translation_planned_vs_remaining, "first" => $planned, "second" => $remaining),
                        array("category" => $translation_actual_vs_remaining, "first" => $actual, "second" => $remaining),

                    );

                    $category_amount_copy = array(
                        array("category" => $translation_planned_vs_actual, "first" => $planned, "second" => $actual),
                        array("category" => $translation_planned_vs_remaining, "first" => $planned, "second" => $remaining),
                        array("category" => $translation_actual_vs_remaining, "first" => $actual, "second" => $remaining),

                    );
                    //$label_first = $category_amount[0]["category"]=="Budget vs Cost"?$translation_planned:$translation_actual;
                    //$label_second= $y++==0?$translation_actual:$translation_remaining;

                    // var_dump($category_amount[]["category"]);//0-budget vs cost 1-
                    BarChart::create(array(
                        // "title"=>"Sale Report",
                        "dataSource" => $category_amount,
                        "colorScheme" => array(
                            "#A9A9A9",//grey
                            "#808080",//dark grey


                        ),
                        "columns" => array(
                            "category",
                            "first" => array("label" => "", "type" => "number", "prefix" => "KWD"),
                            "second" => array("label" => "", "type" => "number", "prefix" => "KWD"),
                        ),
                        "options" => array(
                            "legend" => array(
                                "position" => "none", // Accept "bottom", "right", "inset"
                                "show" => false,
                            ),
                            // "pieSliceText" => 'value',
                        )


                    ));
                    ?>
                </div>
                <div class="row" id="colchart" style="text-align:center;">
                    <?php
                    $BAC = $this->dataStore('project_details')->sum("b_bac");
                    $EAC = $this->dataStore('project_details')->sum("b_eac");
                    $ETC = $this->dataStore('project_details')->sum("b_etc");
                    $remaining = $this->dataStore('project_details')->sum("remaining_budget");

                    $textbit1 = "actual";
                    $translation1 = ($language == 'en' ? $transtable->where('key_name', $textbit1)->where('key_pos', NULL)->get(0, "value_en") : $transtable->where('key_name', $textbit1)->where('key_pos', NULL)->get(0, "value_ar"));

                    $textbit2 = "forecast";
                    $translation2 = ($language == 'en' ? $transtable->where('key_name', $textbit2)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit2)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));


                    $category_amount1 = array(
                        array("category" => $translation1, "BAC" => $BAC, "EAC" => $EAC),
                        array("category" => $translation2, "BAC" => $remaining, "EAC" => $ETC),
                    );

                    ColumnChart::create(array(
                        // "title"=>"Sale Report",
                        "dataSource" => $category_amount1,
                        "colorScheme" => array(
                            "#A9A9A9",//grey
                            "#808080",//dark grey


                        ),
                        "columns" => array(
                            "category",
                            "BAC" => array("label" => "", "type" => "number", "prefix" => "KWD"),
                            "EAC" => array("label" => "", "type" => "number", "prefix" => "KWD"),
                        ),
                        "options" => array(
                            "legend" => array(
                                "position" => "none", // Accept "bottom", "right", "inset"
                                "show" => false,
                            ),
                        )

                    )); ?>
                </div>
                <div class="row">
                    <div class="col-sm-2"></div>

                    <div class="col-sm-5">
                        <div class="decoratedLine1 vlabelBold <?php echo($language == 'en' ? 'decoratedLine1En' :
                            'decoratedLine1Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>;font-size:11px;"><span><?php
                                $textbit = "bac";
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                                echo $translation; ?></span></div>
                    </div>
                    <div class="col-sm-5">
                        <div class="decoratedLine1 vlabelBold <?php echo($language == 'en' ? 'decoratedLine1En' :
                            'decoratedLine1Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>;font-size:11px;"><span><?php
                                $textbit = "remaining_budget";
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                                echo $translation; ?></span></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2"></div>

                    <div class="col-sm-5">
                        <div
                            class="decoratedLine2 vlabelBold <?php echo($language == 'en' ? 'decoratedLine2En' : 'decoratedLine2Ar') ?>"
                            style="<?php echo($language == 'en' ? 'padding-left: 40px;' :
                                'padding-right: 40px;'); ?>;font-size:11px"><span><?php
                                $textbit = "eac";
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                                echo $translation;
                                ?></span></div>
                    </div>
                    <div class="col-sm-5">
                        <div class="decoratedLine2 vlabelBold <?php echo($language == 'en' ? 'decoratedLine2En' :
                            'decoratedLine2Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>;font-size:11px;"><span><?php
                                $textbit = "etc";
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                                echo $translation;
                                ?></span></div>
                    </div>
                </div>
                <br>
            </div>
            <div class="col-sm-1" style="transform: translate(-100px,80px);">
                <div class="decoratedLine1 vlabelBold <?php echo($language == 'en' ? 'decoratedLine1En' :
                    'decoratedLine1Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>"><span style="font-size:10px;"><?php
                        $textbit = "planned_budget";
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                        echo $translation;
                        ?></span></div>
            </div>
            <div class="col-sm-1" style="transform: translate(-100px,80px);">
                <div class="decoratedLine2 vlabelBold <?php echo($language == 'en' ? 'decoratedLine2En' :
                    'decoratedLine2Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>"><span style="font-size:10px;"><?php
                        $textbit = "actual_cost";
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation;
                        ?></span></div>
            </div>

            <br>
            <br>
            <div class="col-sm-1" style="transform: translate(-100px,120px);">
                <div class="decoratedLine1 vlabelBold <?php echo($language == 'en' ? 'decoratedLine1En' :
                    'decoratedLine1Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>"><span style="font-size:10px;"><?php

                        $textbit = "planned_budget";
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                        echo $translation;
                        ?></span></div>
            </div>
            <div class="col-sm-1" style="transform: translate(-100px,120px);">
                <div class="decoratedLine2 vlabelBold <?php echo($language == 'en' ? 'decoratedLine2En' :
                    'decoratedLine2Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>"><span style="font-size:10px;"><?php

                        $textbit = "remaining_budget";
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation;
                        ?></span></div>
            </div>


            <div class="col-sm-1" style="transform: translate(-100px,160px);display: flex;">
                <div class="decoratedLine1 vlabelBold <?php echo($language == 'en' ? 'decoratedLine1En' :
                    'decoratedLine1Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>"><span style="font-size:10px;max"><?php
                        $textbit = "actual_cost";
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation; ?></span></div>
            </div>
            <div class="col-sm-1" style="transform: translate(-100px,160px);">
                <div class="decoratedLine2 vlabelBold <?php echo($language == 'en' ? 'decoratedLine2En' :
                    'decoratedLine2Ar') ?>" style="<?php echo($language == 'en' ? 'padding-left: 40px;' : 'padding-right:
                        40px;') ?>"><span style="font-size:10px;"><?php

                        $textbit = "remaining_budget";
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar"));
                        echo $translation;
                        ?></span></div>
            </div>


        </div>
    </div>

    <script type="text/javascript">
        KoolReport.load.onDone(function () {
            var locale = '<?php echo $language;?>';

            var table = $('#example').DataTable({
                destroy: true,
                "pageLength": 50,
                "language": {
                    "sProcessing": locale == 'ar' ? "جارٍ التحميل..." : "Processing...",
                    "sLengthMenu": locale == 'ar' ? "اعرض _MENU_ سجلات" : "Show _MENU_ entries",
                    "sZeroRecords": locale == 'ar' ? "لم يعثر على أية سجلات" : "No matching records found",
                    "sInfo": locale == 'ar' ? "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل" : "Showing _START_ to _END_ of _TOTAL_ entries",
                    "sInfoEmpty": locale == 'ar' ? "يعرض 0 إلى 0 من أصل 0 سجل" : "Showing 0 to 0 of 0 entries",
                    "sInfoFiltered": locale == 'ar' ? "(منتقاة من مجموع _MAX_ مُدخل)" : "(filtered from _MAX_ total entries)",
                    "sInfoPostFix": "",
                    "sSearch": locale == 'ar' ? "ابحث:" : "Search:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": locale == 'ar' ? "الأول" : "First",
                        "sPrevious": locale == 'ar' ? "السابق" : "Last",
                        "sNext": locale == 'ar' ? "التالي" : "Next",
                        "sLast": locale == 'ar' ? "الأخير" : "Previous",
                    },
                },
                // responsive: true,
                "columnDefs": [{
                    "searchable": true,
                    "orderable": true,
                    "targets": 0
                },
                    {
                        'visible': false,
                        'targets': 11,
                        // className: 'noVis'
                    },
                    {
                        'visible': false,
                        'targets': 12,
                        // className: 'noVis'
                    },

                ], "order": [[0, 'asc']],
                "scrollX": true,
                initComplete: function () {
                    this.api().columns([11, 12]).every(function () {
                        //var selectField = 'title'+i;
                        var column = this;
                        var title = $(column.header()).text().replace(/[\s()]+/gi, '');
                        console.log(title);
                        var select = $('#' + title)


                            .on('change', function () {

                                var data = $.map($(this).select2('data'), function (value, key) {
                                    console.log(value);
                                    return value.text ? '^' + $.fn.dataTable.util.escapeRegex(value.id) + '$' : null;
                                });

                                //if no data selected use ""
                                if (data.length === 0) {
                                    data = [""];
                                }
                                console.log(data)

                                //join array into string with regex or (|)
                                var val = data.join('|');
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });


                    });


                }
            });
            table.buttons().container().appendTo($('#button1'));

        });

        function myFunction() {
            var x = document.getElementById("myDIV");
            if (x.style.display === "none") {
                $(x).show('slow');
            } else {
                $(x).hide('slow');
            }
        };

        function printDiv() {

            var divElements = document.getElementById('pivot').innerHTML;
            var summary = document.getElementById('summary').innerHTML;
            var charts = document.getElementById('charts').innerHTML;
            var heading = "<?php echo $translation = ($language == 'en' ? $transtable->where('key_name', 'title')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'title')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar")); ?>";

            var table = $('#example').DataTable()
            // var heading="بيان عدم استخدام النظام"
            var tableTag = "<table id=\"example1\" class=\"table table-striped table-bordered color no-footer dataTable\" role=\"grid\" aria-describedby=\"example_info\" style=\"width: 1234px;\">";
            var thead = table.table().header().outerHTML;
            var rows = table.rows({search: 'applied'}).nodes();
            var rowStr = "";

            for (var i = 0; i < rows.length; i++)
                rowStr += rows[i].outerHTML;
            var dir = "<?php echo $language == 'ar' ? 'rtl' : 'ltr';?>"

            var datatest = tableTag + thead + rowStr//$('#example').prop('outerHTML')//$('#example').wrapAll('<div>').parent().html();
            data2 = "<div><h2 style=\"text-align:center !important;\">" + heading + "</h2><div><div>" + datatest + "</table></div><br><br><div> <h3 style=\"text-align:center !important;\">" + heading + "</h3><br><br>" + summary + "</div><div style=\"text-align:center;\">" + charts + "</div></div></div>";

            document.getElementById('printarea').style.display = "block";
            document.getElementById('printarea').innerHTML = data2;
            document.getElementById('printissue').style.display = "none";

            window.print(); // call print
            //    console.log(data2);

            document.getElementById('printissue').style.display = "block";
            document.getElementById('printarea').style.display = "none";


        }

        function linktokpi() {
            var m = $('#sector').val()
            var n = $('#section').val()
            console.log(n);
            document.cookie = "sectorname=" + m + ";domain=.najah.online; path=/; ";


            if (n != null)

                document.cookie = "orgname=" + n + ";domain=.najah.online; path=/; ";
            top.window.location.href = "https://dev.najah.online/kpilist";


        };
    </script>
    <style>
        .container1 {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
            display: inline-flex;
            column-width: 100%;
            padding: 0px 0px;
            background: #fff;
        }

        .buttons-print {
            background-color: #ffffff;
            border: none;
            /*color:black;*/
        }

        button.dt-button, div.dt-button, a.dt-button, a.dt-button:focus {
            border: none !important;
            background-color: #ffffff;
            background: none;
            padding: 0;
        }


        div.dt-button-collection {

            /* top: 19.6166px; */
            left: -98.433px;
            transform: translateX(-98px);
        }

        /* .card-body {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        } */
        .select2 {
            width: 100% !important;
            /*border: 1px solid #e8e8e8;*/
            min-height: 40px;
        }

        button.dt-button {
            font-size: 0.68em;
        }

        @media print {
            .removeTable {
                visibility: hidden;

                /* font-size: 50%; */
                /* transform: <\?php echo $language=='ar'?'scale(0.5); ':'scale(0.5); ' ;?>; */
                /* margin-right:20px;
                padding-right:20px; */
            }

            body {
                transform: <?php echo $language=='ar'?'scale(0.5) translate(300px,-400px) !important':'scale(0.7) translate(-150px,-150px) !important' ;?>;
                /* width:80%; */
            }

            /* a{
                visibility:hidden;
            } */
            a[href]:after {
                content: none !important;
            }


        }


        /* .select2:after{
            content: '';
            position:absolute;
            left:10px;
            top:15px;
            width:0;
            height:0;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-top: 5px solid #888;
            direction: rtl;
        }; */
    </style>
</div>
</body>
</html>
