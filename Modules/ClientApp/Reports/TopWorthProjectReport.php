<?php

namespace Modules\ClientApp\Reports;

use \koolreport\pivot\processes\Pivot;
use \koolreport\processes\Filter;
use \koolreport\processes\ColumnMeta;
use \koolreport\processes\CalculatedColumn;
use \koolreport\processes\Group;
use \koolreport\processes\ColumnRename;
use \koolreport\processes\Sort;
use \koolreport\processes\Custom;
use \koolreport\processes\Limit;
use \koolreport\pivot\processes\PivotExtract;
use \koolreport\processes\OnlyColumn;
use \koolreport\processes\CopyColumn;
use \koolreport\processes\ValueMap;
use \koolreport\cleandata\FillNull;
use Modules\ClientApp\User;
use Spatie\Permission\Models\Role;

error_reporting(E_ALL ^ E_NOTICE);

class TopWorthProjectReport extends \koolreport\KoolReport
{
    use \koolreport\clients\jQuery;
    use \koolreport\clients\Bootstrap;
    use \koolreport\clients\FontAwesome;

    //  use \koolreport\laravel\Friendship;


    // By adding above statement, you have claim the friendship between two frameworks
    // As a result, this report will be able to accessed all databases of Laravel
    // There are no need to define the settings() function anymore
    // while you can do so if you have other datasources rather than those
    // defined in Laravel.

    use \koolreport\inputs\Bindable;
    use \koolreport\inputs\POSTBinding;


    protected $language;

    function __construct(array $params = array())
    {
        // $this->id = $this->params['id'];

        $this->sect = $this->params['sect'];
        $this->org = $this->params['org'];
        $this->language = $params['language'];
        // $this->test=$params['test'];
        parent::__construct($params);
    }


    protected function defaultParamValues()
    {
        $this->src("mysql")->query("select name,value from system_vars")->pipe($this->dataStore("debug_mode"))->requestDataSending();
        $debug_modeprog = $this->dataStore("debug_mode")->get(0, "value");
        $debug_modeperf = $this->dataStore("debug_mode")->get(1, "value");


        $month =  date('n');
        $currentmtp = \DB::select(\DB::raw("select mtp.id, mtp.name, fys.start_date, curdate(), fye.end_date from mtp , fiscal_year fys, fiscal_year fye where
mtp.tenant_id = 1  and fys.id = mtp.mtp_start and fye.id = mtp.mtp_end and
CURDATE() >= fys.start_date and CURDATE() <= fye.end_date"));
        $currentmtpID = $currentmtp[0]->id;

        $currentmtpstartdate = $currentmtp[0]->start_date;
        $currentmtpenddate = $currentmtp[0]->end_date;
        $getAllYears = \DB::select(\DB::raw("SELECT @rownum:=@rownum+1 as no, f.*  FROM (SELECT @rownum:=0) r, `fiscal_year` as f WHERE start_date >= '$currentmtpstartdate' and start_date <= '$currentmtpenddate'"));

        $currentYear = 1;
        foreach ($getAllYears as $key => $years) {
            if($years->start_date < date('Y-m-d') && $years->end_date > date('Y-m-d')) {
                $currentYear = $years->no;
            }
        }

        $currentYearReal = $currentYear;

        $currentPeriod = '';
        if(in_array($month, [4,5,6])) {
            $currentPeriod = 1;
        } else if(in_array($month, [7,8,9])) {
            $currentPeriod = 2;
        } else if(in_array($month, [10,11,12])) {
            $currentPeriod = 3;
        } else if(in_array($month, [1,2,3])) {
            $currentPeriod = 4;
        }

        $currentPeriodReal = $currentPeriod;

        if($currentYear == 1 && $currentPeriod == 1) {
            $currentmtpID = count($currentmtp) > 0 ? $currentmtpID -1 : $currentmtpID;
            $currentPeriod = 4;
            $currentYear = 3;
        }
        if($currentPeriod == 1 && $currentYear != 1) {
            $currentPeriod = 4;
            $currentYear = $currentYear -1;
        }

        return array(
            "tenant" => env('TENANT_ID'),
            "parent" => null,
            "sector" => null,
            "section" => null,
            "mtp" => ($currentPeriodReal == 1 && $currentYearReal == 1) ? $currentmtpID : $currentmtp[0]->id,
            "status1" => "",
            "expand" => 1,
            //  "filter"=>1,
            "expand1" => "",
            "top_performing" => 5,
            //  "filter"=>1,
            "first_time" => 0,

            "sector1" => null,
            "section1" => null,
            "mtp1" => 4,
            "top_performing1" => 5,
            "value_gt" => 60,
            "value_lt" => 30,
            "radiolist" => "3",
            "operational_status" => [],
            "project_cat" => "",
            "project_type" => [],

            "radiolist1" => "3",
            "operational_status1" => [],
            "project_cat1" => "",
            "project_type1" => [],
            "value_gt1" => 60,
            "value_lt1" => 30,
            "debug_modeprog" => $debug_modeprog,//false,
            "debug_modeperf" => $debug_modeperf,//true,


        );
    }

    protected function bindParamsToInputs()
    {
        return array(
            "tenant",
            "parent",
            "sector",
            "section",
            "mtp",
            "status1",
            "expand",
            "top_performing",
            // "filter",
            "sector1",
            "section1",
            "mtp1",
            "first_time",
            "value_gt",
            "value_lt",
            "radiolist",
            "operational_status",
            "project_cat",
            "project_type",

            "expand1",
            "radiolist1",
            "value_gt1",
            "value_lt1",
            "radiolist",
            "operational_status1",
            "project_cat1",
            "project_type1",
            "top_performing1",
            "debug_modeprog",
            "debug_modeperf",


        );
    }

    public function settings()
    {
        return array(
            // 'assets' => array(
            //     'path' => '../../../public',
            //     'url' => 'public/',
            // ),
            "dataSources" => array(
                "mysql" => array(
                    'host' => env('DB_HOST'),
                    'username' => env('DB_USERNAME'),
                    'password' => env('DB_PASSWORD'),
                    'dbname' => env('DB_DATABASE'),
                    'charset' => 'utf8',
                    'class' => "\koolreport\datasources\MySQLDataSource",
                ),
            )


        );
    }

    function setup()
    {
        $language = '';
        if (isset($this->params['language']) && !empty($this->params['language'])) {
            $language = $this->params['language'];
        }

        if (isset($_POST['expand'])) {

            $this->params["radiolist"] = $this->params["radiolist1"];
            $this->params["sector"] = $this->params["sector1"];
            $this->params["section"] = $this->params["section1"];
            $this->params["mtp"] = $this->params["mtp1"];
            $this->params["project_cat"] = $this->params["project_cat1"];
            $this->params["top_performing"] = $this->params["top_performing1"];
            $this->params["value_gt"] = $this->params["value_gt1"];
            $this->params["value_lt"] = $this->params["value_lt1"];
            $pt = explode(',', $this->params["project_type1"]);
            $key = array_search("", $pt); // $key = 2;
            if ($key)
                $this->params["project_type"] = explode(',', $this->params["project_type1"]);


            $os = explode(',', $this->params["operational_status1"]);
            $key = array_search("", $os); // $key = 2;
            if ($key)
                $this->params["operational_status"] = explode(',', $this->params["operational_status1"]);

        }
        if (isset($_POST['expand1'])) {

            $this->params["expand"] = $this->params["expand1"];

        }

        if (empty($_POST['sector']))
            $this->params['sector'] = $this->params['sid'];

        if (empty($_POST['section']))
            $this->params['section'] = $this->params['oid'];

        if ($this->params['sector'] == "" || $this->params['sector'] == "null")
            $this->params['sector'] = null;

        if ($this->params['section'] == "" || $this->params['section'] == "null")
            $this->params['section'] = null;

        if ($this->params['sector'] != 'null' && $this->params['sector'] != '') {
            if ($this->params['sector']) {
                $id = $this->params['sector'];

                $ddd = \DB::select(\DB::raw("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = $id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), $id, '', CONCAT(id, '') from subtenant where parent_id = $id) select id, name from cte order by path"));
                $sectorKeys = [];
                foreach ($ddd as $dd) {
                    if ($dd->id) {
                        $sectorKeys[] = $dd->id;
                    }
                }
            }

            if (!empty($_POST['sector']) && (int)$_POST['sector'] != (int)$this->params['sid']) {
                if (!in_array($_POST['section'], $sectorKeys)) {
                    $this->params['section'] = "";
                }
                //$this->params['section'] = "";
            } else {
                if (!in_array($this->params['section'], $sectorKeys)) {
                    $this->params['section'] = "";
                } else {
                    $this->params['section'] = $this->params['oid'];
                }
            }

            if($this->params['sid'] == $this->params['sector']) {
                if(!empty($_POST['section'])) {
                    $this->params['section'] = (!in_array($this->params['section'], $sectorKeys)) ? $this->params['oid']
                        : $_POST['section'];
                }
            }

            if(!empty($_POST['sector']) && $_POST['sector'] == 2) {
                $this->params['section'] = "";
            }
        }

        if (isset($this->params['sector']) && !empty($this->params['sector']) && $this->params['sector'] != "") {
            $parent_id = $this->params["sector"];
            if (isset($this->params['section']) && !empty($this->params['section']) && $this->params['section'] != "") {
                $parent_id = $this->params["section"];
            } else {
                $parent_id = $this->params["sector"];
            }
        } else {

            $parent_id = null;

        }


        //    echo "<pre>";

        // $date2=date("Y-m-d");
        $limit = $this->params['top_performing'];
        $this->params["parent"] = $parent_id;

        $value_lt = $this->params['value_lt'] / 100;
        $value_gt = $this->params['value_gt'] / 100;
        if ($this->params['operational_status'] != "")
            $op_status = array_map(function ($val) {
                return (int)$val - 1;
            }, $this->params['operational_status']);

        $project_cat = (int)$this->params['project_cat'] - 1;

        // var_dump($this->params['project_list']);
        // print_r($this->params['project_type']);
        // if($this->params['expand']==2)
        // {
        //     $this->params['radiolist']=$this->params['radiolist1'];
        // }
        //   echo $this->params['expand'];
        //  ->query("set @period_name = 'Y';")
        //  ->query("set @year_no = 1;")
        //  ->query(" set @mtp_id = 4;")
        //  ->query("  set @sub_id = 114;")
        //  /**set your arg here, value list is {Q1, Q2, Q3, Q4, H1, H2, Y} for quarter/half annual/annual**/
        /**set your arg here**/
        // var_dump($this->params["radiolist"]);
        //----------------------------------------performing--------------------------------
        $this->src("mysql")
            ->query("select t1.proj_id, t1.proj_symbol, t1.proj_name, t1.org_unit_id, t1.org_unit_name,
                                t1.status_operational, t1.project_category, t1.project_type,
                                pv.progress_val, pvs.proj_perf,
                                case when pvs.proj_perf > $value_gt then 'gt'
                                                    when pvs.proj_perf < $value_gt then 'lt'
                                end as gt_or_lt,
                                ps.name as proj_status_name, ps.color_code

        from (select p.id as proj_id, p.symbol as proj_symbol, p.name_short as proj_name, sub.id as org_unit_id, sub.name as org_unit_name,
                                p.status_operational, p.project_category, p.project_type,
                                p.start_dt_actual, p.end_dt_base,
                                 (select	pvs.id from proj_value_stats pvs, proj_values pv where
                                                        pvs.id = pv.id and
                                                        pv.project_id = p.id and
                                                        pv.period_dt = (select max(pv_i.period_dt) from proj_values pv_i, proj_value_stats pvs_i where pv_i.project_id = pv.project_id and
pvs_i.id = pv_i.id and
pv_i.progress_val is not null)) as pvs_id
                    from project p, subtenant sub, mtp, fiscal_year fs_start, fiscal_year fs_end where
                             p.tenant_id = :tenant_id and
                                (ifnull(p.subtenant_id, p.sector_id) = :parent_id or  :parent_id is null) and  sub.id = ifnull(p.subtenant_id, p.sector_id) and mtp.id = :mtp and mtp.mtp_start = fs_start.id and
                                mtp.mtp_end = fs_end.id and
                                (
                                            (p.mtp_id =:mtp) or
                                            (p.start_dt_actual between fs_start.start_date and fs_end.end_date)
                                            or
                                            (ifnull(p.end_dt_actual, CURDATE()) between fs_start.start_date and fs_end.end_date)
                                            or
                                            (p.start_dt_actual < fs_start.start_date and ifnull(p.end_dt_actual, CURDATE()) > fs_end.end_date)
                                )
        ) as t1
        , proj_values pv, proj_value_stats pvs, proj_status ps where
                    pv.id = t1.pvs_id and
                    pvs.id = t1.pvs_id and
                    ps.id = pvs.proj_status and
                    (pvs.proj_perf > $value_gt or pvs.proj_perf < $value_lt)
        order by gt_or_lt, if(gt_or_lt = 'gt', pvs.proj_perf, null) desc, if(gt_or_lt = 'lt', pvs.proj_perf, null) asc;")
            ->params(array(":parent_id" => $this->params["parent"], ":sector_id" => $this->params["sector"], ":mtp" => $this->params["mtp"], ":value_gt" => $this->params["value_gt"], ":value_lt" => $this->params["value_lt"], ":tenant_id" => $this->params["tenant"]))
            ->pipe(new Limit(array($this->params['top_performing'])))
            ->saveTo($nodep);

        if (!empty($this->params['operational_status'])) {
            $nodep->pipe(new Filter(array(
                array("status_operational", "in", $op_status)
            )))
                ->saveTo($nodep);
        }
        if (!empty($this->params['project_cat'])) {
            $nodep->pipe(new Filter(array(
                array("project_category", "=", $project_cat)
            )))
                ->saveTo($nodep);
        }
        if (!empty($this->params['project_type'])) {
            $nodep->pipe(new Filter(array(
                array("project_type", "in", $this->params['project_type'])
            )))
                ->saveTo($nodep);
        }


        $nodep->pipe(new ColumnMeta(array(

            "proj_perf1" => array(
                "type" => "number",
            )
        )))
            ->pipe(new CopyColumn(array(
                "proj_perf1" => "proj_perf",
                // "copy_of_amount"=>"amount"
            )))
            ->pipe(new ValueMap(array(
                "proj_perf1" => array(
                    "{func}" => function ($value) {
                        // switch ($value) {
                        static $i = 1;
                        $i++;
                        return $i;

                    }
                )
            )))
            //  ->pipe(new Sort(array(
            //     "proj_perf1"=>"desc",
            //     // "name"=>"desc"
            // )))
            ->pipe(new Custom(function ($row) {
                if ($row["proj_perf"] != NULL) {
                    $perf_value = $row["proj_perf"] * 100;
                    $row["proj_perf"] = round($perf_value, 2);
                    if ($this->params['debug_modeperf'] == false && $row["proj_perf"] > 100)
                        $row["proj_perf"] = 100;


                }

                return $row;
            }))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['proj_perf'])) {
                        $row['proj_perf'] = $row['proj_perf'] . '%';
                    }
                    return $row;
                },

            ]))
            ->pipe(new Custom(function ($row) {
                if ($row["progress_val"] != NULL) {
                    $perf_value = $row["progress_val"] * 100;
                    $row["progress_val"] = round($perf_value, 2);
                    if ($this->params['debug_modeprog'] == false && $row["progress_val"] < 0)
                        $row["progress_val"] = 0;


                }

                return $row;
            }))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['progress_val'])) {
                        $row['progress_val'] = $row['progress_val'] . '%';
                    }
                    return $row;
                },

            ]))
            // ->pipe(new Limit(array($this->params['top_performing'])))
            ->pipe(new CalculatedColumn(array(
                "id" => "{#}+1",
                "perf_name" => array(
                    "exp" => function ($data) {
                        if ($data != NULL) {
                            $perdata = str_replace("%", "", $data["proj_perf"]);
                            if (($perdata < $this->params["value_lt"]) && ($perdata != NULL)) {
                                $translation = ($this->params['language'] == 'en' ? 'Not Performing' : 'غير مؤدي');
                                return $translation;
                            }
                            if (($perdata >= $this->params["value_gt"]) && ($perdata != NULL)) {
                                $translation = ($this->params['language'] == 'en' ? 'Performing' : 'مؤدي');
                                return $translation;
                            } else
                                return "rest";
                        }
                        return NULL;
                    }),

            )))
            ->pipe(new OnlyColumn(array(
                "perf_name",
                "org_unit_name",
                "proj_name",
                "progress_val",
                "proj_status_name",
                "proj_perf",
            )))
            ->pipe(new Filter(array(
                array("perf_name", "!=", "rest"),
                //
            )))
            ->pipe(new Filter(array(
                array("perf_name", "=", "Performing"),
                "or",
                array("perf_name", "=", "مؤدي"),

            )))
            //  ->pipe(new Limit(array($this->params['top_performing'])))
            // ->pipe($this->dataStore('test'))

            ->saveTo($node_perf);
        // ->pipe(new Sort(array(
        //     "proj_perf"=>"desc"
        // )))

        $node_perf->pipe(new Pivot(array(
            "dimensions" => array(
                "row" => "perf_name,org_unit_name,proj_name,progress_val,proj_status_name,proj_perf",
                // "column"=>"kpi_symbol,kpi_name,next_reading_date"
            ),
            'aggregates' => array(
                'sum' => 'proj_perf1',
                'count' => 'proj_perf'
            )

        )))->saveTo($node3);
// //------------------barchart-count-----------------------------------
//         $node3->pipe(new PivotExtract(array(
//             "row" => array(
//                 "parent" => array(),
//             ),
//             "column" => array(
//                 "parent" => array(),
//             ),
//             "measures" => array(
//                 "proj_perf - count",
//             ),
//         )))
//             ->pipe($this->dataStore('chartTable1'));
        $node3->pipe($this->dataStore('performing'));


        // $source ->pipe(new Filter(array(
        //     array("perf_name","=","nonperforming")
        // )))

        //    if($this->params['section']=="")
        $node_perf->pipe($this->dataStore('performing1'));

        //  var_dump($this->params['radiolist']);
        //------------------nonperforming(node2)--------------------------


        $this->src("mysql")
            ->query("select t1.proj_id, t1.proj_symbol, t1.proj_name, t1.org_unit_id, t1.org_unit_name,
                                t1.status_operational, t1.project_category, t1.project_type,
                                pv.progress_val, pvs.proj_perf,
                                case when pvs.proj_perf > $value_gt then 'gt'
                                                    when pvs.proj_perf < $value_gt then 'lt'
                                end as gt_or_lt,
                                ps.name as proj_status_name, ps.color_code

        from (select p.id as proj_id, p.symbol as proj_symbol, p.name_short as proj_name, sub.id as org_unit_id, sub.name as org_unit_name,
                                p.status_operational, p.project_category, p.project_type,
                                p.start_dt_actual, p.end_dt_base,
                                 (select	pvs.id from proj_value_stats pvs, proj_values pv where
                                                        pvs.id = pv.id and
                                                        pv.project_id = p.id and
                                                        pv.period_dt = (select max(pv_i.period_dt) from proj_values pv_i, proj_value_stats pvs_i where pv_i.project_id = pv.project_id and
pvs_i.id = pv_i.id and
pv_i.progress_val is not null)) as pvs_id
                    from project p, subtenant sub, mtp, fiscal_year fs_start, fiscal_year fs_end where
                             p.tenant_id = :tenant_id and
                                (ifnull(p.subtenant_id, p.sector_id) = :parent_id or  :parent_id is null) and  sub.id = ifnull(p.subtenant_id, p.sector_id) and mtp.id = :mtp and mtp.mtp_start = fs_start.id and
                                mtp.mtp_end = fs_end.id and
                                (
                                            (p.mtp_id =:mtp) or
                                            (p.start_dt_actual between fs_start.start_date and fs_end.end_date)
                                            or
                                            (ifnull(p.end_dt_actual, CURDATE()) between fs_start.start_date and fs_end.end_date)
                                            or
                                            (p.start_dt_actual < fs_start.start_date and ifnull(p.end_dt_actual, CURDATE()) > fs_end.end_date)
                                )
        ) as t1
        , proj_values pv, proj_value_stats pvs, proj_status ps where
                    pv.id = t1.pvs_id and
                    pvs.id = t1.pvs_id and
                    ps.id = pvs.proj_status and
                    (pvs.proj_perf > $value_gt or pvs.proj_perf < $value_lt)
        order by gt_or_lt, if(gt_or_lt = 'gt', pvs.proj_perf, null) desc, if(gt_or_lt = 'lt', pvs.proj_perf, null) asc;")
            ->params(array(":parent_id" => $this->params["parent"], ":sector_id" => $this->params["sector"], ":mtp" => $this->params["mtp"], ":value_gt" => $this->params["value_gt"], ":value_lt" => $this->params["value_lt"], ":tenant_id" => $this->params["tenant"]))
            ->pipe(new Limit(array($this->params['top_performing'])))
            ->saveTo($noden);
        if (!empty($this->params['operational_status'])) {
            $noden->pipe(new Filter(array(
                array("status_operational", "in", $op_status)
            )))
                ->saveTo($noden);
        }
        if (!empty($this->params['project_cat'])) {
            $noden->pipe(new Filter(array(
                array("project_category", "=", $project_cat)
            )))
                ->saveTo($noden);
        }
        if (!empty($this->params['project_type'])) {
            $noden->pipe(new Filter(array(
                array("project_type", "in", $this->params['project_type'])
            )))
                ->saveTo($noden);
        }


        $noden->pipe(new ColumnMeta(array(

            "proj_perf1" => array(
                "type" => "number",
                // "label"=>"Order Date",
                // 'suffix' => "%",
            )
        )))
            ->pipe(new Custom(function ($row) {
                if ($row["proj_perf"] != NULL) {
                    $perf_value = $row["proj_perf"] * 100;
                    $row["proj_perf"] = round($perf_value, 2);
                    if ($this->params['debug_modeperf'] == false && $row["proj_perf"] > 100)
                        $row["proj_perf"] = 100;


                }
                // if ($row["dept_name"] == NULL) {
                //     $row["dept_name"] = "";
                // }
                // if ($row["sub_name"] == NULL) {
                //     $row["sub_name"] = "";
                // }
                // if ($row["sector_name"] == NULL) {
                //     $row["sector_name"] = "";
                // }
                return $row;
            }))
            ->pipe(new CopyColumn(array(
                "proj_perf1" => "proj_perf",
                // "copy_of_amount"=>"amount"
            )))
            ->pipe(new ValueMap(array(
                "proj_perf1" => array(
                    "{func}" => function ($value) {
                        // switch ($value) {
                        static $i = 1;
                        $i++;
                        return $i;

                    }
                )
            )))
            ->pipe(new Sort(array(
                "proj_perf1" => "asc",
                // "name"=>"desc"
            )))
            // ->pipe(new Limit(array($this->params['top_performing'])))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['proj_perf'])) {
                        $row['proj_perf'] = $row['proj_perf'] . '%';
                    }
                    return $row;
                },

            ]))
            ->pipe(new Custom(function ($row) {
                if ($row["progress_val"] != NULL) {
                    $perf_value = $row["progress_val"] * 100;
                    $row["progress_val"] = round($perf_value, 2);
                    if ($this->params['debug_modeprog'] == false && $row["progress_val"] < 0)
                        $row["progress_val"] = 0;


                }

                return $row;
            }))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['progress_val'])) {
                        $row['progress_val'] = $row['progress_val'] . '%';
                    }
                    return $row;
                },

            ]))
            ->pipe(new CalculatedColumn(array(
                "id" => "{#}+1",
                "perf_name" => array(
                    "exp" => function ($data) {
                        if ($data != NULL) {
                            $perdata = str_replace("%", "", $data["proj_perf"]);
                            if (($perdata < ($this->params["value_lt"])) && ($perdata != NULL)) {
                                $translation = ($this->params['language'] == 'en' ? 'Not Performing' : 'غير مؤدي');
                                return $translation;
                            } else if (($perdata >= ($this->params["value_gt"])) && ($perdata != NULL)) {
                                $translation = ($this->params['language'] == 'en' ? 'Performing' : 'مؤدي');
                                return $translation;
                            } else
                                return "rest";
                        } else
                            return NULL;
                    }),

            )))
            ->saveTo($source_nonperf);

        $source_nonperf->pipe(new OnlyColumn(array(
            "perf_name",
            "org_unit_name",
            "proj_name",
            "progress_val",
            "proj_status_name",
            "proj_perf",
            "proj_perf1"
        )))
            ->pipe(new Filter(array(
                array("perf_name", "!=", "rest"),
                //
            )))
            ->pipe(new Filter(array(
                array("perf_name", "=", "Not Performing"),
                "or",
                array("perf_name", "=", "غير مؤدي"),

            )))
            // ->pipe(new Sort(array(

            //     "proj_perf"=>"asc"
            //  )))
            // ->pipe(new Limit(array($this->params['top_performing'])))

            ->saveTo($node_nonperf)
            // ->pipe($source)


            ->pipe(new Pivot(array(
                "dimensions" => array(
                    "row" => "perf_name,org_unit_name,proj_name,proj_status_name,proj_perf",
                    // "column"=>"kpi_symbol,kpi_name,next_reading_date"
                ),
                'aggregates' => array(
                    'sum' => 'proj_perf1',
                    'count' => 'proj_perf'
                )

            )))->saveTo($node2);
        // if($this->params['section']=="")
        $node2->pipe($this->dataStore('nonperforming'));
//node2
//--------------------------Barchart Count nonperf----------------------
//         $node2->pipe(new PivotExtract(array(
//             "row" => array(
//                 "parent" => array(),
//             ),
//             "column" => array(
//                 "parent" => array(),
//             ),
//             "measures" => array(
//                 "proj_perf - count",
//             ),
//         )))
//             ->pipe($this->dataStore('chartTable2'));
// //------------------------------------------------------------
//         $node_nonperf->pipe($this->dataStore('nonperforming1'));
//node_perf


//----------------------------------------combined------------------------------

        $this->src("mysql")
            ->query("select t1.proj_id, t1.proj_symbol, t1.proj_name, t1.org_unit_id, t1.org_unit_name,
                        t1.status_operational, t1.project_category, t1.project_type,
                        pv.progress_val, pvs.proj_perf,
                        case when pvs.proj_perf > $value_gt then 'gt'
                                            when pvs.proj_perf < $value_gt then 'lt'
                        end as gt_or_lt,
                        ps.name as proj_status_name, ps.color_code

from (select p.id as proj_id, p.symbol as proj_symbol, p.name_short as proj_name, sub.id as org_unit_id, sub.name as org_unit_name,
                        p.status_operational, p.project_category, p.project_type,
                        p.start_dt_actual, p.end_dt_base,
                         (select	pvs.id from proj_value_stats pvs, proj_values pv where
                                                pvs.id = pv.id and
                                                pv.project_id = p.id and
                                                pv.period_dt = (select max(pv_i.period_dt) from proj_values pv_i, proj_value_stats pvs_i where pv_i.project_id = pv.project_id and
pvs_i.id = pv_i.id and
pv_i.progress_val is not null)) as pvs_id
            from project p, subtenant sub, mtp, fiscal_year fs_start, fiscal_year fs_end where
                     p.tenant_id = :tenant_id and
                        (ifnull(p.subtenant_id, p.sector_id) = :parent_id or  :parent_id is null) and  sub.id = ifnull(p.subtenant_id, p.sector_id) and mtp.id = :mtp and mtp.mtp_start = fs_start.id and
                        mtp.mtp_end = fs_end.id and
                        (
                                    (p.mtp_id =:mtp) or
                                    (p.start_dt_actual between fs_start.start_date and fs_end.end_date)
                                    or
                                    (ifnull(p.end_dt_actual, CURDATE()) between fs_start.start_date and fs_end.end_date)
                                    or
                                    (p.start_dt_actual < fs_start.start_date and ifnull(p.end_dt_actual, CURDATE()) > fs_end.end_date)
                        )
) as t1
, proj_values pv, proj_value_stats pvs, proj_status ps where
            pv.id = t1.pvs_id and
            pvs.id = t1.pvs_id and
            ps.id = pvs.proj_status and
            (pvs.proj_perf > $value_gt or pvs.proj_perf < $value_lt)
order by gt_or_lt, if(gt_or_lt = 'gt', pvs.proj_perf, null) desc, if(gt_or_lt = 'lt', pvs.proj_perf, null) asc;")
            ->params(array(":parent_id" => $this->params["parent"], ":sector_id" => $this->params["sector"], ":mtp" => $this->params["mtp"], ":value_gt" => $this->params["value_gt"], ":value_lt" => $this->params["value_lt"], ":tenant_id" => $this->params["tenant"]))
            ->pipe(new Limit(array($this->params['top_performing'])))

// ->pipe(new ValueMap(array(
//     "dept_name"=>array(
//         NULL=>"-",

//     )
// )))
            ->saveTo($nodex);
        if (!empty($this->params['operational_status'])) {
            $nodex->pipe(new Filter(array(
                array("status_operational", "in", $op_status)
            )))
                ->saveTo($nodex);
        }
        if (!empty($this->params['project_cat'])) {
            $nodex->pipe(new Filter(array(
                array("project_category", "=", $project_cat)
            )))
                ->saveTo($nodex);
        }
        if (!empty($this->params['project_type'])) {
            $nodex->pipe(new Filter(array(
                array("project_type", "in", $this->params['project_type'])
            )))
                ->saveTo($nodex);
        }

// ->pipe(new Limit(array($this->params['top_performing'])))

        $nodex->pipe(new ColumnMeta(array(

            "proj_perf1" => array(
                "type" => "number",

            )
        )))
            ->pipe(new Custom(function ($row) {
                if ($row["proj_perf"] != NULL) {
                    $perf_value = $row["proj_perf"] * 100;
                    $row["proj_perf"] = round($perf_value, 2);
                    if ($this->params['debug_modeperf'] == false && $row["proj_perf"] > 100)
                        $row["proj_perf"] = 100;


                }
                return $row;
            }))
            ->pipe(new CopyColumn(array(
                "proj_perf1" => "proj_perf",
                // "copy_of_amount"=>"amount"
            )))
            ->pipe(new ValueMap(array(
                "proj_perf1" => array(
                    "{func}" => function ($value) {
                        // switch ($value) {
                        static $i = 1;
                        $i++;
                        return $i;

                    }
                )
            )))
            ->pipe(new Sort(array(
                "proj_perf1" => "asc",
                // "name"=>"desc"
            )))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['proj_perf'])) {
                        $row['proj_perf'] = $row['proj_perf'] . '%';
                    }
                    return $row;
                },

            ]))
            ->pipe(new Custom(function ($row) {
                if ($row["progress_val"] != NULL) {
                    $perf_value = $row["progress_val"] * 100;
                    $row["progress_val"] = round($perf_value, 2);
                    if ($this->params['debug_modeprog'] == false && $row["progress_val"] < 0)
                        $row["progress_val"] = 0;


                }

                return $row;
            }))
            ->pipe(new \koolreport\processes\Map([
                '{value}' => function ($row, $meta, $index, $mapState) {
                    if (isset($row['progress_val'])) {
                        $row['progress_val'] = $row['progress_val'] . '%';
                    }
                    return $row;
                },

            ]))
// ->pipe(new Limit(array($this->params['top_performing'])))
            ->pipe(new CalculatedColumn(array(
                "id" => "{#}+1",
                "perf_name" => array(
                    "exp" => function ($data) {

                        $perdata = str_replace("%", "", $data["proj_perf"]);
                        if (($perdata < $this->params["value_lt"]) && ($perdata != NULL)) {
                            $translation = ($this->params['language'] == 'en' ? 'Not Performing' : 'غير مؤدي');
                            return $translation;
                        } else if (($perdata >= $this->params["value_gt"]) && ($perdata != NULL)) {
                            $translation = ($this->params['language'] == 'en' ? 'Performing' : 'مؤدي');
                            return $translation;
                        } else
                            return "rest";


                    }),

            )))
            ->pipe(new OnlyColumn(array(
                "perf_name",
                "org_unit_name",
                "proj_name",
                "progress_val",
                "proj_status_name",

                // "kpi_symbol",
                "proj_perf",
                "proj_perf1"
            )))
            ->pipe(new Filter(array(
                array("perf_name", "!=", "rest")
            )))
// ->pipe($this->dataStore('test'))

            ->pipe(new Pivot(array(
                "dimensions" => array(
                    "row" => "perf_name,org_unit_name,proj_name,progress_val,proj_status_name,proj_perf",
                    // "column"=>"kpi_symbol,kpi_name,next_reading_date"
                ),
                'aggregates' => array(
                    'sum' => 'proj_perf1',
                    'count' => 'proj_perf1'
                )

            )))
            // ->saveTo($node_combined);


            ->pipe($this->dataStore("combined"));

        //-------------------To get work on behalf roles----------------------------------------------------------------------------------

        $userDetails = User::find($this->params["uid"]);
        if ($this->params["uid"] != "null") {

            /*$this->src("mysql")
                ->query("select model_has_roles.role_id, model_has_roles.model_id, role_work_on_behalf_sectors.sector_id, role_work_on_behalf_sectors.subtenant_id from model_has_roles INNER JOIN role_work_on_behalf_sectors ON role_work_on_behalf_sectors.role_id = model_has_roles.role_id")
                ->pipe(new Filter(array(
                    array("model_id", "=", $this->params["uid"])
                )))
                ->pipe($this->dataStore('role1'))->requestDataSending();*/

            $getRole = Role::where('name', $userDetails->currentRole)->pluck('id')->all();
            $this->src("mysql")
                ->query("select model_has_roles.role_id, model_has_roles.model_id, role_work_on_behalf_sectors.sector_id, role_work_on_behalf_sectors.subtenant_id from model_has_roles INNER JOIN role_work_on_behalf_sectors ON role_work_on_behalf_sectors.role_id = model_has_roles.role_id
                where 1=1
            " . (" and model_id IN ('" . $this->params["uid"] . "')") . "
            " . (" and model_has_roles.role_id in ('" . $getRole[0] . "')") . "
            ")->pipe($this->dataStore('role1'))->requestDataSending();

        }

        $role_sector = $this->dataStore("role1")->only("sector_id")->data();
        $role_subtenant = $this->dataStore("role1")->only("subtenant_id")->data();
        $role_id = $this->dataStore("role1")->get(0, "role_id");
        $i = 0;

        // var_dump($role_sector)--fetching role sector;
        foreach ($role_sector as $key => $value) {
            foreach ($value as $k => $v) {
                $r_sect[$i] = $v;
                $i = $i + 1;
            }
        }
        $r_sect[$i] = (int)$this->params["sid"];
        if(in_array(2, $r_sect)) {
            $merge= [2,3,4,5,6,7,8,9,10];
            $r_sect = $merge;//array_diff( $merge, [2] );
        }
        $m = 0;

        foreach ($role_subtenant as $key => $value) {
            foreach ($value as $k => $v) {
                $r_sub[$m] = $v;
                $m = $m + 1;
            }
        }
        $p_sub = $r_sub;
        $p_sub[$m] = (int)$this->params["oid"];

//----------------------------merging sid with role sector-----------------------------------------------------------------------------------

        if ($this->params["sid"] == "null") {

            $this->src("mysql")
                ->query("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)")//and s.subtenant_type_id in (2,3)
                ->pipe($this->dataStore('sector1'));
        }
        if ($this->params["sid"] != "null") {
            $this->src("mysql")
                ->query("select id, name from subtenant s where s.tenant_id=1 and s.subtenant_type_id in (2,3)")
                ->pipe(new Filter(array(
                    array("id", "in", $r_sect)
                )))
                ->saveTo($node);
            $node->pipe($this->dataStore('sector1'));


        }

        //----------------------------merging sid with role subtenant----------------------------------------------------------------------------------
        if ($this->params["sector"] != null) {

// ----------------------------------------selecting roles corresponding to this sector--------------------------------------------
            if ($this->params["oid"] != "null") {
                if (!empty(array_intersect($p_sub, $sectorKeys))) {
                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name, path from cte order by path")
                        ->params(array(":sector_id" => $this->params["sector"]))
                        ->pipe(new Filter(array(
                            array("id", "in", $p_sub)
                        )))
                        ->pipe(new Custom(function ($row) {
                            if ($row["id"] != NULL)
                                return $row;
                        }))
                        ->pipe($this->dataStore('section111'))->requestDataSending(); //sections corresponding to that sector
                } else {
                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name, path from cte order by path")
                        ->params(array(":sector_id" => $this->params["sector"]))
                        ->pipe($this->dataStore('section222'))->requestDataSending(); //sections
                }
            }

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
//-----------------------------generating each org unit datastore------------------------------
            if ($this->datastore('section111')->count() > 0) {
                foreach ($r_section as $key => $v) {

                    $this->src("mysql")
                        ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '') from subtenant where id = :section_id UNION ALL select s.id, concat(CONCAT(c.level, ''), '', s.name), s.parent_id, CONCAT(c.level, ''), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('', 50), :section_id, '', CONCAT(id, '') from subtenant where id = :section_id) select id,  name from cte order by path")
                        ->params(array(":section_id" => $v))
                        ->saveTo($node_test);

                    $section_name = "sect" . $v;
                    $node_test->pipe($this->dataStore($section_name));

                }


            }


            //-------------------------------------------------------------------------------------------------------------------------------
            if ($this->params["oid"] == "null") {

                $this->src("mysql")
                    ->query("WITH RECURSIVE cte (id, name, parent_id, level, path) AS (select id, name, parent_id, CAST('' AS CHAR(10)), concat( cast(id as char(200)), '_') from subtenant where parent_id = :sector_id UNION ALL select s.id, concat(CONCAT(c.level, '='), '> ', s.name), s.parent_id, CONCAT(c.level, '='), CONCAT(c.path, ',', s.id) from subtenant s inner join cte c on s.parent_id = c.id UNION ALL select null, repeat('_', 50), :sector_id, '', CONCAT(id, '') from subtenant where parent_id = :sector_id) select id, name from cte order by path")
                    ->params(array(":sector_id" => $this->params["sector"]))
                    ->pipe($this->dataStore('section1'));
            }

        }

        $this->src("mysql")
            ->query("select id,name,status_operational from project")
            ->pipe($this->dataStore('project_name'));

        $this->src("mysql")
            ->query("select id,name from proj_type where tenant_id=:tenant_id")
            ->params(array(":tenant_id" => $this->params["tenant"]))
            ->pipe($this->dataStore('project_type'));

        $this->src("mysql")
            ->query("select id,name from mtp")
            // ->pipe($this->dataStore('mtp1'));
            ->pipe($this->dataStore('mtp1'));


        $this->src("mysql")
            ->query("select * from trans_table")
            ->pipe($this->dataStore('translation'))->requestDataSending();

    }
}
