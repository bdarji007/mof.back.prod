<?php

use \koolreport\widgets\koolphp\Table;
use \koolreport\processes\CalculatedColumn;
use \koolreport\inputs\BSelect;
use \koolreport\inputs\Select;
use \koolreport\processes\Sort;
use \koolreport\inputs\Select2;
use \koolreport\datagrid\DataTables;
use \koolreport\sparklines;
use \koolreport\inputs\DateTimePicker;
use Modules\ClientApp\Reports\ProjectRiskSubReport;

//  $s=2;
//  $data = 10 ** $s;
// //  $data=$data+'%';

// var_dump($data);

$org_unit = '';
if (isset($this->params['org_unit']) && !empty($this->params['org_unit'])) {
    $org_unit = $this->params['org_unit'];
    // var_dump($org_unit);
}

$kpi_symbol = '';
if (isset($this->params['kpi_symbol']) && !empty($this->params['kpi_symbol'])) {
    $kpi_symbol = $this->params['kpi_symbol'];
}
$kpi_name = '';
if (isset($this->params['kpi_name']) && !empty($this->params['kpi_name'])) {
    $kpi_name = $this->params['kpi_name'];
}
$language = '';
if (isset($this->params['language']) && !empty($this->params['language'])) {
    $language = $this->params['language'];
}
$proj_id = '';
if (isset($this->params['proj_id']) && !empty($this->params['proj_id'])) {
    $proj_id = $this->params['proj_id'];
    // var_dump($proj_id);
}
$mtp = '';
if (isset($this->params['mtp']) && !empty($this->params['mtp'])) {
    $mtp = $this->params['mtp'];
}
$value_type = '';
if (isset($this->params['value_type']) && !empty($this->params['value_type'])) {
    $value_type = $this->params['value_type'];
}

$transtable = $this->dataStore('translation');
$mtptable = $this->dataStore('mtp1');
$mtp_name = $mtptable->where('id', $mtp)->get(0, "name");
$transtable = $this->dataStore('translation');

?>

<!DOCTYPE html>
<?php if ($language == 'ar')
    $dir = "rtl";
else
    $dir = "ltr";
?>
<html dir="<?php echo $dir; ?>">
<!-- <script>window.location.replace('profile');</script> -->
<head>
    <meta charset="utf-8">
    <title>project risk sub report
    </title>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script> -->
</head>
<style>
    .container1 {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
        display: inline-flex;
        column-width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .buttons-print {
        background-color: #ffffff;
        boder: none;
    }

    table {
        width: 100%;
        table-layout: fixed;
    }

    .color {
        border: 0px solid black;
    }

    .insideBorder {
        border: 10px solid white;
    }

    .line {
        width: 1320px;
        border-bottom: 1px solid black;
        position: absolute;
    }

    .cssHeader {
        background-color: #73818f;
        color: #fff;
        text-align: <?php echo $language=='ar'?'right':'left' ;?>;
        font-size: 12px;

    }

    .cssItem {
        background-color: #fdffe8;
        font-size: 12px;
    }

    .container {
        width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .container1 {
        display: inline-flex;
        column-width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .header img {
        float: left;
        width: 100px;
        height: 100px;
        background: #555;
    }

    .select {
        margin: 10px 10px 0px 325px;
    }
</style>
<body>
<div align=center
     style="background-color:#ffffff;margin-left:30px;margin-right:30px;margin-top:0px;margin-bottom:30px;padding-top:30px;">
    <h4 class="mb-0 pt-2" style="color: #20a8d8;font-size: 20px;font-weight: normal;">
        <?php $textbit = 'title';
        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
        echo $translation ?>
    </h4>
    <scan class="col-md-3 form-group" style="float: right">
        <script> document.write(new Date().toDateString()); </script>
    </scan>
    <?php if ($language == 'ar') {
        $dir = "rtl";
        $lin = "left";
    } else {
        $dir = "ltr";
        $lin = "right";
    }
    ?>

    <div style="background-color:#ffffff;border: 1px solid #a5aeb7;position:relative;" class="col-md-12"
         style="float:right;">
        <div id="button1" dir="rtl" style="float:<?php echo $lin; ?>">


            <button onClick="javascript:history.go(-1)" style="float:left;border:none;background-color: #ffffff;"><i
                    style="padding-top:10px;color:#a9a9a9;font-size:12px;" class="fa fa-arrow-left "></i></button>
            <button onClick="javascript:printDiv('details')"
                    style="float:left;border:none;background-color: #ffffff;"><i
                    style="padding-top:10px;color:#a9a9a9;font-size:12px" class="fa fa-print "></i></button>
        </div>
    </div>
    <br/>
    <?php
    $new = $this->dataStore('project_details');
    // ->filter(function ($row) {
    //     return $row["value"] != null;
    // });
    $project = $this->dataStore('project_details')->get(0, "name");
    $project_type = $this->dataStore('project_details')->get(0, "project_type");
    $project_cat = $this->dataStore('project_details')->get(0, "project_category");

    $org_unit1 = $this->dataStore('project_details')->get(0, "org_unit");

    // $org_unit1 = $this->dataStore('project_details')->get(0, "subtenant_id");
    // var_dump($org_unit1);

    $proj_mgr = $this->dataStore('project_details')->get(0, "proj_mgr");
    $proj_start_date = $this->dataStore('project_details')->get(0, "start_dt_actual");
    $proj_end_date = $this->dataStore('project_details')->get(0, "end_dt_base");
    // var_dump($proj_end_date);

    $proj_planned_budget = $this->dataStore('project_details')->get(0, "budget_base");
    $actual_cost = $this->dataStore('project_details')->get(0, "b_actual_cost");
    // $remaining = $this->dataStore('project_details')->get(0, "budget_base");
    // $cost_variance = $this->dataStore('project_details')->get(0, "budget_base");
    // $cpi = $this->dataStore('project_details')->get(0, "budget_base");
    $SPI = $this->dataStore('project_details')->get(0, "spi");
    $schedule_variance = $this->dataStore('project_details')->get(0, "sv_sched_var_pct");
    $proj_duration = $this->dataStore('project_details')->get(0, "pd_planned_duration");
    $remaining = $this->dataStore('project_details')->get(0, "remaining");
    $cost_variance = $this->dataStore('project_details')->get(0, "b_cv_cost_var");
    $CPI = $this->dataStore('project_details')->get(0, "cpi");
    //  $this->dataStore('project_details')->get(0, "start_date_actual");
    //$schedule_variance= $this->dataStore('project_details')->get(0, "start_date_actual");
    //$SDI= $this->dataStore('project_details')->get(0, "start_date_actual");


    // $risk= $this->dataStore('risk_details')->get(0, "description");
    // $risk_cat= $this->dataStore('risk_details')->get(0, "risk_cat");
    // $risk_trigger= $this->dataStore('risk_details')->get(0, "risk_trigger");
    // $date_identified= $this->dataStore('risk_details')->get(0, "identified_dt");
    // $action_plan= $this->dataStore('risk_details')->get(0, "action_plan");
    // $risk_importance=$this->dataStore('risk_details')->get(0, "propability")*$this->dataStore('risk_details')->get(0, "impact");
    // $risk_owner=$this->dataStore('risk_details')->get(0, "risk_owner");
    // $risk_treatment= $this->dataStore('risk_details')->get(0, "treatment");


    ?>

    <?php $style = "";
    if (empty($_POST)) {
        $style = 'display:none !important;';
    } else {
        $style = 'display:block !important;';
    } ?>

    <div class="col-md-12"
         style="background-color:#ffffff;border: 1px solid #a5aeb7;padding-top:10px;margin-bottom:20px;border-radius: 0.25rem;<?php echo $style; ?>;">
        <br>
    </div>
    <br/>
    <br/>
    <div
        style="background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right: 20px;">
        <br>
        <?php if ($language == 'ar') {
            $dir = "rtl";
            $lin = "right";
        } else {
            $dir = "ltr";
            $lin = "left";
        }
        ?>
        <div id='details' dir="<?php echo $language == 'ar' ? "rtl" : "ltr"; ?>">
            <div class="row">
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- project -->
                        <!-- <\?php $textbit = 'project';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?> -->
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- project_name -->
                        <?php $textbit = 'project_name';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>

                <div class="col-md-5 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $project; ?>
                    </strong>
                </div>

                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- project_type -->
                        <?php $textbit = 'project_type';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>

                <div class="col-md-2 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php
                        $prj_type = $this->dataStore('proj_type')->where('id', $project_type)->get(0, "name");
                        echo $prj_type;

                        ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- project_cat -->
                        <?php $textbit = 'project_cat';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>

                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo
                        $cat = $project_cat == 1 ? 'ministry' : ($project_cat == 2 ? 'development' : "");

                        $prj_cat = $language == 'en' ? $transtable->where('key_name', $cat)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $cat)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar");

                        $project_cat; ?>
                    </strong>
                </div>

            </div>

            <div class="row">
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- org_unit -->
                        <!-- <\?php $textbit = 'org_unit';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?> -->
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- org_unit -->
                        <?php $textbit = 'org_unit';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-5 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php
                        $org = $this->dataStore('org_name')->where('id', $org_unit1)->get(0, "name");
                        echo $org;

                        ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <strong>     <!-- <\?php echo get_text('mtp',$language,$kpi,$mtp);?> -->
                        <!-- proj_mgr -->
                        <?php $textbit = 'proj_mgr';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-2 form-group" style="text-align:<?php echo $lin; ?>;float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $proj_mgr//$this->params['mtp']?>
                    </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- proj_start_date -->
                        <!-- <\?php $textbit = 'proj_start_date';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?> -->
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- proj_start_date -->
                        <?php $textbit = 'proj_start_date';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $proj_start_date ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- proj_end_date -->
                        <?php $textbit = 'proj_end_date';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $proj_end_date; ?>
                    </strong>
                </div>

                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- proj_duration -->
                        <?php $textbit = 'proj_duration';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $proj_duration ?>
                    </strong>
                </div>


                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- schedule_variance -->
                        <?php $textbit = 'schedule_variance';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-2 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $schedule_variance ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- SDI -->
                        <?php $textbit = 'spi';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $SPI ?>
                    </strong>
                </div>

            </div>

            <div class="row">
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- Proj_planed_budget -->
                        <!-- <\?php $textbit = 'proj_planned_budget';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?> -->
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- Proj_planed_budget -->
                        <?php $textbit = 'proj_planned_budget';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $proj_planned_budget ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- actual_cost -->
                        <?php $textbit = 'actual_cost';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $actual_cost ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- remaining -->
                        <?php $textbit = 'remaining';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $remaining ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- cost_variance -->
                        <?php $textbit = 'cost_variance';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-2 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $cost_variance ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                    <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                    <strong>
                        <!-- CPI -->
                        <?php $textbit = 'cpi';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                </div>
                <div class="col-md-1 form-group" style="text-align:<?php echo $lin; ?>;float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $CPI ?>
                    </strong>
                </div>


            </div>
        </div>
        <br>
        <br>
        RISK BLOCK

        <?php
        $new = $this->dataStore('risk_details')->process(new CalculatedColumn(array(
            "slno" => "{#}+1")));

        $new->each(function ($row) {
            // echo $row["name"];
            $slno = $row["slno"];
            $risk = $row["risk_name"];//$this->dataStore('risk_details')->get(0, "description");
            $risk_cat = $row["risk_cat"];// $this->dataStore('risk_details')->get(0, "risk_cat");
            $risk_trigger = $row["risk_trigger"];// $this->dataStore('risk_details')->get(0, "risk_trigger");
            $date_identified = $row["identified_dt"];//$this->dataStore('risk_details')->get(0, "identified_dt");
            $action_plan = $row["action_plan"];//$this->dataStore('risk_details')->get(0, "action_plan");
            $risk_importance = $row["propability"] * $row['impact'];//$this->dataStore('risk_details')->get(0, "propability")*$this->dataStore('risk_details')->get(0, "impact");
            $risk_owner = $row["risk_owner"];//$this->dataStore('risk_details')->get(0, "risk_owner");
            $risk_treatment = $row["treatment"];//$this->dataStore('risk_details')->get(0, "treatment");
            $language = '';
            if (isset($this->params['language']) && !empty($this->params['language'])) {
                $language = $this->params['language'];
            }

            if ($language == 'ar') {
                $dir = "rtl";
                $lin = "right";
            } else {
                $dir = "ltr";
                $lin = "left";
            }
            $transtable = $this->dataStore('translation');

            ?>
            <div class="row">
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>

                        <?php echo $slno ?>
                        <!-- <\?php $textbit = 'risk';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?> -->
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>

                        <!-- risk -->
                        <?php $textbit = 'risk';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-10 form-group" style="text-align:<?php echo $lin; ?>;float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $risk ?>
                    </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- risk_cat -->
                        <!-- <\?php $textbit = 'risk_cat';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?> -->
                    </strong> <!-- sector -->
                </div>

                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- risk_cat -->
                        <?php $textbit = 'risk_cat';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-2 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php
                        $category = $this->datastore('risk_cat')->where('id', $risk_cat)->get(0, "name");
                        echo $category;


                        ?>
                    </strong>
                </div>

                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- risk_importance -->
                        <?php $textbit = 'risk_importance';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-2 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php
                        $risk_imp = $risk_importance == 1 || $risk_importance == 2 ? 'very low' : ($risk_importance == 3 || $risk_importance == 4 ? 'low' : ($risk_importance == 5 || $risk_importance == 6 ? 'mid' : ($risk_importance == 7 || $risk_importance == 8 ? 'high' : ($risk_importance == 9 ? 'very high' : ''))));
                        echo $risk_imp;
                        ?>
                    </strong>
                </div>


                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- risk_owner -->
                        <?php $textbit = 'risk_owner';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-4 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php
                        $risk_owner1 = $this->datastore('risk_owner_list')->where('id', $risk_owner)->get(0, "name");
                        echo $risk_owner1;
                        ?>
                    </strong>
                </div>

            </div>

            <div class="row">
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>

                        <!-- risk trigger -->
                        <!-- <\?php $textbit = 'risk';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?> -->
                    </strong> <!-- sector -->
                </div>

                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>

                        <!-- risk trigger -->
                        <?php $textbit = 'risk_trigger';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-10 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $risk_trigger ?>
                    </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>

                        <!-- risk trigger -->
                        <!-- <\?php $textbit = 'risk';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?> -->
                    </strong> <!-- sector -->
                </div>

                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- date_identified -->
                        <?php $textbit = 'date_identified';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-2 form-group" style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                    <strong>
                        <?php echo $date_identified ?>
                    </strong>
                </div>

                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- risk_treatment -->
                        <?php $textbit = 'risk_treatment';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-2 form-group" style="text-align:<?php echo $lin; ?>;float: <?php echo $lin; ?>;">
                    <strong>
                        <?php
                        $treatment = $risk_treatment == 1 ? 'accept' : ($risk_treatment == 2 ? 'mitigate' : ($risk_treatment == 3 ? 'transfer' : ($risk_treatment == 4 ? 'avoid' : '')));
                        echo $treatment;
                        ?>
                    </strong>
                </div>

                <div class="col-md-1 form-group"
                     style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                    <strong>
                        <!-- action_plan -->
                        <?php $textbit = 'action_plan';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_risk_sub_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong> <!-- sector -->
                </div>
                <div class="col-md-4 form-group" style="text-align:<?php echo $lin; ?>;float: <?php echo $lin; ?>;">
                    <strong>
                        <?php
                        //    $treatment=$risk_treatment==1?'accept':($risk_treatment==2?'mitigate':($risk_treatment==3?'transfer':($risk_treatment==4?'avoid':'')));
                        echo $action_plan;
                        ?>
                    </strong>
                </div>
            </div>
        <?php }); ?>

    </div>
    <script type="text/javascript">
        KoolReport.load.onDone(function () {
            var locale = '<?php echo $language;?>';
            var details = $('#details').innerHTML;
            var table1 = $('#example1').DataTable({
                "pageLength": 25,
                destroy: true,
                "language": {
                    "sProcessing": locale == 'ar' ? "جارٍ التحميل..." : "Processing...",
                    "sLengthMenu": locale == 'ar' ? "اعرض _MENU_ سجلات" : "Show _MENU_ entries",
                    "sZeroRecords": locale == 'ar' ? "لم يعثر على أية سجلات" : "No matching records found",
                    "sInfo": locale == 'ar' ? "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل" : "Showing _START_ to _END_ of _TOTAL_ entries",
                    "sInfoEmpty": locale == 'ar' ? "يعرض 0 إلى 0 من أصل 0 سجل" : "Showing 0 to 0 of 0 entries",
                    "sInfoFiltered": locale == 'ar' ? "(منتقاة من مجموع _MAX_ مُدخل)" : "(filtered from _MAX_ total entries)",
                    "sInfoPostFix": "",
                    "sSearch": locale == 'ar' ? "ابحث:" : "Search:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": locale == 'ar' ? "الأول" : "First",
                        "sPrevious": locale == 'ar' ? "السابق" : "Last",
                        "sNext": locale == 'ar' ? "التالي" : "Next",
                        "sLast": locale == 'ar' ? "الأخير" : "Previous",
                    },
                },
                "columnDefs": [{
                    "searchable": true,
                    "orderable": true,
                    "targets": 0
                },
                ],
                "order": [[0, 'asc']],
            });
            table1.buttons().container().appendTo($('#button1'));
        });

        function printDiv(divID) {
            // //Get the HTML of div
            // var divElements = document.getElementById(divID).innerHTML;
            // // var table = $('#example1').DataTable()
            // var heading = "بيان  حالة المؤشرات"
            // // var tableTag = "<table id=\"example1\" class=\"table table-striped table-bordered color no-footer dataTable\" role=\"grid\" aria-describedby=\"example_info\" style=\"width: 1234px;\">";
            // // var thead = table.table().header().outerHTML;
            // // var rows = table.rows({search: 'applied'}).nodes();
            // // var rowStr = "";
            // // for (var i = 0; i < rows.length; i++)
            // //     rowStr += rows[i].outerHTML;
            // var divElements = document.getElementById(divID).innerHTML;

            // // var datatest = tableTag + thead + rowStr + divElements//$('#example').prop('outerHTML')//$('#example').wrapAll('<div>').parent().html();
            // data1 = "<html dir=\"rtl\" lang=\"ar\"><head><meta charset=\"utf-8\"> <title>" + heading + "</title></head><body><div style=\"text-align:center\"><h1>" + heading + "</h1></div>" + divElements + "</table></body></html>";
            // var oldPage = document.body.innerHTML;

            // document.body.innerHTML = data1;
            window.print();
            // document.body.innerHTML = oldPage;

        }

    </script>
    <style>
        @media print {
            .header-print {
                display: table-header-group;
            }
        }

        .buttons-print {
            background-color: #ffffff;
            boder: none;
        }

        button.dt-button, div.dt-button, a.dt-button, a.dt-button:focus {
            border: none !important;
            background-color: #ffffff;
            background: none;
        }

        .card-body {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }

        button.dt-button {
            font-size: 0.68em;
        }

        .select2 {
            width: 100% !important;
            min-height: 40px;
        }

    </style>
</body>
</html>
