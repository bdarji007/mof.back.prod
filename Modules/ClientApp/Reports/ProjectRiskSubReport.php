<?php

namespace Modules\ClientApp\Reports;

use \koolreport\processes\Filter;
use \koolreport\processes\ColumnMeta;
use \koolreport\processes\CalculatedColumn;
use \koolreport\processes\Custom;

error_reporting(E_ALL ^ E_NOTICE);


class ProjectRiskSubReport extends \koolreport\KoolReport
{
    use \koolreport\inputs\Bindable;
    use \koolreport\inputs\POSTBinding;

    protected function defaultParamValues()
    {
        return array(
            "sector" => "",
            "section" => "",
            // "proj_id"=>2,

        );
    }

    protected function bindParamsToInputs()
    {
        return array(
            "sector",
            "section",
            // "proj_id"
        );
    }

    public function settings()
    {
        return array(
            "dataSources" => array(
                "mysql" => array(
                    'host' => env('DB_HOST'),
                    'username' => env('DB_USERNAME'),
                    'password' => env('DB_PASSWORD'),
                    'dbname' => env('DB_DATABASE'),
                    'charset' => 'utf8',
                    'class' => "\koolreport\datasources\MySQLDataSource",
                ),
            )
        );
    }

    function setup()
    {
        // var_dump($this->params['proj_id']);
        $this->src("mysql")
            ->query("select 	p.id, p.name, t.name as project_type, p.project_category,
            sub.name as org_unit,
            if(u_mgr.id is null,
                    p.proj_mgr_ext,
                    f_build_person_name(u_mgr.name, u_mgr.second_name, u_mgr.last_name)
                ) as proj_mgr,
                p.start_dt_actual, p.end_dt_base, pvs.pd_planned_duration,
                pvs.sv_sched_var_pct, pvs.sv_sched_var_days, if(p.has_budget=1, pvs.b_spi, pvs.spi) as spi,
                p.budget_base, pvs.b_actual_cost, (p.budget_base - pvs.b_actual_cost) as remaining,
                pvs.b_cv_cost_var, pvs.b_cpi as cpi,
                pvs.proj_status, ps.name as status_name, ps.color_code, pvs.proj_perf
from project p
left join 	users u_mgr
        ON	p.proj_mgr = u_mgr.id
, proj_type t, subtenant sub
, proj_values pv, proj_value_stats pvs, proj_status ps
where
            p.id = :proj_id and
            t.id = p.project_type and
            sub.id = ifnull(p.subtenant_id, p.sector_id) and
            pvs.id = pv.id and
            pv.project_id = p.id and
            pv.period_dt = 	(select max(pv_i.period_dt) from proj_values pv_i where
                                                                                                                pv_i.project_id = pv.project_id and
                                                                                                                pv_i.progress_val is not null
                                                                ) and
            ps.id = pvs.proj_status
;")
            ->params(array(":proj_id" => $this->params["proj_id"]))
            ->pipe($this->dataStore('project_details'));

        $this->src("mysql")
            ->query("select 	pr.id, pr.description as risk_name, prc.name as risk_cat,
            pr.propability, pr.impact,
            if(u_owner.id is null,
                    pr.risk_owner_ext,
                    f_build_person_name(u_owner.name, u_owner.second_name, u_owner.last_name)
                ) as risk_owner,
                pr.risk_trigger, pr.identified_dt,pr.treatment, pr.action_plan
from proj_risks pr
left join 	users u_owner
    ON		pr.risk_owner = u_owner.id
, proj_risk_cat prc where
                pr.project_id = :proj_id and
                pr.risk_cat = prc.id
;")
            ->params(array(":proj_id" => $this->params["proj_id"]))
            ->pipe($this->dataStore('risk_details'));


        $this->src("mysql")
            ->query("select id,name from users")
            ->pipe($this->dataStore('risk_owner_list'));


        $this->src("mysql")
            ->query("select * from trans_table")
            ->pipe($this->dataStore('translation'))->requestDataSending();
        $this->src("mysql")
            ->query("select id,name from proj_risk_cat")
            ->pipe($this->dataStore('risk_cat'));
        $this->src("mysql")
            ->query("select id,name from proj_type")
            ->pipe($this->dataStore('proj_type'));
        $this->src("mysql")
            ->query("select id,name from subtenant")
            ->pipe($this->dataStore('org_name'))->requestDataSending();
    }
}
