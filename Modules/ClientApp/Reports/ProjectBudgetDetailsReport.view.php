<?php

use \koolreport\widgets\koolphp\Table;
use \koolreport\processes\CalculatedColumn;
use \koolreport\inputs\BSelect;
use \koolreport\inputs\Select;
use \koolreport\processes\Sort;
use \koolreport\inputs\Select2;
use \koolreport\datagrid\DataTables;
use \koolreport\sparklines;
use \koolreport\inputs\DateTimePicker;
use Modules\ClientApp\Reports\ProjectBudgetDetailsReport;


$project_budget_details_report = '';
if (isset($this->params['project_budget_details_report']) && !empty($this->params['project_budget_details_report'])) {
    $org_unit = $this->params['org_unit'];
    // var_dump($org_unit);
}

$kpi_symbol = '';
if (isset($this->params['kpi_symbol']) && !empty($this->params['kpi_symbol'])) {
    $kpi_symbol = $this->params['kpi_symbol'];
}
$kpi_name = '';
if (isset($this->params['kpi_name']) && !empty($this->params['kpi_name'])) {
    $kpi_name = $this->params['kpi_name'];
}
$language = '';
if (isset($this->params['language']) && !empty($this->params['language'])) {
    $language = $this->params['language'];
}
$proj_id = '';
if (isset($this->params['proj_id']) && !empty($this->params['proj_id'])) {
    $proj_id = $this->params['proj_id'];
    // var_dump($proj_id);
}
$mtp = '';
if (isset($this->params['mtp']) && !empty($this->params['mtp'])) {
    $mtp = $this->params['mtp'];
}
$value_type = '';
if (isset($this->params['value_type']) && !empty($this->params['value_type'])) {
    $value_type = $this->params['value_type'];
}

//-------------------------generating datastore for section------------------------------
if ($this->params["sector"] != null) {

    // ----------------------------------------selecting roles corresponding to this sector--------------------------------------------

    if ($this->datastore('section111')->count() > 0) {
        if ($this->params["oid"] != "null") {

            $role_section_id = $this->dataStore("section111")->only("id")->data();
            $j = 0;
            foreach ($role_section_id as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($v != NULL) {
                        // echo "Key=" . $k . ", Value=" . $v;
                        // echo "<br>";
                        $r_section[$j] = $v;
                        $j = $j + 1;
                    }
                }
            }
            $store = [];


            foreach ($r_section as $key => $v) {
                $section_name = "sect" . $v;
                $store = array_merge($store, $this->dataStore($section_name)->data());
            }
        }
    }

    if ($this->datastore('section222')->count() > 0) {
        if ($this->params["oid"] != "null") {
            $store = $this->dataStore("section222")
                ->filter(function ($row) {
                    return $row["id"] != NULL;
                });

        }
    }

    if ($this->params["oid"] == "null") {
        $store = $this->dataStore("section1")
            ->filter(function ($row) {
                return $row["id"] != NULL;
            });

    }
}
//----------------------------------------------------------------------------------

$transtable = $this->dataStore('translation');
$mtptable = $this->dataStore('mtp1');
$mtp_name = $mtptable->where('id', $mtp)->get(0, "name");
$transtable = $this->dataStore('translation');

?>

<!DOCTYPE html>
<?php if ($language == 'ar')
    $dir = "rtl";
else
    $dir = "ltr";
?>
<html dir="<?php echo $dir; ?>">
<!-- <script>window.location.replace('profile');</script> -->
<head>
    <meta charset="utf-8">
    <title>
    </title>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script> -->
</head>
<style>

    .legend {
        list-style: none;
    }

    .legend li {
        float: <?php echo $lin?>;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    .legendar {
        list-style: none;
        margin-right: -6%;
    }

    .legendar li {
        float: <?php echo $lin?>;
        margin-left: 10px;
    }

    .legendar span {
        border: 1px solid #ccc;
        float: right;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    /* your colors */
    .legend .greenlegend {
        background-color: green;
        text-align: <?php echo $lin?>;
    }

    .legend .bluelegend {
        background-color: blue;
        text-align: <?php echo $lin?>;
    }

    .legend .redlegend {
        background-color: red;
        text-align: <?php echo $lin?>;
    }

    .legend .blacklegend {
        background-color: black;
        text-align: <?php echo $lin?>;
    }

    .legendar .greenlegend {
        background-color: green;
        text-align: <?php echo $lin?>;
    }

    .legendar .bluelegend {
        background-color: blue;
        text-align: <?php echo $lin?>;
    }

    .legendar .redlegend {
        background-color: red;
        text-align: <?php echo $lin?>;
    }

    .legendar .blacklegend {
        background-color: black;
        text-align: <?php echo $lin?>;
    }

    .legendstatus {
        list-style: none;
    }

    .legendstatus li {
        float: <?php echo $lin?>;
        margin-right: 10px;
    }

    .legendstatus span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

    /* your colors */
    .legendstatus .greenlegendstatus {
        background-color: green;
    }

    .legendstatus .bluelegendstatus {
        background-color: blue;
    }

    .legendstatus .redlegendstatus {
        background-color: red;
    }

    .legendstatus .blacklegendstatus {
        background-color: black;
    }

    .legendstatus .bluelegendstatus {
        background-color: blue;
    }

    .line {
        width: 100%;
        border-bottom: 1px solid black;
        display: flex;
        flex: 1;
    }

    .decoratedLine1, .decoratedLine2, .decoratedLine3, .decoratedLine4, .decoratedLine5 {
        overflow: hidden;
    }

    .decoratedLine1 > span, .decoratedLine2 > span, .decoratedLine3 > span, .decoratedLine4 > span, .decoratedLine5 > span {
        position: relative;
        display: inline-block;
    }

    .decoratedLine1 > span:before, .decoratedLine2 > span:before, .decoratedLine3 > span:before, .decoratedLine4 > span:before, .decoratedLine5 > span:before {
        content: '';
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        width: 591px;
        margin: 0 20px;
    }

    .decoratedLine1 > span:before {
        border-bottom: 12px solid #01996d;
    }

    .decoratedLine2 > span:before {
        border-bottom: 12px solid #cc043e;
    }

    .decoratedLine3 > span:before {
        border-bottom: 12px solid #01996d;
    }

    .decoratedLine4 > span:before {
        border-bottom: 12px solid #cc043e;
    }

    .decoratedLine1En > span:before, .decoratedLine2En > span:before, .decoratedLine3En > span:before, .decoratedLine4En > span:before, .decoratedLine5En > span:before {
        right: 100%;
    }

    .decoratedLine1Ar > span:before, .decoratedLine2Ar > span:before, .decoratedLine3Ar > span:before, .decoratedLine4Ar > span:before, .decoratedLine5Ar > span:before {
        left: 100%;
    }


    .container1 {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
        display: inline-flex;
        column-width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .buttons-print {
        background-color: #ffffff;
        boder: none;
    }

    table {
        width: 100%;
        table-layout: fixed;
    }

    .color {
        border: 0px solid black;
    }

    .insideBorder {
        border: 10px solid white;
    }


    .cssHeader {
        background-color: #73818f;
        color: #fff;
        text-align: <?php echo $language=='ar'?'right':'left' ;?>;
        font-size: 12px;

    }

    .cpiClass {
        background-color: #73818f;
        color: #fff;
        text-align: <?php echo $language=='ar'?'right':'left' ;?>;
        font-size: 12px;
        padding-left: 5px;
        padding-right: 5px;
        cursor: pointer;
    }

    .far fa-arrow-alt-circle-up {
        /* display: </?php echo $cpi>1?'visible':'none' ;?>; */

    }

    .far fa-arrow-alt-circle-down {
        /* display: </?php echo $cpi<1 && $cpi!=0?'visible':'none' ;?>; */

    }

    .fas fa-check-circle {
        /* display: </?php echo $cpi==1?'visible':'none' ;?>; */

    }

    .fas fa-circle {
        /* display: </?php echo $cpi==0?'visible':'none' ;?>; */

    }

    .cssItem {
        background-color: #fdffe8;
        font-size: 12px;
    }

    .container {
        width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .container1 {
        display: inline-flex;
        column-width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .header img {
        float: left;
        width: 100px;
        height: 100px;
        background: #555;
    }

    .select {
        margin: 10px 10px 0px 325px;
    }
</style>
<body>
<p id="printarea"></p>
<div id="printissue">

    <div align=center
         style="background-color:#ffffff;margin-left:30px;margin-right:30px;margin-top:0px;margin-bottom:30px;padding-top:30px;">
        <h4 class="mb-0 pt-2" style="color: #20a8d8;font-size: 20px;font-weight: normal;">
            <?php $textbit = 'title';
            $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
            echo $translation ?>
        </h4>
        <scan class="col-md-3 form-group" style="float: right">
            <script> document.write(new Date().toDateString()); </script>
        </scan>
        <?php if ($language == 'ar') {
            $dir = "rtl";
            $lin = "left";
            $lin1 = "right";
            $legend = "legend";
        } else {
            $dir = "ltr";
            $lin = "right";
            $lin1 = "left";
            $legend = "legendar";


        }
        $dis = $this->params['proj_id'] != NULL ? 'block' : 'none';
        ?>

        <div style="background-color:#ffffff;border: 1px solid #a5aeb7;position:relative;" class="col-md-12"
             style="float:<?php echo $lin; ?>">
            <div id="button1" dir="rtl" style="float:<?php echo $lin; ?>">

                <button onclick="myFunction();" style="float:left;border:none;background-color: #ffffff;"><i
                        style="padding-top:5px;color:#a9a9a9;;" class="fa fa-angle-up 4x"></i></button>
                <button onClick="javascript:history.go(-1)"
                        style="display:<?php echo $dis; ?>;float:<?php echo $lin1; ?>;border:none;background-color: #ffffff;">
                    <i
                        style="padding-top:10px;color:#a9a9a9;font-size:12px;" class="fa fa-arrow-left "></i></button>
                <button onClick="javascript:printDiv('details')"
                        style="float:left;border:none;background-color: #ffffff;"><i
                        style="padding-top:10px;color:#a9a9a9;font-size:12px" class="fa fa-print "></i></button>
            </div>
        </div>
        <br/>
        <?php
        $new = $this->dataStore('project_details')->process(new CalculatedColumn(array(
            "sl" => "{#}+1")));

        ?>

        <?php $style = "";
        if (empty($_POST)) {
            $style = 'display:none !important;';
        } else {
            $style = 'display:block !important;';
        } ?>


        <div class="col-md-12" id="myDIV"
             style="overflow:auto;width:100%;background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;<?php echo $style; ?>">
            <form id="form1" method="post">
                <div class="col-md-4 form-group" style="float:<?php echo $lin1; ?>">
                    <strong>
                        <!-- sector -->
                        <?php $textbit = 'sector';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php
                    select2::create(array(
                        // "multiple"=>false,
                        "name" => "sector",
                        "defaultOption" => array("--" => ""),
                        "dataStore" => $this->dataStore("sector1"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        // "attributes" => array(
                        //     "class" => "col-md-4 form-control"
                        // ),
                        "clientEvents" => array(
                            "change" => "function(params){
                             $('#section').val(null).trigger('change');
                             $('#form1').submit();
                }"
                        )
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float: <?php echo $lin1; ?>">
                    <strong>
                        <!-- org_unit -->
                        <?php $textbit = 'org_unit';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php

                    Select2::create(array(
                        // "multiple"=>true,
                        "name" => "section",
                        "defaultOption" => array("--" => ""),
                        "dataStore" => $store,
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){

                    $('#form1').submit();
                }"
                        )
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float: <?php echo $lin1; ?>">
                    <strong>
                        <!-- project_symbol -->
                        <?php $textbit = 'status_operational';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "operational_status",
                        "defaultOption" => array("--" => ""),
                        "data" => array(
                            $language == 'en' ? $transtable->where('key_name', 'planning')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'planning')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar") => 1,
                            $language == 'en' ? $transtable->where('key_name', 'progressing')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'progressing')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar") => 2,
                            $language == 'en' ? $transtable->where('key_name', 'onhold')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'onhold')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar") => 3,
                            $language == 'en' ? $transtable->where('key_name', 'closed')->where('key_pos', 'project_budget_summary_report')->get(0, "value_en") : $transtable->where('key_name', 'closed')->where('key_pos', 'project_budget_summary_report')->get(0, "value_ar") => 4,


                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){

                    $('#form1').submit();
                }"
                        )
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float:<?php echo $lin1; ?>">
                    <strong>
                        <!-- project_name -->
                        <?php $textbit = 'project_list';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                        echo $translation; ?>
                    </strong>
                    <?php
                    select2::create(array(
                        "multiple" => true,
                        "name" => "project_name",
                        "dataStore" => $this->dataStore("project_name"),
                        "placeholder" => "اختر ",

                        "defaultOption" => array("--" => ""),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "name",
                        ),
                        "attributes" => array(
                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){

                    $('#form1').submit();
                }"
                        )
                    ));
                    ?>
                </div>
                <div class="col-md-4 form-group" style="float: <?php echo $lin1; ?>">
                    <strong>
                        <?php $textbit = 'mtp';
                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'kpi_status_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'kpi_status_report')->get(0, "value_ar"));
                        echo $translation ?>                </strong>
                    <?php
                    select2::create(array(
                        // "multiple"=>true,
                        "name" => "mtp",
                        "placeholder" => "اختر ",
                        "dataStore" => $this->dataStore("mtp1"),
                        "dataBind" => array(
                            "text" => "name",
                            "value" => "id",
                        ), "attributes" => array(

                            "class" => "col-md-4 form-control"
                        ),
                        "clientEvents" => array(
                            "change" => "function(params){

                    $('#form1').submit();
                }"
                        )
                    ));
                    ?>
                </div>
                <!-- </div> -->

                <!-- <br> -->
            </form>
            <!-- </div> -->
        </div>
        <br/>
        <br/>
        <div id="pivot"
             style="background-color:#ffffff;border: 1px solid #a5aeb7; padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right: 20px;">
            <br>
            <?php

            $new

            ->each(function ($row) {
            $i = $row["sl"];
            $project_name = $row["name"];// $this->dataStore('project_details')->get(0, "name");
            $project_symbol = $row["symbol"];//$this->dataStore('project_details')->get(0, "symbol");

            $project_type = $row["proj_type_name"];//$this->dataStore('project_details')->get(0, "proj_type_name");
            $project_category = $row["project_category"];//$this->dataStore('project_details')->get(0, "project_category");
            $project_status = $row["proj_status_name"];//$this->dataStore('project_details')->get(0, "proj_status_name");
            $project_importance = $row["importance"];//$this->dataStore('project_details')->get(0, "importance");

            $project_manager = $row["proj_mgr_name"];//$this->dataStore('project_details')->get(0, "proj_mgr_name");
            $responsible_unit = $row["resp_org_unit"];//$this->dataStore('project_details')->get(0, "resp_org_unit");
            $project_owner = $row["proj_owner_name"];//$this->dataStore('project_details')->get(0, "proj_owner_name");

            $start_date = $row["start_dt_actual"];// $this->dataStore('project_details')->get(0, "start_dt_actual");
            $end_date = $row["end_dt_base"];//$this->dataStore('project_details')->get(0, "end_dt_base");
            $duration = $row["pd_planned_duration"];//$this->dataStore('project_details')->get(0, "pd_planned_duration");
            $progress = $row["progress_val"];//$this->dataStore('project_details')->get(0, "progress_val");

            $planned_budget = $row["budget_base"];//$this->dataStore('project_details')->get(0, "budget_base");
            $earned_value = $row["b_ev_earned_val"];//$this->dataStore('project_details')->get(0, "b_ev_earned_val");
            $planned_value = $row["b_pv_planned_val"];//$this->dataStore('project_details')->get(0, "b_pv_planned_val");
            $schedule_variance = $row["b_sv_sched_var"]; //$this->dataStore('project_details')->get(0, "b_sv_sched_var");

            $actual_cost = $row["actual_cost"]; //$this->dataStore('project_details')->get(0, "actual_cost");
            $SDI = "";
            $remaining = $row["remaining_budget"]; //$this->dataStore('project_details')->get(0, "remaining_budget");
            $cost_variance = $row["cost_variance"];//$this->dataStore('project_details')->get(0, "cost_variance");
            $CPI = $row["b_cpi"];//$this->dataStore('project_details')->get(0, "b_cpi");
            // var_dump($CPI);
            $estimate_to_complete = $row["b_etc"]; //$this->dataStore('project_details')->get(0, "b_etc");
            $estimate_at_complete = $row["b_eac"];//$this->dataStore('project_details')->get(0, "b_eac");
            $remaining_budget = $row["remaining_budget"];// $this->dataStore('project_details')->get(0, "remaining_budget");
            $variance_at_complete = $row["b_var_at_complete"];//$this->dataStore('project_details')->get(0, "b_var_at_complete");
            $TCPI = $row["tcpi"];//$this->dataStore('project_details')->get(0, "b_tcpi");
            $transtable = $this->dataStore('translation');
            $language = $this->params['language'];
            if ($language == 'ar') {
                $dir = "rtl";
                $lin = "right";
            } else {
                $dir = "ltr";
                $lin = "left";
            }


            ?>
            <div id='details' dir="<?php echo $language == 'ar' ? "rtl" : "ltr"; ?>">
                <div class="row" style="width:95%;">

                    <div class="col-md-12">

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                            <strong>
                                <?php echo $i; ?>
                                <!-- project -->
                                <!-- <\?php $textbit = 'project';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?> -->
                            </strong> <!-- sector -->
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                            <strong>

                                <?php $textbit = 'project_symbol';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong> <!-- sector -->
                        </div>

                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <!-- project_symbol -->

                                <?php echo $project_symbol; ?>
                            </strong>
                        </div>

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                            <strong>
                                <!-- project_name -->
                                <?php $textbit = 'project_name';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong> <!-- sector -->
                        </div>

                        <div class="col-md-7 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php echo $project_name; ?>
                            </strong>
                        </div>

                    </div>
                    <div class="col-md-12">

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                            <strong>
                                <!-- project_budget_details_report -->
                                <!-- <\?php $textbit = 'project_budget_details_report';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?> -->
                            </strong> <!-- sector -->
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f;float: <?php echo $lin; ?>;">
                            <strong>

                                <?php $textbit = 'project_status';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong> <!-- sector -->
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php echo $project_status; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <strong>     <!-- <\?php echo get_text('mtp',$language,$kpi,$mtp);?> -->

                                <?php $textbit = 'project_category';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>;float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                if ($project_category == 0) {
                                    $textbit = 'ministry';
                                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                }
                                if ($project_category == 1) {
                                    $textbit = 'development';
                                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                }

                                echo $translation; ?>

                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <strong>     <!-- <\?php echo get_text('mtp',$language,$kpi,$mtp);?> -->

                                <?php $textbit = 'project_type';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>;float: <?php echo $lin; ?>;">
                            <strong>
                                <?php echo $project_type; ?>
                            </strong>
                        </div>

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <strong>

                                <?php $textbit = 'project_importance';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                if ($project_importance == 1) {
                                    $textbit = 'weakly_important';
                                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                }
                                if ($project_importance == 2) {
                                    $textbit = 'averagely_important';
                                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                }
                                if ($project_importance == 3) {
                                    $textbit = 'important';
                                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                }
                                if ($project_importance == 4) {
                                    $textbit = 'very_important';
                                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                }
                                if ($project_importance == 5) {
                                    $textbit = 'critically_important';
                                    $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                }
                                echo $translation; ?>
                            </strong>
                        </div>
                    </div>
                    <div class="col-md-12">

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <!-- proj_start_date -->
                                <!-- <\?php $textbit = 'proj_start_date';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?> -->
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'project_owner';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php echo $project_owner; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>; color:#73818f;float: <?php echo $lin; ?>;">
                            <strong>

                                <?php $textbit = 'responsible_unit';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation ?>
                            </strong>
                        </div>
                        <div class="col-md-7 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php echo $responsible_unit; ?>
                            </strong>
                        </div>


                    </div>
                    <br>
                    <br>
                    <div class="col-md-12">

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <!-- Proj_planed_budget -->
                                <!-- <\?php $textbit = 'proj_planned_budget';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?> -->
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'start_date';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php echo $start_date; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'end_date';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php echo $end_date; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'duration';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $textbit = 'days';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                // echo $translation;

                                echo $duration . "&nbsp;" . $translation;

                                ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php


                                $textbit = 'progress';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $progress = $progress * 100;
                                $progress = number_format((float)($progress), 2);
                                echo $progress . "%"; ?>
                            </strong>
                        </div>


                    </div>
                    <br>
                    <br>
                    <div class="col-md-12">

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <!-- Proj_planed_budget -->
                                <!-- <\?php $textbit = 'proj_planned_budget';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?> -->
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'planned_budget';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $planned_budget = number_format((float)($planned_budget), 3);
                                echo "KWD " . $planned_budget; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'earned_value';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $earned_value = number_format((float)($earned_value), 3);
                                echo "KWD " . $earned_value; ?>

                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'planned_value';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>

                                <?php
                                $planned_value = number_format((float)($planned_value), 3);
                                echo "KWD " . $planned_value; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'schedule_variance';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $schedule_variance = number_format((float)($schedule_variance), 3);
                                echo "KWD " . $schedule_variance; ?>
                            </strong>
                        </div>
                    </div>

                    <div class="col-md-12">

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <!-- Proj_planed_budget -->
                                <!-- <\?php $textbit = 'proj_planned_budget';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?> -->
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'actual_cost';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $actual_cost = number_format((float)($actual_cost), 3);
                                echo "KWD " . $actual_cost; ?>

                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'remaining_budget';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $remaining_budget = number_format((float)($remaining_budget), 3);
                                echo "KWD " . $remaining_budget; ?>

                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'cost_variance';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $cost_variance = number_format((float)($cost_variance), 3);
                                echo "KWD " . $cost_variance; ?>

                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'cpi';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $CPI = number_format((float)($CPI), 3);
                                echo $CPI;
                                if ($CPI > 1) {
                                    $color = "blue";
                                    $icon_cpi = "far fa-arrow-alt-circle-up";
                                }
                                if ($CPI < 1 && $CPI != 0) {
                                    $color = "red";
                                    $icon_cpi = "far fa-arrow-alt-circle-down";
                                }
                                if ($CPI == 1) {
                                    $color = "green";
                                    $icon_cpi = "fas fa-check-circle";
                                }
                                if ($CPI == 0) {
                                    $color = "";
                                    $icon_cpi = "fas fa-circle";
                                }
                                ?>
                                <i style="padding-left:5px;padding-right:5px;color:<?php echo $color; ?>;cursor:pointer"
                                   class="<?php echo $icon_cpi ?>" data-toggle="tooltip"></i>
                            </strong>
                        </div>

                    </div>
                    <div class="col-md-12">

                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <!-- Proj_planed_budget -->
                                <!-- <\?php $textbit = 'proj_planned_budget';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos','project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?> -->
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'etc';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $estimate_to_complete = number_format((float)($estimate_to_complete), 3);
                                echo "KWD " . $estimate_to_complete; ?>

                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>
                                <?php $textbit = 'eac';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $estimate_at_complete = number_format((float)($estimate_at_complete), 3);
                                echo "KWD " . $estimate_at_complete; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'variance_at_complete';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-2 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $variance_at_complete = number_format((float)($variance_at_complete), 3);
                                echo "KWD " . $variance_at_complete; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>;color:#73818f; float: <?php echo $lin; ?>;">
                            <!-- <\?php echo get_text('symbol',$language,$kpi,$mtp);?> -->
                            <strong>

                                <?php $textbit = 'tcpi';
                                $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                echo $translation; ?>
                            </strong>
                        </div>
                        <div class="col-md-1 form-group"
                             style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                            <strong>
                                <?php
                                $TCPI = number_format((float)($TCPI), 3);

                                echo $TCPI;
                                if ($TCPI > 1) {
                                    $color = "red";
                                    $icon_cpi = "far fa-arrow-alt-circle-down";

                                }
                                if ($TCPI < 1 && $TCPI != 0) {
                                    $color = "blue";
                                    $icon_cpi = "far fa-arrow-alt-circle-up";
                                }
                                if ($TCPI == 1) {
                                    $color = "green";
                                    $icon_cpi = "fas fa-check-circle";
                                }
                                if ($TCPI == 0) {
                                    $color = "";
                                    $icon_cpi = "fas fa-circle";
                                }
                                ?>
                                <i style="padding-left:5px;padding-right:5px;color:<?php echo $color; ?>;cursor:pointer"
                                   class="<?php echo $icon_cpi ?>" data-toggle="tooltip"></i>

                            </strong>
                        </div>
                    </div>
                </div>


                <div class="col-md-12 line">
                </div>

                <div class="row">
                </div>
                <br>
                <br>

                <?php
                $i = $i + 1;
                }); ?>

                <br>
                <br>

                <div class="row" style="width:90%;">

                    <div class="col-md-12 form-group"
                         style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                        <ul class="<?php echo $language == 'en' ? 'legend' : 'legendar'; ?>">
                            <div style="text-align:center;">

                                <strong>
                                    <?php
                                    $translation_cpi = ($language == 'en' ? $transtable->where('key_name', 'cpi')->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', 'cpi')->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                    $translation_legend = ($language == 'en' ? $transtable->where('key_name', 'legend')->where('key_pos', 'gaugechart')->get(0, "value_en") : $transtable->where('key_name', 'legend')->where('key_pos', 'gaugechart')->get(0, "value_ar"));
                                    $translation_cpi_legend = $translation_cpi . "\t" . $translation_legend;
                                    echo $translation_cpi_legend;
                                    ?>
                                </strong>
                            </div>
                            <div class="row" style="width:90%;">
                                <br>
                                <br>
                            </div>
                            <div class="col-md-6 form-group"
                                 style="text-align:<?php echo $lin1; ?>; float: <?php echo $lin1; ?>;">
                                <ul class="<?php echo $language == 'en' ? 'legend' : 'legendar'; ?>">

                                    <li><span class="greenlegend"></span><?php

                                        $textbit = 'project_on_budget';
                                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                        echo $translation;

                                        ?></li>
                                    <li><span class="bluelegend"></span><?php


                                        $textbit = 'project_under_budget';
                                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                        echo $translation;

                                        ?> </li>
                            </div>
                            <div class="col-md-6 form-group"
                                 style="text-align:<?php echo $lin1; ?>; float: <?php echo $lin1; ?>;">
                                <ul class="<?php echo $language == 'en' ? 'legend' : 'legendar'; ?>">

                                    <li><span class="redlegend"></span><?php

                                        $textbit = 'project_over_budget';
                                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                        echo $translation;


                                        ?> </li>
                                    <li><span class="blacklegend"></span> <?php
                                        $textbit = 'project_has_no_state';
                                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                        echo $translation;
                                        ?>
                                    </li>
                            </div>
                        </ul>

                    </div>
                </div>
                <div class="row" style="width:90%;">


                    <div class="col-md-12 form-group"
                         style="text-align:<?php echo $lin; ?>; float: <?php echo $lin; ?>;">
                        <ul class="<?php echo $language == 'en' ? 'legend' : 'legendar'; ?>">
                            <div style="text-align:center;">
                                <strong>
                                    <?php
                                    $translation_tcpi = ($language == 'en' ? $transtable->where('key_name', 'tcpi')->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', 'tcpi')->where('key_pos', 'project_budget_details_report')->get(0, "value_ar"));
                                    $translation_legend = ($language == 'en' ? $transtable->where('key_name', 'legend')->where('key_pos', 'gaugechart')->get(0, "value_en") : $transtable->where('key_name', 'legend')->where('key_pos', 'gaugechart')->get(0, "value_ar"));
                                    $translation_tcpi_legend = $translation_tcpi . "\t" . $translation_legend;
                                    echo $translation_tcpi_legend;
                                    ?>
                                </strong>
                                <div class="row">
                                    <br>
                                    </br>
                                </div>
                            </div>
                            <div class="col-md-6 form-group"
                                 style="text-align:<?php echo $lin1; ?>; float: <?php echo $lin1; ?>;">
                                <ul class="<?php echo $language == 'en' ? 'legend' : 'legendar'; ?>">

                                    <li><span class="greenlegend"></span><?php
                                        $textbit = 'project_has_just_exact_funds';
                                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                        echo $translation;
                                        ?></li>
                                    <li><span class="bluelegend"></span> <?php
                                        $textbit = 'project_has_more_funds';
                                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                        echo $translation;

                                        ?></li>
                            </div>
                            <div class="col-md-6 form-group"
                                 style="text-align:<?php echo $lin1; ?>; float: <?php echo $lin1; ?>;">
                                <ul class="<?php echo $language == 'en' ? 'legend' : 'legendar'; ?>">

                                    <li><span class="redlegend"></span><?php
                                        $textbit = 'project_has_less_funds';
                                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                        echo $translation;


                                        ?> </li>
                                    <li><span class="blacklegend"></span><?php
                                        $textbit = 'project_has_no_state';
                                        $translation = ($language == 'en' ? $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_en") : $transtable->where('key_name', $textbit)->where('key_pos', 'project_view')->get(0, "value_ar"));
                                        echo $translation;


                                        ?> </li>
                            </div>
                        </ul>

                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    KoolReport.load.onDone(function () {
        var locale = '<?php echo $language;?>';
        var details = $('#details').innerHTML;
        var table1 = $('#example1').DataTable({
            "pageLength": 25,
            destroy: true,
            "language": {
                "sProcessing": locale == 'ar' ? "جارٍ التحميل..." : "Processing...",
                "sLengthMenu": locale == 'ar' ? "اعرض _MENU_ سجلات" : "Show _MENU_ entries",
                "sZeroRecords": locale == 'ar' ? "لم يعثر على أية سجلات" : "No matching records found",
                "sInfo": locale == 'ar' ? "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل" : "Showing _START_ to _END_ of _TOTAL_ entries",
                "sInfoEmpty": locale == 'ar' ? "يعرض 0 إلى 0 من أصل 0 سجل" : "Showing 0 to 0 of 0 entries",
                "sInfoFiltered": locale == 'ar' ? "(منتقاة من مجموع _MAX_ مُدخل)" : "(filtered from _MAX_ total entries)",
                "sInfoPostFix": "",
                "sSearch": locale == 'ar' ? "ابحث:" : "Search:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": locale == 'ar' ? "الأول" : "First",
                    "sPrevious": locale == 'ar' ? "السابق" : "Last",
                    "sNext": locale == 'ar' ? "التالي" : "Next",
                    "sLast": locale == 'ar' ? "الأخير" : "Previous",
                },
            },
            "columnDefs": [{
                "searchable": true,
                "orderable": true,
                "targets": 0
            },
            ],
            "order": [[0, 'asc']],
        });
        table1.buttons().container().appendTo($('#button1'));
    });

    function myFunction() {
        var x = document.getElementById("myDIV");
        if (x.style.display === "none") {
            $(x).show('slow');
        } else {
            $(x).hide('slow');
        }
    };

    function printDiv(divID) {

        //window.print();

        var divElements = document.getElementById('pivot').innerHTML;
        var heading = "<?php echo $translation = ($language == 'en' ? $transtable->where('key_name', 'title')->where('key_pos', 'project_budget_details_report')->get(0, "value_en") : $transtable->where('key_name', 'title')->where('key_pos', 'project_budget_details_report')->get(0, "value_ar")); ?>";
        var siz = "";
        var test = '<?php echo $language;?>';
        if (test == 'en') {
            lang = 'ltr';
        } else {
            lang = 'rtl';
        }

        data2 = "<div style=\"text-align:center;font-size:9px;border: 0px !important;\"><h4>" + heading + "</h4>" + divElements + "</div>";


        document.getElementById('printarea').style.display = "block";

        document.getElementById('printarea').innerHTML = data2;
        document.getElementById('printissue').style.display = "none";

        window.print(); // call print
        document.getElementById('printissue').style.display = "block";
        document.getElementById('printarea').style.display = "none";


        // document.body.innerHTML = oldPage;

    }

</script>
<style>

    <
    style >
    .container1 {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
        display: inline-flex;
        column-width: 100%;
        padding: 0px 0px;
        background: #fff;
    }

    .buttons-print {
        background-color: #ffffff;
        border: none;
        /*color:black;*/
    }

    button.dt-button, div.dt-button, a.dt-button, a.dt-button:focus {
        border: none !important;
        background-color: #ffffff;
        background: none;
        padding: 0;
    }


    div.dt-button-collection {

        /* top: 19.6166px; */
        left: -98.433px;
        transform: translateX(-98px);
    }

    /* .card-body {
        -webkit-box-flex: 1;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
    } */
    .select2 {
        width: 100% !important;
        /*border: 1px solid #e8e8e8;*/
        min-height: 40px;
    }

    button.dt-button {
        font-size: 0.68em;
    }

    /* .select2:after{
        content: '';
        position:absolute;
        left:10px;
        top:15px;
        width:0;
        height:0;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-top: 5px solid #888;
        direction: rtl;
    }; */


    @media print {
        body {
            color: #ccc !important;
            -webkit-print-color-adjust: exact !important;
        }

    }
</style>
</div>
</body>
</html>
