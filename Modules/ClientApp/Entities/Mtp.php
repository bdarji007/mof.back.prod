<?php

namespace Modules\ClientApp\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Mtp extends Model  implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = "mtp" ;

    protected $fillable = ['subtenant_id' , "mtp_start", "mtp_end", "name", "prev_mtp", "kpi_margin_pct"];
    protected $auditInclude = ['subtenant_id' , "mtp_start", "mtp_end", "name", "prev_mtp", "kpi_margin_pct"];
    protected $primaryKey = "id";
    protected $auditEvents = [
        'created',
        'updated',
        'deleted',
        'restored',
    ];
}
