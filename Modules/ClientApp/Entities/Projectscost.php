<?php

namespace Modules\ClientApp\Entities;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Projectscost extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;


    protected $table = "role_cost" ;
    protected $primaryKey = "role_id" ;
    protected $fillable = ['role_id', 'salary_scheme', 'salary_average', 'bonus_cost'];
    // ...

    protected $auditInclude = ['role_id', 'salary_scheme', 'salary_average' , 'bonus_cost'];

    protected $auditEvents = [
        'created',
        'updated',
        'deleted',
        'restored',
    ];


}
