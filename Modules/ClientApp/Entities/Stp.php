<?php

namespace Modules\ClientApp\Entities;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stp extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;


    protected $table = "stp" ;
    protected $primaryKey = "id" ;
    protected $fillable = ['mtp_id', 'fiscal_year', 'sector_id' , 'subtenant_id','responsible_user','comm_officer ','notes','cost_direct_p','cost_indirect_p','cost_direct_a','cost_indirect_a','capacity_available','value_period','status_approval'];
    // ...

    protected $auditInclude = ['mtp_id', 'fiscal_year', 'sector_id' , 'subtenant_id','responsible_user','comm_officer ','notes','cost_direct_p','cost_indirect_p','cost_direct_a','cost_indirect_a','capacity_available','value_period','status_approval'];

    protected $auditEvents = [
        'created',
        'updated',
        'deleted',
        'restored',
    ];


}
